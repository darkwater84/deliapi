<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableMantenimientosNullableHoraFechaEntrada extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mantenimientos', function(Blueprint $table){
            $table->date('FechaEntrada')->nullable()->change();
            $table->time('HoraEntrada')->nullable()->change();
            $table->dropForeign('mantenimientos_imecanico_foreign');
            $table->unsignedInteger('iMecanico')->nullable()->change();
            $table->foreign('iMecanico')->references('iUsuario')->on('usuarios');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mantenimientos', function(Blueprint $table){
            $table->date('FechaEntrada')->change();
            $table->time('HoraEntrada')->change();
            $table->dropForeign('mantenimientos_imecanico_foreign');
            $table->unsignedInteger('iMecanico')->change();
            $table->foreign('iMecanico')->references('iUsuario')->on('usuarios');
        });
    }
}
