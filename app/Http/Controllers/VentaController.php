<?php

namespace App\Http\Controllers;

use App\Jobs\NotificarPedidoPendiente;
use App\Models\Comentario;
use App\Models\Dispositivo;
use Faker\Provider\tr_TR\DateTime;
use App\Http\Controllers\JelpController;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Models\Venta;
use App\Models\Reparto;
use App\Models\Asignacion;
use App\Models\Repartidor;
use App\Models\Rutas;
use App\Models\Sucursal;
use App\Models\Log;
use Parse\ParseInstallation;
use Parse\ParseObject;
use Parse\ParsePush;
use Parse\ParseClient;
use Exception;
use Validator;
use Illuminate\Support\Facades\Input;
use App\Models\Cliente;

class VentaController extends JelpController
{

    const MOVIL = 'MOVIL';
    const CRM = 'CRM';
    const API = 'API';
    const PENDIENTE = 1;
    const ACEPTADO = 2;
    const TRANSITO = 3;
    const ENTREGADO = 4;
    const CANCELADO = 5;

    const QUERY_VENTAS = <<<EOD
        SELECT r.iReparto,
       r.HoraSalida,
       r.HoraEntrega,
       r.HoraEnterado AS FechaVenta,
       v.Telefono,
       v.Importe,
       v.Firma,
       r.iVenta,
       v.Folio,
       v.Latitud      AS latitude,
       v.Longitud     AS longitude,
       rp.iUsuario,
       u.Nombre,
       u.Apellidos,
       r.iComentario,
       v.iEstatus,
       e.Estatus,
       v.iSucursal,
       s.NumeroSucursal,
       s.Sucursal,
       r.Traspaso
    FROM   repartos r
       LEFT OUTER JOIN ventas v
         ON r.iVenta = v.iVenta
       LEFT OUTER JOIN estatus e
         ON v.iEstatus = e.iEstatus
       LEFT OUTER JOIN repartidores rp
         ON r.iRepartidor = rp.iRepartidor
       LEFT OUTER JOIN usuarios u
         ON rp.iUsuario = u.iUsuario
       LEFT OUTER JOIN sucursales s
         ON v.iSucursal = s.iSucursal
    WHERE %s Date(r.HoraEnterado) BETWEEN '%s' AND '%s'
    ORDER  BY r.HoraEnterado DESC ;
EOD;

    const QUERY_VENTAS_CADENA = <<<EOD
    SELECT sum(v.Importe) as Total,
     s.numerosucursal as NumeroSucursal,s.Sucursal
    FROM   ventas v
     JOIN sucursales s
       ON v.isucursal = s.isucursal
    WHERE   %s
    GROUP  BY v.isucursal
EOD;

    const QUERY_ENTREGAS_CADENA = <<<EOD
    SELECT count(r.iReparto) as `Total`,
           r.iSucursal,
           s.Sucursal
    FROM romadb.repartos as `r` join sucursales as `s` on r.iSucursal = s.iSucursal
    where date(HoraEnterado) %s
    group by r.iSucursal;
EOD;

    const QUERY_ENTREGAS_HORARIO_CADENA = <<<EOD
    SELECT count(r.iReparto) as `Total`,
  			concat(hour(r.HoraEnterado),'-',hour(r.HoraEnterado)+1) as Rango
    FROM romadb.repartos as `r` join sucursales as `s` on r.iSucursal = s.iSucursal
    where date(HoraEnterado) %s
    group by hour(r.HoraEnterado)
	 order by Rango ASC;
EOD;


    const QUERY_PROMEDIO_CADENA = <<<EOD
SELECT isucursal                                                             AS        iSucursal,
       numerosucursal                                                        AS        NumeroSucursal,
       sucursal                                                              AS        Sucursal,
(SELECT Count(iventa) FROM   repartos
WHERE  repartos.isucursal = s.isucursal
AND Date(horaenterado) %s) AS        TotalVentas,
(SELECT ( Sum(Time_to_sec(Timediff(vw.horaentrega, vw.horasalida))) ) / Count(vw.ireparto) AS diff
FROM   repartos vw
JOIN ventas v
ON vw.iventa = v.iventa
WHERE  Date(vw.horaenterado) %s
AND vw.isucursal = s.isucursal    AND v.iestatus = 4)                        AS        Segundos
FROM   sucursales s;
EOD;

    const QUERY_PROMEDIO_RANGOHORARIO = <<<EOD
select avg(time_to_sec(timediff(r.HoraEntrega,r.HoraSalida))) as Segundos,
			concat(hour(HoraEnterado),'-',hour(HoraEnterado)+1) as Rango
   from repartos r
   where date(HoraEnterado) %s
	group by Rango
	order by Rango
EOD;


    public function pendientes($iRepartidor)
    {
        //asignacion de la sucursal del repartidor
        $asignaciones = new Asignacion();
        $asignado = $asignaciones->where('iRepartidor', '=', $iRepartidor)->firstOrFail();

        $ventas = new Venta();
        $venta = $ventas->where('iSucursal', '=', $asignado->iSucursal)->where('iEstatus', '=', '1')->get();

        try {
            if (count($venta) > 0) {
                echo json_encode(['Respuesta' => '200', 'Mensaje' => 'True', 'Ventas' => $venta]);
            } else {
                echo json_encode(['Respuesta' => '200', 'Mensaje' => 'False']);
            }
        } catch (Exception $e) {
            return JelpController::CustomError($e->getMessage());
        }
    }

    //Cuando el Repartidor de click en Enterado, crear nueva orden de reparto
    public function enterado(Request $request)
    {
        $inputs = $request->all();
        //Buscar la asignacion del repartidor
        try {
            $repartidor = Repartidor::where('iUsuario', '=', $inputs['iUsuario'])->first();

            $asignado = Asignacion::where('iRepartidor', '=', $repartidor->iRepartidor)
                ->orderBy('created_at', 'desc')
                ->first();

            $venta = Venta::where('Folio', '=', $inputs['Folio'])->whereRaw('(iEstatus = 1 or iEstatus=2)')->first();

            if(empty($venta))
            {
                Log::create(array('Mensaje' => 'No se Encontro Venta',
                    'Request' => json_encode($request->all()),
                    'Origen' => $this::API));
                echo json_encode(['Respuesta' => '202', 'Mensaje' => 'No se econtro Venta']);
            }
            if (!empty($venta)) {
                $miReparto = Reparto::where('iVenta', '=', $venta->iVenta)->first();
                if (empty($miReparto)) {
                    $venta->iEstatus = 2;
                    $venta->save();

                    $reparto = new Reparto();
                    $datetime = new \DateTime();
                    $reparto->HoraEnterado = $datetime->format('Y-m-d H:i:s');
                    $reparto->iVenta = $venta->iVenta;
                    $reparto->iRepartidor = $asignado->iRepartidor;
                    $reparto->iSucursal = $asignado->iSucursal;
                    $reparto->iComentario = 1;
                    $reparto->save();

                    Log::create(array('Mensaje' => 'Enterado',
                        'iUsuario' => $inputs['iUsuario'],
                        'Request' => json_encode($request->all()),
                        'Response' => '200 Exito',
                        'Origen' => $this::MOVIL));
                }
                echo json_encode(['Respuesta' => '200', 'Mensaje' => 'OK']);
            } else {
                echo json_encode(['Respuesta' => '202', 'Mensaje' => 'Venta Asignada']);
            }
        } catch (Exception $e) {
            Log::create(array('Mensaje' => 'Error en transaccion',
                'Request' => json_encode($request->all()),
                'Origen' => $this::API));
            echo json_encode(['Respuesta' => '400', 'Mensaje' => $e->getMessage()]);
        }
    }

    public function nueva(Request $request)
    {

        $inputs = $request->all();
        $validator = Validator::make($request->all(), [
            'folio' => 'required',
            'telefono' => 'required',
            'importe' => 'required',
            'sucursal' => 'required',
        ]);

        if ($validator->fails()) {
            $newLog = new Log();
            $newLog->Mensaje = 'Parametros Incompletos';
            $newLog->Request = json_encode($request->all());
            $newLog->Response = 'Error';
            $newLog->Origen = $this::CRM;
            $newLog->save();
            return JelpController::CustomError('folio, telefono, importe, sucursal son requeridos.');
        }
        try {
            $repetido = Venta::where('Folio', '=', $inputs['folio'])->first();
            if (!empty($repetido)) {
                Log::create(array('Mensaje' => 'Folio Repetido',
                    'Request' => json_encode($request->all()),
                    'Response' => 'CustomError',
                    'Detalle' => 'Folio ya existente en sistema',
                    'Origen' => $this::CRM));
                return JelpController::CustomError('Folio Repetido');
            }

            Cliente::firstOrCreate(['Telefono' => $inputs['telefono']])->touch();

            $venta = new Venta();
            $venta->Folio = $inputs['folio'];
            $venta->Telefono = $inputs['telefono'];
            $venta->Importe = $inputs['importe'];
            $venta->iEstatus = 1;
            $sucursal = Sucursal::where('NumeroSucursal', '=', $inputs['sucursal'])->first();
            $venta->iSucursal = $sucursal->iSucursal;
            $venta->save();

            if(env('EMAIL_NOTIFICATIONS') == 1) {
                $job = (new NotificarPedidoPendiente($venta))->delay(60 * 15);
                $this->dispatch($job);
            }

            ParseClient::initialize('DiK8MAsgN6u2q6LGgjnGJB0lotZ2YVrMUUGpFhXH', '5JF7eBR3zoDi6vNoihzvQshxfhn8N1FPSuc3k3S5', 'tkrrbH4R9j7LjDgYhT5SUNSTpFAT973gaRoVXQbx');

            $data = array("Folio" => $venta->iVenta, 'FechaVenta' => $venta->created_at->format('Y-m-d H:i:s'), "titulo" => "Nueva Venta", "mensaje" => $inputs['folio']);

            $dispositivos = new Dispositivo();
            $allDispositivos = $dispositivos->where('iSucursal', '=', $sucursal->iSucursal)->where('iEstatus', '=', 1)->get();

            foreach ($allDispositivos as $newDispositivo) {
                //Push to Query
                $query = ParseInstallation::query();
                $query->equalTo("IMEI", $newDispositivo->IMEI);
                ParsePush::send(array(
                    "where" => $query,
                    "data" => $data
                ));
            }

            $newLog = new Log();
            $newLog->Mensaje = 'True';
            $newLog->Request = json_encode($request->all());
            $newLog->Response = 'Success';
            $newLog->Origen = $this::CRM;
            $newLog->save();

            echo json_encode(['Respuesta' => '200', 'Mensaje' => 'True', 'Ventas' => $venta]);
        } catch (Exception $e) {
            $newLog = new Log();
            $newLog->Mensaje = 'Parametros Incompletos';
            $newLog->Request = json_encode($request->all());
            $newLog->Response = 'Error';
            $newLog->Origen = $this::CRM;
            $newLog->Detalle = $e->getMessage();
            $newLog->save();
            return JelpController::CustomError($e->getMessage());
        }
    }

    public function getAllVentas(Request $request)
    {

        //QUERY_VENTAS
        $validator = Validator::make($request->all(), [
            'Inicio' => 'required',
            'Fin' => 'required',
        ]);

        if ($validator->fails()) {
            return JelpController::CustomError('Inicio y Fin son requeridos.');
        }

        $fechaInicial = $request->input('Inicio');
        $fechaFinal = $request->input('Fin');

        if ($fechaFinal < $fechaInicial) {
            return JelpController::CustomError('Inicio debe ser mayor o igual que fin');
        }

        $query = sprintf($this::QUERY_VENTAS, '',$fechaInicial, $fechaFinal);

        $entregas = DB::select(DB::raw($query));

        return JelpController::Respuesta(['Ventas' => $entregas]);
    }

    /**
     * GET - (dashboard/ventas/{id}/ruta) - Obtiene la ruta de una venta.
     */
    public function getRutaVentaReparto($id)
    {
        if (($id == null) or (!is_numeric($id)) or ($id <= 0)) {
            return JelpController::CustomError('Reparto invalida.' . $id);
        }
        try {
            $reparto = Reparto::where('iReparto', '=', $id)->firstOrFail();
            if (empty($reparto->HoraEntrega)) {
                if (empty($reparto->HoraSalida)) {
                    $reparto->HoraSalida = $reparto->HoraEnterado;
                }
                $query = "select iRuta,Latitud as latitude,Longitud as longitude,iRepartidor " .
                    "from rutas " .
                    "where iRepartidor=$reparto->iRepartidor and created_at between '$reparto->HoraSalida' and NOW()";
            } else {
                $query = "select iRuta,Latitud as latitude,Longitud as longitude,iRepartidor " .
                    "from rutas " .
                    "where iRepartidor=$reparto->iRepartidor and created_at between '$reparto->HoraSalida' and '$reparto->HoraEntrega'";
            }
            $ruta = DB::select(DB::raw($query));
        } catch (Exception $e) {
            return JelpController::CustomError('Reparto no encontrado.');
        }
        return JelpController::Respuesta(['RutaReparto' => $ruta]);
    }

    /*GET - Numero de ventas en cadena agrupado por Sucursal*/
    public function getVentasCadena(Request $request)
    {
        try {
            $query = Venta::select(DB::raw('sum(ventas.Importe) as Total, sucursales.NumeroSucursal,  sucursales.Sucursal'))
                ->join('sucursales', 'ventas.iSucursal', '=', 'sucursales.iSucursal')
                ->where('ventas.iEstatus', '=', $this::ENTREGADO)
                ->orderBy('sucursales.Sucursal', 'ASC');

            if ($request->exists('Inicio')) {
                $fechaInicial = $request->input('Inicio');
                $fechaFinal = $request->input('Fin');
                $query = $query->whereRaw("Date(ventas.created_at) BETWEEN '$fechaInicial' AND '$fechaFinal'");
            } else {
                $query = $query->whereRaw("Date(ventas.created_at) = CURDATE()");
            }

            $ventas = $query->groupBy('ventas.iSucursal')->get();

            return JelpController::Respuesta($ventas);
        } catch (Exception $e) {
            return JelpController::CustomError($e->getMessage());
        }
    }

    /* GET Numero de entregas en cadena, agrupado por sucursal  */
    public function getEntregasCadena(Request $request)
    {
        try {

            $query = Reparto::select(DB::raw('count(ventas.iVenta) as `Total`, ventas.iSucursal, sucursales.Sucursal'))
                ->join('ventas', 'repartos.iVenta', '=', 'ventas.iVenta')
                ->join('sucursales', 'ventas.iSucursal', '=', 'sucursales.iSucursal')
                ->where('ventas.iEstatus', '=', $this::ENTREGADO);

            if ($request->exists('Inicio')) {
                $fechaInicio = $request->input('Inicio');
                $fechaFin = $request->input('Fin');
                $query = $query->whereRaw("date(ventas.created_at) between '".$fechaInicio."' and '".$fechaFin."'");
            } else {
                $query = $query->whereRaw("date(ventas.created_at)=CURDATE()");
            }
            $entregas = $query->groupBy('ventas.iSucursal')
                ->OrderBy('Sucursal')
                ->get();

            if (count($entregas) > 0) {
                return JelpController::Respuesta($entregas);
            } else {
                return JelpController::Respuesta([]);
            }
        } catch (Exception $e) {
            return JelpController::CustomError($e->getMessage());
        }
    }

    /*Total de Entregas agrupado por rango de horario*/
    public function getEntregasPorHorario(Request $request)
    {
        try {
            $query = DB::table('repartos')
                ->selectRaw('COUNT(repartos.iReparto) as Total, concat(hour(repartos.HoraEntrega),\'-\',hour(repartos.HoraEntrega)+1) as Rango, repartos.HoraEntrega')
                ->join('ventas', 'repartos.iVenta', '=', 'ventas.iVenta')
                ->join('sucursales', 'ventas.iSucursal', '=', 'sucursales.iSucursal')
                ->where('ventas.iEstatus', '=', self::ENTREGADO)
                ->groupBy(DB::raw('hour(repartos.HoraEntrega)'))
                ->orderBy('Rango', 'ASC');

            if ($request->get('Inicio')) {
                $fechaInicio = INPUT::GET('Inicio');
                $fechaFin = INPUT::GET('Fin');

                $query->whereRaw("date(ventas.created_at) BETWEEN '$fechaInicio' AND '$fechaFin'");
            } else {
                $query->whereRaw("date(ventas.created_at) = CURDATE()");
            }

            return $query->get();
        } catch (Exception $e) {
            return JelpController::CustomError($e->getMessage());
        }
    }

    /*Tiempo promedio de entrega agrupado por sucursal*/
    public function getPromediosCadena(Request $request)
    {
        try {

            $query = Venta::selectRaw('count(ventas.iVenta) as TotalVentas, NumeroSucursal, Sucursal,
                SUM(Time_to_sec(Timediff(repartos.horaentrega, repartos.horasalida))) / Count(repartos.iReparto) as Segundos')
                ->join('repartos', 'ventas.iVenta', '=', 'repartos.iVenta')
                ->join('sucursales', 'ventas.iSucursal', '=', 'sucursales.iSucursal')
                ->where('iEstatus', '=', $this::ENTREGADO);

            if ($request->exists('Inicio')) {
                $fechaInicio = $request->input('Inicio');
                $fechaFin = $request->input('Fin');
                $query = $query->whereRaw("date(ventas.created_at) between '".$fechaInicio."' and '".$fechaFin."'");
            } else {
                $query = $query->whereRaw("date(ventas.created_at)=CURDATE()");
            }

            $ventas = $query->groupBy('ventas.iSucursal')
                ->OrderBy('Sucursal')
                ->get();

            if (count($ventas) > 0) {
                return JelpController::Respuesta($ventas);
            } else {
                return JelpController::Respuesta([]);
            }

        } catch (Exception $e) {
            return JelpController::CustomError($e->getMessage());
        }

    }

    /*Obtener promedios de cadena por rangos de horario*/
    public function getPromediosPorHorario(Request $request)
    {
        try {
            $query = Venta::selectRaw("avg(time_to_sec(timediff(repartos.HoraEntrega,repartos.HoraSalida))) as Segundos,
                concat(hour(HoraEnterado),'-',hour(HoraEnterado)+1) as Rango")
                ->join('repartos', 'ventas.iVenta', '=', 'repartos.iVenta')
                ->where('ventas.iEstatus', '=', $this::ENTREGADO);

            if ($request->exists('Inicio')) {
                $fechaInicio = $request->input('Inicio');
                $fechaFin = $request->input('Fin');
                $query = $query->whereRaw("date(ventas.created_at) between '".$fechaInicio."' and '".$fechaFin."'");
            } else {
                $query = $query->whereRaw("date(ventas.created_at) = CURDATE()");
            }

            $promedios = $query->groupBy('Rango')
                ->OrderBy('Rango')
                ->get();

            return JelpController::Respuesta($promedios);

        } catch (Exception $e) {
            return JelpController::CustomError($e->getMessage());
        }
    }

    /*2.2 Entregas de Sucursal por rango de horario*/
    public function getEntregasSucursal()
    {
        try {
            $fechaInicio = INPUT::GET('Inicio');
            $fechaFin = INPUT::GET('Fin');
            $noSucursal = INPUT::GET('Sucursal');
            if (!empty($fechaInicio) && !empty($fechaFin) && !empty($noSucursal)) {
                //En base a la sucursal, buscamos la iSucursal
                $sucursal = Sucursal::where('NumeroSucursal', '=', $noSucursal)->firstOrFail();
                $entregas = DB::Select(DB::raw("select count(iReparto) as Total,s.NumeroSucursal,s.Sucursal,r.HoraEnterado as FechaHora,concat(hour(HoraEnterado),'-',hour(HoraEnterado)+1) as Rango
                                         from repartos r
                                         join sucursales s on r.iSucursal=s.iSucursal
                                         where r.iSucursal=$sucursal->iSucursal and r.HoraEnterado between '$fechaInicio' and '$fechaFin'
                                         group by HOUR(r.HoraEnterado),DAY(r.HoraEnterado)"));
                return JelpController::Respuesta($entregas);
            } elseif (!empty($noSucursal) && empty($fechaInicio) && empty($fechaFin)) {
                $sucursal = Sucursal::where('NumeroSucursal', '=', $noSucursal)->firstOrFail();
                $entregas = DB::Select(DB::raw("select count(iReparto) as Total,s.NumeroSucursal,s.Sucursal,r.HoraEnterado as FechaHora, concat(hour(HoraEnterado),'-',hour(HoraEnterado)+1) as Rango
                                          from repartos r
                                          join sucursales s on r.iSucursal=s.iSucursal
                                          where r.iSucursal=$sucursal->iSucursal and DATE(r.HoraEnterado)=CURDATE()
                                          group by HOUR(r.HoraEnterado),DAY(r.HoraEnterado)"));
                return JelpController::Respuesta($entregas);
            } else {
                return JelpController::CustomError('Sucursal es Obligatorio,Fechas de Inicio y Fin son opcionales');
            }
        } catch (Exception $e) {
            return JelpController::CustomError($e->getMessage());
        }
    }

    /*Promedio de entrega en sucursal*/
    public function getPromedioSucursal()
    {
        try {
            $fechaInicio = INPUT::GET('Inicio');
            $fechaFin = INPUT::GET('Fin');
            $noSucursal = INPUT::GET('Sucursal');
            if (!empty($noSucursal) && empty($fechaInicio) && empty($fechaFin)) {
                $sucursal = Sucursal::where('NumeroSucursal', '=', $noSucursal)->firstOrFail();
                $promedios = DB::Select(DB::RAW("select sec_to_time(avg(time_to_sec(timediff(HoraEntrega,HoraSalida)))) as Promedio, HoraEnterado as FechaHora
                                             from repartos
                                             where iSucursal=$sucursal->iSucursal and DATE(HoraEnterado) = CURDATE()
                                             group by HOUR(HoraEnterado)"));
                return JelpController::Respuesta($promedios);
            } elseif (!empty($noSucursal) && !empty($fechaInicio) && !empty($fechaFin)) {
                $sucursal = Sucursal::where('NumeroSucursal', '=', $noSucursal)->firstOrFail();
                $promedios = DB::Select(DB::RAW("select sec_to_time(avg(time_to_sec(timediff(HoraEntrega,HoraSalida)))) as Promedio, HoraEnterado as FechaHora
                                           from repartos
                                           where iSucursal=$sucursal->iSucursal and DATE(HoraEnterado) between '$fechaInicio' and '$fechaFin'
                                           group by HOUR(HoraEnterado),DAY(HoraEnterado)
                                           order by HoraEnterado DESC"));
                return JelpController::Respuesta($promedios);
            } else {
                return JelpController::CustomError('Sucursal es Obligatorio,Fechas de Inicio y Fin son opcionales');
            }
        } catch (Exception $e) {
            return JelpController::CustomError($e->getMessage());
        }
    }

    /*Entregas en sucursal*/
    public function getSucursalEntregas()
    {
        try {
            $fechaInicio = INPUT::GET('Inicio');
            $fechaFin = INPUT::GET('Fin');
            $noSucursal = INPUT::GET('Sucursal');
            if (!empty($noSucursal) && empty($fechaInicio) && empty($fechaFin)) {
                $sucursal = Sucursal::where('NumeroSucursal', '=', $noSucursal)->firstOrFail();
                $entregas = DB::Select(DB::raw("select count(iVenta) as Total,Importe,iSucursal,created_at as FechaHora
                                         from ventas where iSucursal=$sucursal->iSucursal and DATE(created_at) = CURDATE()
                                         group by Importe,HOUR(created_at)"));
                return JelpController::Respuesta($entregas);
            } elseif (!empty($noSucursal) && !empty($fechaInicio) && !empty($fechaFin)) {
                $sucursal = Sucursal::where('NumeroSucursal', '=', $noSucursal)->firstOrFail();
                $entregas = DB::Select(DB::raw("select count(iVenta) as Total,Importe,iSucursal,created_at
                                          from ventas where iSucursal=$sucursal->iSucursal and date(created_at) between '$fechaInicio' and '$fechaFin'
                                          group by HOUR(created_at),day(created_at),Importe"));
                return JelpController::Respuesta($entregas);
            } else {
                return JelpController::CustomError('Numero de Sucursal es Requerido, Fecha Inicio y Fin son Opcionales');
            }

        } catch (Exception $e) {
            return JelpController::CustomError($e->getMessage());
        }
    }

    /*Entregas Agente*/
    public function getEntregasAgente()
    {
        $noEmpleado = INPUT::GET('Empleado');
        $fechaInicio = INPUT::GET('Inicio');
        $fechaFin = INPUT::GET('Fin');

        try {
            if (!empty($noEmpleado) && empty($fechaInicio) && empty($fechaFin)) {
                $repartidor = DB::table('vw_repartidores')->where('NoEmpleado', '=', $noEmpleado)->get();
                $entregas = DB::select(DB::raw("select count(repartos.iReparto) as Total,vw.NoEmpleado,vw.Nombre,HoraEnterado as FechaHora,concat(hour(HoraEnterado),'-',hour(HoraEnterado)+1) as Rango
                                          from repartos join vw_repartidores vw on repartos.iRepartidor=vw.iRepartidor
                                          where repartos.iRepartidor=" . $repartidor[0]->iRepartidor . " and DATE(HoraEnterado)=curdate()"));
                return JelpController::Respuesta($entregas);
            } elseif (!empty($noEmpleado) && !empty($fechaInicio) && !empty($fechaFin)) {
                $repartidor = DB::table('vw_repartidores')->where('NoEmpleado', '=', $noEmpleado)->get();
                $entregas = DB::select(DB::raw("select count(repartos.iReparto) as Total,vw.NoEmpleado,vw.Nombre,HoraEnterado as FechaHora,concat(hour(HoraEnterado),'-',hour(HoraEnterado)+1) as Rango
                                          from repartos join vw_repartidores vw on repartos.iRepartidor=vw.iRepartidor
                                          where repartos.iRepartidor=" . $repartidor[0]->iRepartidor . " and DATE(HoraEnterado) between '$fechaInicio' and '$fechaFin'
                                          group by HOUR(HoraEnterado),day(HoraEnterado)"));
                return JelpController::Respuesta($entregas);
            } else {
                return JelpController::CustomError('Numero de Empleado es Requerido, Fecha Inicio y Fin son Opcionales');
            }
        } catch (Exception $e) {
            return JelpController::CustomError($e->getMessage());
        }
    }

    public function getVentasAgente()
    {
        $noEmpleado = INPUT::GET('Empleado');
        $fechaInicio = INPUT::GET('Inicio');
        $fechaFin = INPUT::GET('Fin');

        try {
            if (!empty($noEmpleado) && empty($fechaInicio) && empty($fechaFin)) {
                $repartidor = DB::table('vw_repartidores')->where('NoEmpleado', '=', $noEmpleado)->get();
                $ventas = DB::select(DB::raw("select count(r.iReparto) as Total, v.Importe,HoraEnterado as FechaHora from repartos r
                                        join ventas v on r.iVenta=v.iVenta
                                        where date(r.HoraEnterado)=CURDATE() and r.iRepartidor=" . $repartidor[0]->iRepartidor . "
                                        group by v.Importe,HOUR(HoraEnterado)"));
                return JelpController::Respuesta($ventas);
            } elseif (!empty($noEmpleado) && !empty($fechaInicio) && !empty($fechaFin)) {
                $repartidor = DB::table('vw_repartidores')->where('NoEmpleado', '=', $noEmpleado)->get();
                $fecha = DB::select(DB::raw("select count(r.iReparto) as Total, v.Importe,HoraEnterado as FechaHora from repartos r
                                       join ventas v on r.iVenta=v.iVenta
                                       where (date(r.HoraEnterado) between '$fechaInicio' and '$fechaFin') and r.iRepartidor=" . $repartidor[0]->iRepartidor . "
                                       group by DAY(HoraEnterado), v.Importe"));
                return JelpController::Respuesta($fecha);
            } else {
                return JelpController::CustomError('Numero de Empleado es Requerido, Fecha Inicio y Fin son Opcionales');
            }
        } catch (Exception $e) {
            return JelpController::CustomError($e->getMessage());
        }
    }

    public function getEntregasVentas()
    {
        $noEmpleado = INPUT::GET('Empleado');
        $fechaInicio = INPUT::GET('Inicio');
        $fechaFin = INPUT::GET('Fin');

        try {
            if (!empty($noEmpleado) && empty($fechaInicio) && empty($fechaFin)) {
                $repartidor = DB::table('vw_repartidores')->where('NoEmpleado', '=', $noEmpleado)->get();
                $venta = DB::select(DB::raw("select count(r.iReparto) as Total, v.Importe,HoraEnterado as FechaHora
                                       from repartos r
                                       join ventas v on r.iVenta=v.iVenta
                                       where v.iEstatus=4 and date(r.HoraEnterado)=CURDATE() and r.iRepartidor=" . $repartidor[0]->iRepartidor . "
                                       group by v.Importe,HOUR(HoraEnterado)"));
                return JelpController::Respuesta($venta);
            } elseif (!empty($noEmpleado) && !empty($fechaInicio) && !empty($fechaFin)) {
                $repartidor = DB::table('vw_repartidores')->where('NoEmpleado', '=', $noEmpleado)->get();
                $fecha = DB::select(DB::raw("select count(r.iReparto) as Total, v.Importe,HoraEnterado as FechaHora from repartos r
                                       join ventas v on r.iVenta=v.iVenta
                                       where v.iEstatus=4 and (date(r.HoraEnterado) between '$fechaInicio' and '$fechaFin') and r.iRepartidor=" . $repartidor[0]->iRepartidor . "
                                       group by DAY(HoraEnterado), v.Importe"));
                return JelpController::Respuesta($fecha);
            } else {
                return JelpController::CustomError('Numero de Empleado es Requerido, Fecha Inicio y Fin son Opcionales');
            }
        } catch (Exception $e) {
            return JelpController::CustomError($e->getMessage());
        }

    }

    public function getVentaTotal()
    {
        $fechaInicio = INPUT::GET('Inicio');
        $fechaFin    = INPUT::GET('Fin');

        if(!$this->validateDate($fechaInicio) || !$this->validateDate($fechaFin)) {
            return JelpController::CustomError('Los parametros Inicio y Fin no son');
        }

        try {
            $stats = DB::table('ventas')
                ->select(DB::raw('AVG(Importe) as Promedio, COUNT(Importe) Entregados, SUM(Importe) as Total, COUNT(DISTINCT Telefono) as Clientes, SUM(Importe)/COUNT(DISTINCT iSucursal) as PromedioSucursal'))
                ->where('iEstatus', '=', $this::ENTREGADO)
                ->whereRaw("date(created_at) between '$fechaInicio' and '$fechaFin'")
                ->get();

            if(!$stats) {
                return [];
            }

            $sumatoria        = number_format($stats[0]->Total, 2, '.', '');
            $promedioTotal    = number_format($stats[0]->Promedio, 2, '.', '');
            $promedioSucursal = number_format($stats[0]->PromedioSucursal, 2, '.', '');
        } catch (Exception $e) {
            return JelpController::CustomError($e->getMessage());
        }

        return JelpController::Respuesta([
            'Total'            => $sumatoria,
            'Promedio'         => $promedioTotal,
            'Clientes'         => $stats[0]->Clientes,
            'Entregados'       => $stats[0]->Entregados,
            'PromedioSucursal' => $promedioSucursal
        ]);
    }

    public function getGraph()
    {
        $fechaInicio = INPUT::GET('Inicio');
        $fechaFin = INPUT::GET('Fin');

        if(!$this->validateDate($fechaInicio) || !$this->validateDate($fechaFin)) {
            return JelpController::CustomError('Los parametros Inicio y Fin no son');
        }

        try {
            // Obtener total de ventas agrupado por sucursal y por día
            $elementos = DB::table('ventas')
                ->select(DB::raw('count(Importe) as Ventas, sum(Importe) as Total, DATE_FORMAT(created_at,\'%Y-%m-%d\') as fecha, iSucursal'))
                ->whereRaw("date(created_at) between '$fechaInicio' and '$fechaFin'")
                ->where('iEstatus', '=', $this::ENTREGADO)
                ->groupBy('iSucursal')
                ->groupBy(DB::raw('DATE_FORMAT(created_at,\'%Y-%m-%d\')'))
                ->orderBy(DB::raw('DATE_FORMAT(created_at,\'%Y-%m-%d\')'), 'ASC')
                ->get();

            $days = [];

            // Agrupar los resultados por dia con su respectiva suma
            foreach($elementos as $elemento) {
                if(!isset($days[$elemento->fecha])) {
                    $days[$elemento->fecha] = [
                        'totalVenta'          => 0,
                        'Total'               => 0,
                        'sucursalesCount'     => 0,
                        'ventasCount'         => 0,
                        'canceladas'          => 0
                    ];
                }

                $days[$elemento->fecha]['totalVenta']          += $elemento->Total;
                $days[$elemento->fecha]['ventasCount']         += $elemento->Ventas;
                $days[$elemento->fecha]['sucursalesCount']++;
            }

            // Obtener las ventas canceladas
            $ordenesCanceladas = DB::table('ventas')
                ->select(DB::raw('sum(Importe) as Importe, DATE_FORMAT(created_at,\'%Y-%m-%d\') as fecha'))
                ->where('iEstatus', '=', $this::CANCELADO)
                ->whereRaw("date(created_at) between '$fechaInicio' and '$fechaFin'")
                ->groupBy(DB::raw('DATE_FORMAT(created_at,\'%Y-%m-%d\')'))->get();

            // Agregar monto de ordenes canceladas a los dias
            foreach($ordenesCanceladas as $cancelada) {
                if(!isset($days[$cancelada->fecha])) {
                    $days[$cancelada->fecha] = [
                        'totalVenta'            => 0,
                        'Total'                 => 0,
                        'sucursalesCount'       => 0,
                        'ventasCount'           => 0
                    ];
                }

                $days[$cancelada->fecha]['canceladas'] = $cancelada->Importe;
            }

            $result = [];

            // Calculo final de promedios para regresar el resultado
            foreach($days as $fecha => $day) {
                $result[] = [
                    'VentaTotal'       => $day['totalVenta'],
                    'ImporteCancelado' => $day['canceladas'],
                    'PromedioSucursal' => $day['totalVenta'] / $day['sucursalesCount'],
                    'TicketPromedio'   => $day['totalVenta'] / $day['ventasCount'],
                    'Fecha'            => $fecha
                ];
            }

            return $result;
        } catch (Exception $e) {
            return JelpController::CustomError($e->getMessage());
        }
    }

    //Metodo para el pie chart que retorna en diferentes grupos las ventas relizadas
    public function getGroupOfSales(Request $request)
    {
        $fechaInicio = INPUT::GET('Inicio');
        $fechaFin    = INPUT::GET('Fin');

        if(!$this->validateDate($fechaInicio) || !$this->validateDate($fechaFin)) {
            return JelpController::CustomError('Los parametros Inicio y Fin no son');
        }

        $collection = Venta::where('iEstatus', '=', self::ENTREGADO)
            ->whereRaw("date(created_at) between '$fechaInicio' and '$fechaFin'")
            ->get();

        $minusOne = $collection->filter(function ($item) {
            if ($item->Importe <= 100)
                return $item;
        });


        $minusFive = $collection->filter(function ($item) {
            if ($item->Importe > 100 && $item->Importe <= 500)
                return $item;
        });

        $minusTen = $collection->filter(function ($item) {
            if ($item->Importe > 500 && $item->Importe <= 1000)
                return $item;
        });

        $plusTen = $collection->filter(function ($item) {
            if ($item->Importe > 1000)
                return $item;
        });

        $result = collect([
            ['Titulo' => '$1 - $100', 'Total' => count($minusOne)],
            ['Titulo' => '$101 - $500', 'Total' => count($minusFive)],
            ['Titulo' => '$501 - $1000', 'Total' => count($minusTen)],
            ['Titulo' => '$1000+', 'Total' => count($plusTen)]
        ]);
        return JelpController::Respuesta($result);
    }

    public function getTransfers()
    {
        $fechaInicio = INPUT::GET('Inicio');
        $fechaFin    = INPUT::GET('Fin');

        if(!$this->validateDate($fechaInicio) || !$this->validateDate($fechaFin)) {
            return JelpController::CustomError('Los parametros Inicio y Fin no son');
        }

        $sales = Venta::select('ventas.iVenta', 'Traspaso', 'repartos.iVenta')
            ->where('iEstatus', '=', self::ENTREGADO)
            ->whereRaw("date(ventas.created_at) between '$fechaInicio' and '$fechaFin'")
            ->join('repartos', 'ventas.iVenta', '=', 'repartos.iVenta')
            ->get();

        $traspasos = $sales->filter(function ($item) {
            if ($item->Traspaso != 0) {
                return $item;
            }
        });

        $transferCount = count($traspasos);

        $result = collect([
            ['Titulo' => 'Ventas Estandar', 'Total' => (count($sales) - $transferCount)],
            ['Titulo' => 'Ventas Trasferidas', 'Total' => ($transferCount)]
        ]);

        return JelpController::Respuesta($result);
    }

    public function getDelivieries()
    {
        $fechaInicio = INPUT::GET('Inicio');
        $fechaFin    = INPUT::GET('Fin');

        if(!$this->validateDate($fechaInicio) || !$this->validateDate($fechaFin)) {
            return JelpController::CustomError('Los parametros Inicio y Fin no son');
        }

        try {

            $pendientes = Venta::where('iEstatus', '=', self::PENDIENTE)
                ->whereRaw("date(created_at) between '$fechaInicio' and '$fechaFin'")
                ->count();

            $entregados = Venta::where('iEstatus', '=', self::ENTREGADO)
                ->whereRaw("date(created_at) between '$fechaInicio' and '$fechaFin'")
                ->count();

            $aceptados = Venta::where('iEstatus', '=', self::ACEPTADO)
                ->whereRaw("date(created_at) between '$fechaInicio' and '$fechaFin'")
                ->count();

            $enTransito = Venta::where('iEstatus', '=', self::TRANSITO)
                ->whereRaw("date(created_at) between '$fechaInicio' and '$fechaFin'")
                ->count();

            $cancelados = Venta::where('iEstatus', '=', self::CANCELADO)
                ->whereRaw("date(created_at) between '$fechaInicio' and '$fechaFin'")
                ->count();

            return JelpController::Respuesta([
                'Pendiente'  => $pendientes,
                'Entregados' => $entregados,
                'Aceptados'  => $aceptados,
                'Transito'   => $enTransito,
                'Cancelados' => $cancelados
            ]);

        } catch (Exception $e) {
            return JelpController::CustomError($e->getMessage());
        }
    }

    public function getCancelSales()
    {
        $fechaInicio = INPUT::GET('Inicio');
        $fechaFin    = INPUT::GET('Fin');

        if(!$this->validateDate($fechaInicio) || !$this->validateDate($fechaFin)) {
            return JelpController::CustomError('Los parametros Inicio y Fin no son validos');
        }

        $collection = Venta::whereRaw("date(created_at) between '$fechaInicio' and '$fechaFin'")
            ->whereIn('iEstatus', [ self::ENTREGADO, self::CANCELADO ])
            ->get();

        $canceladas = $collection->filter( function($item) {
            if($item->iEstatus == self::CANCELADO) {
                return $item;
            }
        });

        $total = count($collection);
        $canceladas = count($canceladas);

        $result = collect([
            ['Titulo' => 'Ventas Terminadas', 'Total' => $total - $canceladas],
            ['Titulo' => 'Ventas Canceladas', 'Total' => $canceladas]
        ]);

        return JelpController::Respuesta($result);
    }

    public function getCancelReasons()
    {
        $fechaInicio = INPUT::GET('Inicio');
        $fechaFin    = INPUT::GET('Fin');

        if(!$this->validateDate($fechaInicio) || !$this->validateDate($fechaFin)) {
            return JelpController::CustomError('Los parametros Inicio y Fin no son validos');
        }

        $collection = Venta::select('iComentario',DB::raw('count(repartos.iReparto) as totales'))
            ->where('iEstatus', '=', self::CANCELADO)
            ->rightJoin('repartos', 'ventas.iVenta', '=', 'repartos.iVenta')
            ->whereRaw("date(ventas.created_at) between '$fechaInicio' and '$fechaFin'")
            ->groupBy('repartos.iComentario')
            ->get();

        $comentarios = Comentario::select('iComentario','Comentario')->get();

        foreach($comentarios as $comentario) {
            $comentario->Total = 0;

            foreach ($collection as $collect) {
                if ($comentario->iComentario == $collect->iComentario) {
                    $comentario->Total = $collect->totales;
                    break;
                }
            }
        }

        return JelpController::Respuesta($comentarios);
    }

    /**
     * @param Request $request
     */
    public function posicionventas(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'Inicio' => 'Required',
            'Fin' => 'Required',
            'Estado' => 'Required'
        ]);

        if($validator->fails()){
            return JelpController::CustomError('Parametros incompletos Inicio,Fin,Estatus');
        }

        $estatus = $request->input('Estado');
        $fechaInicial = $request->input('Inicio');
        $fechaFinal = $request->input('Fin');

        $estatus = "v.iEstatus=$estatus AND";

        $query = sprintf($this::QUERY_VENTAS, $estatus,$fechaInicial, $fechaFinal);
        $respuesta = DB::select(DB::raw($query));

        return JelpController::Respuesta(['Ventas'=> $respuesta]);
    }

    public function ventasPendientes($iUsuario)
    {
        try
        {
            //obtener ventas en pendiente
            $repartidor = Repartidor::where('iUsuario','=',$iUsuario)->first();
            if(!is_null($repartidor))
            {
                $asignacion = Asignacion::where('iRepartidor','=',$repartidor->iRepartidor)->orderBy('created_at','DESC')->first();
            }else{
                throw new Exception("No Hay Asignacion Disponible");
            }


            if(!is_null($asignacion))
                $ventas = Venta::where('iSucursal','=',$asignacion->iSucursal)->whereRaw('DATE(created_at)=CURDATE()')->where('iEstatus','=',self::PENDIENTE)->get();

            if(count($ventas)>0)
            {
                $dispositivo = Dispositivo::where('iDispositivo','=',$asignacion->iDispositivo)->first();
                Log::create(array('Mensaje' => '/ventas/pendientes',
                    'Request' => 'iUsuario:'.$iUsuario.' IMEI:'.$dispositivo->IMEI,
                    'Response' => 'Respuesta',
                    'Detalle' => 'Numero de Ventas:'.count($ventas),
                    'Origen' => $this::API));
            }
            return JelpController::Respuesta(['Total'=>count($ventas),'Ventas'=>$ventas]);
        }
        catch(Exception $e)
        {
            return JelpController::CustomError($e->getMessage());
        }

    }
}
