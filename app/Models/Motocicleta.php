<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Motocicleta extends Model{

    use SoftDeletes;
    protected $table = 'motocicletas';
    protected $primaryKey = 'iMotocicleta';
    protected $dates = ['deleted_at'];
    protected $fillable = array('Marca', 'Modelo', 'Placas', 'Placas', 'iEstatus');
    protected $visible = array('iMotocicleta', 'Marca', 'Modelo', 'Placas',
        'Estatus', 'created_at','updated_at', 'Sucursal','Bitacora','iEstatus','estatus');

    const STATUS_ACTIVE = 1;
    const STATUS_MAINTENANCE = 3;

    /**
     * Relacion con Estatus
     */
    public function Estatus(){
        return $this->belongsTo('App\Models\EstatusMotocicleta', 'iEstatus', 'iEstatus');
    }

    public function Repartidor(){
        return $this->hasMany('Repartidor');
    }

    public function ultimaAsignacion()
    {
        return $this->hasMany('App\Models\Asignacion', 'iMotocicleta');
    }

    public function Bitacora()
    {
        return $this->hasOne('App\Models\Bitacora', 'iMotocicleta')->latest();
    }
}