<?php

namespace App\Models;

use App\Lib\ParseCalls;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Exception;

class Mantenimiento extends Model{
    protected $table='mantenimientos';
    protected $primaryKey = 'iMantenimiento';
    protected $guarded = array('iMantenimiento');
    public $timestamps = true;


    protected $fillable = array('iUsuario', 'iMotocicleta', 'Total', 'Servicio', 'iMttoEstatus', 'iMecanico', 'iSucursal',
        'Kilometraje','FechaEntrada', 'FechaSalida','HoraEntrada','HoraSalida','Observacion');

    protected $visible = array('iMantenimiento', 'iUsuario', 'iMotocicleta', 'Total', 'Servicio',
        'Kilometraje','FechaEntrada', 'FechaSalida','HoraEntrada','HoraSalida','Observacion',
        'Usuario', 'Servicios', 'Mecanico', 'Motocicleta', 'Detalle', 'Sucursal','iMttoEstatus', 'Estatus');

    public function detalle(){
        return $this->hasMany('App\Models\Mtto_articulo_detalle', 'iMantenimiento');
    }

    public function motocicleta(){
        return $this->belongsTo('App\Models\Motocicleta', 'iMotocicleta')->select(['iMotocicleta', 'Marca', 'Modelo', 'Placas']);
    }

    public function servicio(){
        return $this->hasMany('App\Models\Mtto_servicio_detalle', 'iMantenimiento');
    }

    public function mecanico(){
        return $this->belongsTo('App\Models\Usuario', 'iMecanico', 'iUsuario')->select(['iUsuario', 'Nombre', 'Apellidos'])
            ->withTrashed();
    }

    public function usuario(){
        return $this->belongsTo('App\Models\Usuario', 'iUsuario')
            ->select(['iUsuario', 'Nombre', 'Apellidos', 'NoEmpleado'])
            ->withTrashed();
    }

    public function sucursal(){
        return $this->belongsTo('App\Models\Sucursal', 'iSucursal')->select(['iSucursal', 'Sucursal', 'NumeroSucursal', 'Telefono', 'Direccion']);
    }

    public function estatus(){
        return $this->belongsTo('App\Models\Mtto_estatus', 'iMttoEstatus');
    }

    public static function search($id){

        $servicios = collect();
        $mantenimiento = Mantenimiento::with('detalle', 'usuario', 'servicio', 'motocicleta', 'sucursal', 'mecanico', 'servicio.servicio', 'estatus')
            ->where('iMantenimiento', '=', $id)
            ->firstOrFail();

        $mantenimiento->Detalle = $mantenimiento->detalle;
        $mantenimiento->Usuario = $mantenimiento->usuario;

        foreach($mantenimiento->servicio as $servicio){
            $serv = $servicio->servicio;
            $serv->ServicioAplicado = (bool) $servicio->ServicioAplicado;
            $servicios->push($serv);
        }

        $mantenimiento->Servicios = $servicios;

        $mantenimiento->Motocicleta = $mantenimiento->motocicleta;

        $mantenimiento->Sucursal =  $mantenimiento->sucursal;

        $mantenimiento->Mecanico = $mantenimiento->mecanico;

        $mantenimiento->Estatus = $mantenimiento->estatus;

        return $mantenimiento;
    }


    public static function saveMaintenanceOrderFirstStep($values, $iMantenimiento = null){

        /*
         * Busca el registro con los siguientes parametros si no lo crea
         * aunque puedan llegar a repetirse algunos datos
         * la fecha de entrada hace la pauta para diferenciar si
         * es un nuevo mantenimiento
         */
        if($iMantenimiento){
            $mantenimiento = Mantenimiento::findOrFail($iMantenimiento);
        }else{
            $mantenimiento = Mantenimiento::firstOrCreate([
                'iMotocicleta'  => $values['iMotocicleta'],
                'iUsuario'      => $values['iUsuario'],
                'iSucursal'     => $values['iSucursal'],
                'FechaEntrada'  => Carbon::now()->toDateString()
            ]);
        }

        $mantenimiento->iMecanico = $values['iMecanico'];
        $mantenimiento->Kilometraje = $values['Kilometraje'];
        $mantenimiento->HoraEntrada = Carbon::now()->format('H:i:s');
        $mantenimiento->Observacion = isset($values['Observaciones']) ? $values['Observaciones'] : '';
        $mantenimiento->Servicio = isset($values['ServiciosExtra']) ? $values['ServiciosExtra'] : '';
        $mantenimiento->iMttoEstatus = Mtto_estatus::STATUS_STARTED;
        $mantenimiento->Iniciado = Carbon::now()->toDateTimeString();
        $mantenimiento->save();
        return $mantenimiento;
    }

    /**
     * @param $values
     * @param $mantenimiento
     * Metodo para almacenar los articulos/servicios
     */
    public static function saveMaintananceOrderSecondStep($values, $mantenimiento)
    {

        //Debido a que pueden enviarse o no los servicios/articulos se valida
        // si estos estan prensentes en el request
        if (isset($values['Servicios'])) {
            $servicios = json_decode($values['Servicios']);

            $count=count($servicios);
            for($i=0; $i<$count; $i++){
                $objServicio = $servicios[$i];
                $servicio = Mtto_servicio::find($objServicio->iMttoServicio);

                if($servicio) {
                    //Se crea una instancia en caso de que se haya equivocado
                    //no duplique entradas en la base de datos
                    $servicio_detalle = Mtto_servicio_detalle::firstOrNew([
                        'iMttoServicio' => $servicio->iMttoServicio,
                        'iMantenimiento' => $mantenimiento->iMantenimiento,
                    ]);

                    //Se le asigna el valor del servicio y se almacena o actualiza
                    //Segun sea el caso
                    if(isset($objServicio->active))
                    {
                        $servicio_detalle->ServicioAplicado = $objServicio->active;
                    }
                    else{
                        $servicio_detalle->ServicioAplicado = false;
                    }
                    
                    $servicio_detalle->save();
                }
            }
        }

        if (isset($values['Articulos'])) {
            $total = 0;
            $articulos = json_decode($values['Articulos']);
            $colleccion_articulos = collect();
            $count = count($articulos);
            for ($i = 0; $i < $count; $i++) {
                $objArticulo = $articulos[$i];
                $articulo = Mtto_articulo::find($objArticulo->description->iMttoArticulo);
                if ($articulo) {
                    $monto = ($articulo->Precio * $objArticulo->qty);
                    //$Clasificacion = $objArticulo->Clasificacion; //queda comentado, para habilitarlo en un futuro

                    //Se crea una instancia sin ser almacenada en base da datos
                    $articulo_detalle = Mtto_articulo_detalle::firstOrNew([
                        'iMttoArticulo' => $articulo->iMttoArticulo,
                        'iMantenimiento' => $mantenimiento->iMantenimiento,
                        'Articulo' => $articulo->Concepto
                    ]);

                    //En caso de ser un nuevo objeto o un articulo ya creado
                    //Se almacena o se actualiza segun el caso
                    $articulo_detalle->Cantidad = $objArticulo->qty;
                    $articulo_detalle->Importe =  $monto;
                    //$articulo_detalle->iClasificacion = $Clasificacion;
                    $articulo_detalle->Precio = $articulo->Precio;
                    $articulo_detalle->save();
                    $total = $total + $monto;
                    $colleccion_articulos->push($articulo->iMttoArticulo);
                }
            }
            $mantenimiento->Total = $total;
            $mantenimiento->save();

            Mtto_articulo_detalle::whereNotIn('iMttoArticulo', $colleccion_articulos)
                ->where('iMantenimiento', '=', $mantenimiento->iMantenimiento)
                ->delete();
        }
    }

    /**
     * @param $values
     * @param $mantenimiento
     * @param string $timezone
     */
    public static function saveMaintananceOrderThirdStep($mantenimiento, $timezone = 'America/Tijuana'){
        $date = Carbon::now($timezone)->toDateString();
        $mantenimiento->FechaSalida = $date;
        $mantenimiento->HoraSalida = Carbon::now()->format('H:i:s');
        $mantenimiento->iMttoEstatus = Mtto_estatus::STATUS_ENDED;
        $mantenimiento->Cerrado = Carbon::now()->toDateTimeString();
        $mantenimiento->save();

        $motocicleta = Motocicleta::find($mantenimiento->iMotocicleta);
        $motocicleta->iEstatus = Motocicleta::STATUS_ACTIVE;
        $motocicleta->save();

        return $mantenimiento;
    }
}