<?php

namespace App\Http\Controllers;

use App\Jobs\NotificarTransito;
use App\Lib\ErrorCodes;
use App\Lib\GeoCalculator;
use App\Models\Cliente;
use App\Models\ClienteEntrega;
use App\Models\Configuracion;
use App\Models\Estatus;
use App\Models\Estatus_repartidor;
use App\Models\Motocicleta;
use App\Models\Sucursal;
use DateTime;
use Laravel\Lumen\Routing\Controller as BaseController;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Illuminate\Database\Query\Builder;

use App\Http\Controllers\JelpController;

use App\Models\Reparto;
use App\Models\Venta;
use App\Models\Repartidor;
use App\Models\Asignacion;
use App\Models\Ruta;
use App\Models\Log;
use App\Models\Usuario;

use Validator;
use Exception;

class RepartosController extends JelpController
{

    const VENTA_PENDIENTE = 1;
    const VENTA_ACEPTADA = 2;
    const VENTA_ENPROCESO = 3;
    const VENTA_TERMINADA = 4;
    const VENTA_CANCELADA = 5;

    const MOVIL = 'MOVIL';
    const CRM = 'CRM';
    const API = 'API';

    const QUERY_REPARTOS = <<<EOD
SELECT v.iventa,
			 v.folio,
			 v.Telefono,
			 v.importe,
			 v.latitud,
			 v.longitud,
			 v.isucursal,
			 v.firma,
			 e.estatus,
			 s.numerosucursal,
			 s.Sucursal,
			 v.created_at AS FechaVenta,
			 r.ireparto,
			 r.horaenterado,
			 r.horasalida,
			 r.horaentrega,
			 u.nombre,
			 u.apellidos,
			 u.noempleado,
			 c.comentario,
			 r.traspaso
FROM   ventas v
			 JOIN estatus e
				 ON v.iestatus = e.iestatus
			 LEFT OUTER JOIN sucursales s
										ON v.isucursal = s.isucursal
			 LEFT OUTER JOIN repartos r
										ON v.iventa = r.iventa
			 LEFT OUTER JOIN repartidores rr
				 ON rr.irepartidor = r.irepartidor
			 LEFT OUTER JOIN usuarios u
				 ON rr.iusuario = u.iusuario
			LEFT OUTER JOIN comentarios c
				 ON r.iComentario=c.iComentario
			WHERE DATE(v.created_at) BETWEEN '%s' AND '%s'
			ORDER BY v.created_at DESC;
EOD;

    //Retorna todos los repartos de la vista de la base de datos, en base a ello se filtran para obtener los que
    //que estan en proceso y pertenecen a al repartidor
    public function getRepartosEnProceso()
    {
        $repartos = DB::table('vw_repartos')->where('Estatus', '=', 'En Proceso')->get();

        //Retorna los repartos en formato JSON
        echo json_encode(['repartos' => $repartos]);
    }

    /*
     * Metodo para cancelar el envio en caso de que por x razon no se complete la venta
     *
     * method: POST
     *
     * ruta /repartos/cancelar/@iReparto - id del reparto a cancelar
     *
     * parametros: @iComentario - id del comentario de cancelacion
     */
    public function cancelarEnvio(Request $request, $id)
    {
        $inputs = $request->only('iComentario');
        if (!empty($id) && !is_null($id) && is_numeric($id) && !empty($inputs)) {
            //Inicia la transaccion
            DB::beginTransaction();
            try {

                //Instancia del raparto y venta
                $reparto = new Reparto();
                $venta = new Venta();

                //Valida que el estatus de la venta este en proceso
                if ($venta->ventaEnProceso($id)) {

                    $reparto = $reparto->cancelarReparto($id, $inputs);

                    //Si se cancela y se guarda todo bien en la venta
                    if ($venta->cancelarVenta($reparto->iVenta, $inputs)) {

                        //Instancia del re
                        $repartidor = new Repartidor();

                        //Obtiene la cantidad de repartos pendiente y actualiza el repartidor, si se guarda el proceso todo pasa bien
                        if ($repartidor->cantidadRepartosPendientes($reparto->iRepartidor)) {

                            //Hace commit de todos los cambios realizados
                            DB::commit();

                            //Retorna el resultado 200 si todo sale bien
                            echo json_encode(['respuesta' => 200]);

                        } else {
                            DB::commit();
                            throw new Exception('Se ha cancelado la venta, mas no se actualizo es estado del vendedor', 1009);
                        }
                    }
                } else {
                    throw new Exception('La venta que desea modificar no esta en proceso', 1010);
                }

            } catch (Exception $e) {
                //Deshace todo cambio
                DB::rollback();

                //Retornar error
                echo json_encode(['respuesta' => $e->getCode(), 'mensaje' => $e->getMessage()]);
            }
        }
    }

    /*
     * Metodo que finaliza el envio
     *
     * method: POST
     *
     * ruta /repartos/completar/@iReparto - id del reparto a completar
     *
     * parametros: @Firma - base64 de la firma enviada desde el movil
     *
     */
    public function completarEnvio(Request $request, $id)
    {
        $inputs = $request->only('firma');
        if (!empty($id) && !is_null($id) && is_numeric($id) && !empty($inputs)) {
            //Inicia la transaccion
            DB::beginTransaction();
            try {

                //Instancia del raparto
                $reparto = new Reparto();
                $venta = new Venta();

                //Verifica que la venta este en proceso
                if ($venta->ventaEnProceso($id)) {

                    //Completa el reparto
                    $reparto = $reparto->completarReparto($id);

                    //Completa la venta
                    if ($venta->completarVenta($reparto->iVenta, $inputs)) {
                        $repartidor = new Repartidor();

                        //Cambia en dado caso el estatus del repartidor
                        if ($repartidor->cantidadRepartosPendientes($reparto->iRepartidor)) {

                            //Guarda todos los cambios
                            DB::commit();

                            //Retorna el resultado 200 como exitoso
                            echo json_encode(['respuesta' => 200]);

                        } else {
                            DB::commit();
                            throw new Exception('Se ha gaurdado la venta, mas no se actualizo es estado del vendedor', 1004);
                        }
                    } else {
                        throw new Exception('No se ha logrado actualizar la venta', 1003);
                    }
                } else {
                    throw new Exception('La venta que desea modificar no esta en proceso', 1010);
                }


            } catch (Exception $e) {
                //Deshace todo cambio
                DB::rollback();
                //Retornar error
                echo json_encode(['respuesta' => $e->getCode(), 'mensaje' => $e->getMessage()]);
            }
        }
    }

    //Crear un nuevo registro en repartos, apartir del No de Folio escaneado
    public function newReparto(Request $request)
    {
        $inputs = $request->all();
        //tomamos el Folio y lo buscamos en ventas
        $venta = DB::table('ventas')
            ->where('Folio', '=', $inputs['Folio'])
            ->get();

        //Buscamos la sucursal anexada al Repartidor
        $repartidores = DB::table('vw_repartidores')
            ->where('iUsuario', '=', $inputs['iusuario'])
            ->get();

        $repartidor = DB::table('vw_asignaciones')
            ->where('iRepartidor', '=', $repartidores[0]->iRepartidor)
            ->get();

        $sucursal = Sucursal::find($repartidor[0]->iSucursal);

        if(isset($inputs['lat']) && isset($inputs['long'])) {
            $distanciaSucusal = GeoCalculator::getDistanceBetweenPoints($sucursal->Latitud, $sucursal->Longitud, $inputs['lat'], $inputs['long']);

            $metros_de_sucursal = Configuracion::where('Codigo', '=', Reparto::CODIGO_DISTANCIA)->first();

            if ($distanciaSucusal > $metros_de_sucursal) {
                return JelpController::CustomError('Para aceptar un pedido debe estar cerca de la sucursal asginada');
            }
        }

        $reparto = new Reparto();

        try {
            $reparto->iRepartidor = $repartidor[0]->iRepartidor;
            $reparto->iVenta = $venta[0]->iVenta;
            $reparto->iSucursal = $repartidor[0]->iSucursal;

            $datetime = new \DateTime();
            $reparto->HoraSalida = $datetime->format('Y-m-d H:i:s');

            $reparto->save();

            //Actualizar el estatus de venta
            $ventas = new Venta();
            $miventa = $ventas->where('Folio', '=', $inputs['Folio'])->firstOrFail();
            $miventa->iEstatus = 2;
            $miventa->save();

            echo json_encode(['Respuesta' => '200', 'Mensaje' => 'OK']);
        } catch (\Exception $e) {
            echo json_encode(['Respuesta' => '500', 'Mensaje' => $e->getMessage()]);
        }
    }

    //Obtener pendientes por Repartidor
    public function pendientes($iUsuario)
    {
        //$repartidor = Repartidor::where('iUsuario','=',$iUsuario)->first();
        $pendientes = DB::table('vw_repartos')->where('iUsuario', '=', $iUsuario)
            ->where('iEstatus', '=', $this::VENTA_ENPROCESO)
            ->whereRaw('DATE(FechaVenta) >= CURDATE() - INTERVAL 1 DAY')
            ->get();
        echo json_encode(['Repartos' => $pendientes, 'Respuesta' => '200']);
    }

    //obtener compleados por Repartidor
    public function completados($iUsuario)
    {
        $completados = DB::table('vw_repartos')->where('iUsuario', '=', $iUsuario)
            ->where('iEstatus', '=', $this::VENTA_TERMINADA)
            ->whereRaw('DATE(FechaVenta) = CURDATE()')
            ->get();
        echo json_encode(['Repartos' => $completados, 'Respuesta' => '200']);
    }

    //Marcar venta como en proceso cuando escanean el Folio
    public function enproceso(Request $request)
    {
        $inputs = $request->all();

        $validator = Validator::make($request->all(), [
            'iUsuario' => 'required|numeric',
            'Folio' => 'required|alpha_num',
            'Latitud' => 'required',
            'Longitud' => 'required'
        ]);

        if ($validator->fails()) {
            Log::create(array('Mensaje' => 'Error en Validacion',
                'Request' => json_encode($request->all()),
                'Origen' => $this::MOVIL));
            return JelpController::CustomError($validator->errors()->first());
        }
        //Buscamos el reparto
        try {

            $repartidores = Repartidor::where('iUsuario', '=', $inputs['iUsuario'])
                ->first();
            if (count($repartidores) == 0) {
                Log::create(array('iUsuario' => $inputs['iUsuario'],
                    'Mensaje' => 'Usuario Inexistente',
                    'Request' => json_encode($request->all()),
                    'Origen' => $this::MOVIL));
                return JelpController::CustomError('Usuario Inexistente.');
            }

            $asignacion = Asignacion::where('iRepartidor', '=', $repartidores->iRepartidor)
                ->orderBy('created_at','desc')->first();

            if(env('MANTENIMIENTO')==true)
            {
                //Para tener una moto asignada debieron realizar la inspeccion
                $motocicleta = Motocicleta::find($asignacion->iMotocicleta);

                if(!$motocicleta)
                {
                    Log::create(array('iUsuario' => $inputs['iUsuario'],
                        'Mensaje' => 'El usuario: '. $inputs['iUsuario'].' no tiene moto asignada',
                        'Request' => json_encode($request->all()),
                        'Origen' => $this::API));
                    return JelpController::CustomError('No tiene asignada una motocicleta');
                }
            }

            if($repartidores->iEstatus == Estatus_repartidor::IN_MAINTENANCE){
                Log::create(array('iUsuario' => $inputs['iUsuario'],
                    'Mensaje' => 'El usuario: '. $inputs['iUsuario'].' esta en estatus de mantenimiento',
                    'Request' => json_encode($request->all()),
                    'Origen' => $this::API));
                return JelpController::CustomError('Espere a terminar mantenimiento');
            }

            $sucursal = Sucursal::find($asignacion->iSucursal);

            $distanciaSucusal = GeoCalculator::getDistanceBetweenPoints($sucursal->Latitud,
                $sucursal->Longitud, $inputs['Latitud'],
                $inputs['Longitud']);

            $metros_de_sucursal = Configuracion::where('Codigo', '=', Reparto::CODIGO_DISTANCIA)
                ->first();

            //Verifica que este dentro o cerca de la sucursal para poder aceptar el pedido
            if ($distanciaSucusal > $metros_de_sucursal->Valor) {
                Log::create(array('iUsuario' => $inputs['iUsuario'],
                    'Mensaje' => 'Fuera de Rango de Sucursal repartidor: ' . $asignacion->iRepartidor ,
                    'Request' => json_encode($request->all()),
                    'Origen' => $this::MOVIL));

                return JelpController::Forbidden(ErrorCodes::OUT_OF_RANGE_ERROR_CODE);
            }

            //Verifica la existencia del folio
            $venta = Venta::select('iVenta', 'Folio', 'created_at', 'iSucursal', 'iEstatus')
                ->where('Folio', '=', $inputs['Folio'])
                ->with('Reparto')
                ->first();

            if (!$venta) {
                Log::create(array('iUsuario' => $inputs['iUsuario'],
                    'Mensaje' => 'Venta Inexistente',
                    'Request' => json_encode($request->all()),
                    'Origen' => $this::MOVIL));
                return JelpController::CustomError('Venta Inexistente.');
            }

            //Validar que el folio sea de la sucursal asignada al repartidor
            if($venta->iSucursal != $asignacion->iSucursal){
                Log::create(array('iUsuario' => $inputs['iUsuario'],
                    'Mensaje' => 'La venta escaneada ' . $venta->iVenta . ' pertenece a otra sucursal y trato de ser escaneado por repartidor: ' . $asignacion->iRepartidor ,
                    'Request' => json_encode($request->all()),
                    'Origen' => $this::MOVIL));
                return JelpController::Forbidden(ErrorCodes::NOT_BRANCH_BELONG_ERROR_CODE);
            }


            if ($venta->Reparto != null) {

                if ($venta->iEstatus == $this::VENTA_ENPROCESO
                    OR $venta->iEstatus == $this::VENTA_TERMINADA OR
                    $venta->iEstatus == $this::VENTA_CANCELADA
                ) {
                    return JelpController::Forbidden(ErrorCodes::FOLIO_ASSIGNED_ERROR_CODE);
                }

                $venta->iEstatus = $this::VENTA_ENPROCESO;
                $venta->save();

                if(env('EMAIL_NOTIFICATIONS') == 1) {
                    $job = (new NotificarTransito($venta))->delay(60 * 45);
                    $this->dispatch($job);
                }

                $updateReparto = $venta->Reparto;
                if ($updateReparto->iRepartidor != $repartidores->iRepartidor) {
                    $updateReparto->iRepartidor = $repartidores->iRepartidor;
                }

                $datetime = new DateTime();
                $updateReparto->HoraSalida = $datetime->format('Y-m-d H:i:s');
                $venta->Reparto->save($updateReparto->toArray());

                $ruta = new Ruta();
                $ruta->Latitud = $inputs['Latitud'];
                $ruta->Longitud = $inputs['Longitud'];
                $ruta->created_at = $datetime->format('Y-m-d H:i:s');
                $ruta->iRepartidor = $repartidores->iRepartidor;
                $ruta->save();

                return JelpController::Respuesta($venta);
            } else {
                Log::create(array('iUsuario' => $inputs['iUsuario'],
                    'Mensaje' => 'Venta Inexistente',
                    'Request' => json_encode($request->all()),
                    'Origen' => $this::API));
                return JelpController::CustomError('Venta inexistente');
            }
        } catch (Exception $e) {
            Log::create(array('iUsuario' => $inputs['iUsuario'],
                'Mensaje' => 'Excepcion',
                'Request' => json_encode($request->all()),
                'Response' => $e->getMessage(),
                'Detalle' => $e->getTraceAsString(),
                'Origen' => $this::API));

            return JelpController::CustomError($e->getMessage());
        }
    }

    //Marcar como completado
    public function completar(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'iUsuario' => 'required|numeric',
            'Folio' => 'required|alpha_num',
            'Firma' => 'required',
            'Latitud' => 'required',
            'Longitud' => 'required'
        ]);

        if ($validator->fails()) {
            Log::create(array('Mensaje' => 'Error en Validacion',
                'Request' => json_encode($request->all()),
                'Origen' => $this::MOVIL));
            return JelpController::CustomError('iUsuario, Folio, Firma, Latitud, Logitud son requeridos.');
        }
        $inputs = $request->all();
        //$storage_path ="[APP_ROOT]/public/uploads";
        $storage_path = realpath(__DIR__ . '/../../../') . '/public/uploads/';

        $image = base64_decode($inputs['Firma']);
        $folio = $inputs['Folio'];
        $repartidor = Repartidor::where('iUsuario', '=', $inputs['iUsuario'])->first();
        $iRepartidor = $repartidor->iRepartidor;

        try {
            file_put_contents($storage_path . $folio . '.png', $image);
            $venta = new Venta();
            $miVenta = $venta->where('Folio', '=', $folio)->firstOrFail();
            $miVenta->Firma = $folio . '.png';
            $miVenta->iEstatus = 4;
            $miVenta->Latitud = $inputs['Latitud'];
            $miVenta->Longitud = $inputs['Longitud'];
            $miVenta->save();

            $cliente = Cliente::firstOrCreate(['Telefono' => $miVenta->Telefono]);

            $entrega = ClienteEntrega::create([
                'iCliente'  => $cliente->iCliente,
                'Longitude' => $miVenta->Longitud,
                'Latitude'  => $miVenta->Latitud,
                'iRepartidor' => $iRepartidor,
                'iVenta' => $miVenta->iVenta
            ]);

            $eficiente = $entrega->calcularEficiencia();
            $cliente->agregarEficiencia($eficiente);

            $reparto = new Reparto();
            $mireparto = $reparto->where('iVenta', '=', $miVenta->iVenta)->firstOrFail();
            $datetime = new \DateTime();
            $mireparto->HoraEntrega = $datetime->format('Y-m-d H:i:s');
            $mireparto->save();

            $ruta = new Ruta();
            $ruta->Latitud = $inputs['Latitud'];
            $ruta->Longitud = $inputs['Longitud'];
            $ruta->iRepartidor = $iRepartidor;
            $ruta->save();

            $respuesta = array('Respuesta' => '200', 'Mensaje' => 'OK');
        } catch (\Exception $e) {
            Log::create(array('Mensaje' => 'Error en Procedimiento',
                'Request' => json_encode($request->all()),
                'Response' => $e->getMessage(),
                'Detalle' => $e->getTrace(),
                'Origen' => $this::API));
            $respuesta = array('Respuesta' => '500', 'Mensaje' => $e->getMessage());
        }

        echo json_encode($respuesta);

    }

    //Marcar como cancelado
    public function cancelar(Request $request)
    {
        $inputs = $request->all();

        try {
            $ventas = new Venta();
            $miventa = $ventas->where('Folio', '=', $inputs['Folio'])->firstOrFail();
            $miventa->iEstatus = 5;
            $miventa->save();

            $repartos = new Reparto();
            $miReparto = $repartos->where('iVenta', '=', $miventa->iVenta)->firstOrFail();
            $miReparto->iComentario = $inputs['iComentario'];
            $miReparto->save();

            $respuesta = array('Respuesta' => '200', 'Mensaje' => 'OK');
        } catch (\Exception $e) {
            $respuesta = array('Respuesta' => '500', 'Mensaje' => $e->getMessage());
        }

        echo json_encode($respuesta);

    }

    /*Regresa todos los repartos del sistema sin importar su estatus*/
    public function getRepartos()
    {
        try {
            $inicio = INPUT::GET('Inicio');
            $fin = INPUT::GET('Fin');
            if (empty($inicio) OR empty($fin)) {
                return JelpController::CustomError('Faltan Parametros: Inicio / Fin');
            }
            $query = sprintf($this::QUERY_REPARTOS, $inicio, $fin);
            $repartos = DB::Select(DB::raw($query));
            return JelpController::Respuesta(['Repartos' => $repartos]);
        } catch (Exception $e) {
            return JelpController::CustomError($e->getMessage());
        }
    }

    /*Funcion que regresa la ruta del reparto, dependiendo del estatus, se regreasara con la posicion actualiza
    /* o se regresara con la posicion de entregado
    */
    public function getRutaReparto($id)
    {
        if (($id == null) or (!is_numeric($id)) or ($id <= 0)) {
            return JelpController::CustomError('Venta invalida.' . $id);
        }
        try {
            $reparto = Reparto::where('iReparto', '=', $id)->firstOrFail();

            //TODO: Obtener informacion de la venta de la ruta.
            $repartidor = Repartidor::where('iRepartidor', '=', $reparto->iRepartidor)->firstOrFail();

            $usuario = Usuario::where('iUsuario', '=', $repartidor->iUsuario)->firstOrFail();
            $usuarioArray = $usuario->toArray();
            unset($usuarioArray["iUsuario"]);
            unset($usuarioArray["Password"]);
            unset($usuarioArray["iRol"]);

            $venta = Venta::where('iVenta', '=', $reparto->iVenta)->firstOrFail();
            $ventaArray = $venta->toArray();
            unset($ventaArray["iVenta"]);
            unset($ventaArray["Latitud"]);
            unset($ventaArray["Longitud"]);

            $adicional = ["Inicio" => ["Enterado" => $reparto->HoraEnterado,
                "Salida" => $reparto->HoraSalida,
                "Entrega" => $reparto->HoraEntrega,
                "Repartidor" => $usuarioArray],
                "Fin" => ["Venta" => $ventaArray]];


            if (empty($reparto->HoraEntrega)) {
                if (empty($reparto->HoraSalida)) {
                    $reparto->HoraSalida = $reparto->HoraEnterado;
                }
                $query = "select Latitud as latitude,Longitud as longitude, created_at " .
                    "from rutas " .
                    "where iRepartidor=$reparto->iRepartidor and created_at between '$reparto->HoraSalida' and NOW() order by iRuta asc";
            } else {
                $query = "select Latitud as latitude,Longitud as longitude , created_at " .
                    "from rutas " .
                    "where iRepartidor=$reparto->iRepartidor and created_at between '$reparto->HoraSalida' and '$reparto->HoraEntrega' order by iRuta asc";
            }
            $ruta = DB::select(DB::raw($query));


        } catch (Exception $e) {
            return JelpController::CustomError('Reparto no encontrado.' . $e);
        }
        return JelpController::Respuesta(['Adicional' => $adicional, 'RutaReparto' => $ruta]);
    }

    public function getRepartosAsignados()
    {
        $folio = INPUT::get('Folio');
        $iUsuario = INPUT::get('iUsuario');

        $repartidor = Repartidor::where('iUsuario', '=', $iUsuario)->first();

        $asignaciones = new Asignacion();
        $asignado = DB::table('vw_asignaciones')->where('iRepartidor', '=', $repartidor->iRepartidor)->get();
        //revisamos que el folio venta este en estatus 2
        $venta = Venta::where('Folio', '=', $folio)->first();
        if (!empty($venta)) {
            //si la venta tiene estatus 1, asignar el repartidor
            if ($venta->iEstatus == 1) {
                $venta->iEstatus = 2;
                $venta->save();

                $reparto = new Reparto();
                $datetime = new \DateTime();
                $reparto->HoraEnterado = $datetime->format('Y-m-d H:i:s');
                $reparto->iVenta = $venta->iVenta;
                $reparto->iRepartidor = $asignado[0]->iRepartidor;
                $reparto->iSucursal = $asignado[0]->iSucursal;
                $reparto->iComentario = 1;
                $reparto->save();

                echo json_encode(['Respuesta' => '200', 'Mensaje' => 'OK']);
            }

            if ($venta->iEstatus == 2) {
                echo json_encode(['Respuesta' => '200', 'Mensaje' => 'OK']);
            }
        } else {
            echo json_encode(['Respuesta' => '400', 'Mensaje' => 'Inexistente']);
        }
    }

    public function traspaso(Request $request)
    {
        $inputs = $request->all();

        $validator = Validator::make($inputs, [
            'Folio' => 'required',
            'iUsuario' => 'required',
            'Latitud' => 'required',
            'Longitud' => 'required'
        ]);

        if ($validator->fails()) {
            Log::create(array('Mensaje' => 'Error en Validador',
                'Request' => json_encode($request->all()),
                'Response' => 'Parametros incompletos',
                'Detalle' => '',
                'Origen' => $this::API));
            return JelpController::CustomError('Folio, iUsuario, Latitud, Longitud, Son requeridos');
        }

        try {
            $repartidor = Repartidor::where('iUsuario', '=', $request->input('iUsuario'))
                ->first();
            $asignacion = Asignacion::where('iRepartidor', '=', $repartidor->iRepartidor)
                ->orderBy('created_at', 'desc')
                ->first();

            $venta = Venta::where('Folio', '=', $request->input('Folio'))
                ->whereRaw("(iEstatus=" . $this::VENTA_PENDIENTE . " OR iEstatus=" . $this::VENTA_ACEPTADA . ")")
                ->first();

            if (empty($venta)) {
                Log::create(array('Mensaje' => 'Venta Ya Asignada',
                    'Request' => json_encode($request->all()),
                    'Response' => 'Invalido',
                    'Detalle' => 'Venta Ya Asignada',
                    'Origen' => $this::MOVIL));
                return JelpController::InvalidAccess();
            }

            if ($venta->iSucursal == $asignacion->iSucursal) {
                Log::create(array('Mensaje' => 'Venta y Reparto corresponden a la misma Sucursal',
                    'Request' => json_encode($request->all()),
                    'Response' => 'Invalido',
                    'Detalle' => 'Venta y Reparto corresponden a la misma Sucursal',
                    'Origen' => $this::MOVIL));
                return JelpController::InvalidAccess();
            }

            //En este caso se obtiene la sucursal de la venta al tratarse de un traspaso
            $sucursal = Sucursal::find($venta->iSucursal);

            $distanciaSucusal = GeoCalculator::getDistanceBetweenPoints($sucursal->Latitud, $sucursal->Longitud, $inputs['Latitud'], $inputs['Longitud']);

            $metros_de_sucursal = Configuracion::where('Codigo', '=', Reparto::CODIGO_DISTANCIA)->first();

            //Verifica que este dentro o cerca de la sucursal para poder aceptar el pedido
            if ($distanciaSucusal > $metros_de_sucursal->Valor) {
                Log::create(array('iUsuario' => $inputs['iUsuario'],
                    'Mensaje' => 'Fuera de Rango de Sucursal repartidor: ' . $asignacion->iRepartidor ,
                    'Request' => json_encode($request->all()),
                    'Origen' => $this::MOVIL));

                return JelpController::Forbidden(ErrorCodes::OUT_OF_RANGE_ERROR_CODE);
            }

            $venta->iEstatus = $this::VENTA_ENPROCESO;
            $venta->save();

            $repartos = Reparto::where('iVenta', '=', $venta->iVenta)->first();
            if(!$repartos) {
                $reparto = new Reparto();
                $datetime = new DateTime();
                $reparto->HoraEnterado = $datetime->format('Y-m-d H:i:s');
                $reparto->HoraSalida = $datetime->format('Y-m-d H:i:s');
                $reparto->iVenta = $venta->iVenta;
                $reparto->iRepartidor = $repartidor->iRepartidor;
                $reparto->iSucursal = $asignacion->iSucursal;
                $reparto->iComentario = 1;
                $reparto->Traspaso = true;
                $reparto->save();
            }else{
                $repartos->Traspaso = true;
                $repartos->iSucursal = $asignacion->iSucursal;
                $repartos->iRepartidor = $repartidor->iRepartidor;
                $repartos->iComentario = 1;
                $datetime = new DateTime();
                $repartos->HoraEnterado = $datetime->format('Y-m-d H:i:s');
                $repartos->HoraSalida = $datetime->format('Y-m-d H:i:s');
                $repartos->save();
            }

            $ruta = new Ruta();
            $ruta->Latitud = $request->input('Latitud');
            $ruta->Longitud = $request->input('Longitud');
            $ruta->iRepartidor = $repartidor->iRepartidor;
            $ruta->save();

            $mventa = Venta::where('Folio', '=', $request->input('Folio'))
                ->with('Reparto')
                ->first();

            return JelpController::Respuesta($mventa);
        } catch (Exception $e) {
            Log::create(array('Mensaje' => $e->getMessage(),
                'Request' => json_encode($request->all()),
                'Response' => $e->getMessage(),
                'Detalle' => $e->getTrace(),
                'Origen' => $this::API));
            return JelpController::CustomError($e->getMessage());
        }

    }

    public function getTimesDeliveries()
    {
        $fechaInicio = INPUT::GET('Inicio');
        $fechaFin    = INPUT::GET('Fin');

        if(!$this->validateDate($fechaInicio) || !$this->validateDate($fechaFin)) {
          return JelpController::CustomError('Los parametros Inicio y Fin no son validos');
        }

        try {
            $timeSale = DB::table('ventas')
                ->select(DB::raw('avg(timestampdiff(SECOND , ventas.created_at, repartos.HoraEntrega))/60 as avgSale, avg(timestampdiff(SECOND , ventas.created_at, repartos.HoraEnterado))/60 as avgEnterado'))
                ->whereRaw("date(ventas.created_at) between '$fechaInicio' and '$fechaFin'")
                ->join('repartos', 'repartos.iVenta', '=', 'ventas.iVenta')
                ->whereNotNull('repartos.HoraEntrega')
                ->where('repartos.HoraEntrega', '!=', '')
                ->get();

            $departureTime = DB::table('repartos')
                ->select(DB::raw('avg(timestampdiff(SECOND, HoraEnterado, HoraSalida))/60 as avgDeparture, avg(timestampdiff(SECOND, HoraSalida, HoraEntrega))/60 as avgTransit'))
                ->whereRaw("date(HoraEnterado) between '$fechaInicio' and '$fechaFin'")
                ->whereNotNull('HoraEntrega')
                ->where('HoraEntrega', '!=', '')
                ->get();

            $venta    = 0;
            $salida   = 0;
            $transito = 0;
            $enterado = 0;

            if(isset($departureTime[0])) {
                $salida   = $departureTime[0]->avgDeparture ?: 0;
                $transito = $departureTime[0]->avgTransit   ?: 0;
            }

            if (isset($timeSale[0])) {
                $venta    = $timeSale[0]->avgSale ?: 0;
                $enterado = $timeSale[0]->avgEnterado ?: 0;
            }

            return JelpController::Respuesta([
                'Venta'    => $venta,
                'Enterado' => $enterado,
                'Salida'   => $salida,
                'Transito' => $transito
            ]);
        } catch (Exception $e) {
            return JelpController::CustomError($e->getMessage());
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\Response
     * Metodo para obtener los repartos en base a un usuario y estatus. El endpoint es principalmente de uso para
     * MOVILES
     */
    public function obtenerRepartos(Request $request)
    {
        try {
            $usuario = Usuario::find($request->input('iUsuario'));
            $estatus = explode(',', $request->input('iEstatus'));
            $estatus = Estatus::select('iEstatus')
                ->whereIn('iEstatus', $estatus)
                ->get();
            $sucursal = Sucursal::find($request->input('iSucursal'));

            if (!$usuario || !$estatus || !$sucursal) {
                return JelpController::Respuesta(['Repartos' => []]);
            }

            $repartos = DB::table('repartos')
                ->select(
                    'repartos.iReparto',
                    'repartos.HoraEnterado',
                    'repartos.HoraEntrega',
                    'repartos.Traspaso',
                    'ventas.created_at as FechaVenta',
                    'ventas.Folio',
                    'ventas.Telefono',
                    'ventas.Importe',
                    'ventas.Latitud',
                    'ventas.Longitud',
                    'usuarios.iUsuario',
                    'ventas.iEstatus',
                    'repartos.iSucursal'
                )
                ->join('ventas', 'repartos.iVenta', '=', 'ventas.iVenta')
                ->join('repartidores', 'repartos.iRepartidor', '=', 'repartidores.iRepartidor')
                ->join('usuarios', 'repartidores.iUsuario', '=', 'usuarios.iUsuario')
                ->where('repartos.iSucursal', '=', $sucursal->iSucursal)
                ->where('usuarios.iUsuario', '=', $usuario->iUsuario)
                ->whereRaw('Date(ventas.created_at) >= CURDATE() - INTERVAL 1 DAY')
                ->whereIn('ventas.iEstatus', $estatus)
                ->get();

            return JelpController::Respuesta(['Repartos' => $repartos]);
        }catch(Exception $e){
            return JelpController::CustomError($e->getMessage());
        }
    }
}
