<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Mtto_articulo extends Model{
    use SoftDeletes;

    protected $table='mtto_articulos';
    protected $primaryKey = 'iMttoArticulo';
    protected $guarded = array('iMttoArticulo');
    public $timestamps = true;
    protected $dates = ['deleted_at'];
    protected $fillable = array('Concepto', 'Precio', 'Categoria');
    protected $visible = array('Concepto', 'Precio', 'iMttoArticulo', 'Categoria');
}