<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Grupo_checklist extends Model{
    protected $table='grupos_checklist';
    protected $primaryKey = 'iGrupoChecklist';
    protected $guarded = array('iGrupoChecklist');
    public $timestamps = true;
    protected $fillable = array('Titulo', 'Orden', 'Activo');
    protected $visible = array('iGrupoChecklist', 'Titulo', 'Orden', 'Activo', 'Checklist');

    public function checklist(){
        return $this->hasMany('App\Models\Checklist', 'iGrupoChecklist')->orderBy('Orden', 'asc');
    }
}