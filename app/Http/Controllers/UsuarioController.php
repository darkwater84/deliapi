<?php

namespace app\Http\Controllers;

use App\Models\Rol;
use App\Models\Usuario;
use App\Models\Repartidor;
use App\Http\Controllers\JelpController;
use Illuminate\Http\Request;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Validator;
use Hash;
use DB;



class UsuarioController extends JelpController
{

    public function __construct(Usuario $usuario)
    {
        $this->usuario = $usuario;
    }

    /**
     * Metodo para regresar todos los Usuarios.
     */
    public function getAll()
    {
        $usuarios = Usuario::with('Rol')->get();
        return JelpController::Respuesta($usuarios);
    }

    /**
     * Metodo para poder obtener una sucursal por id.
     */
    public function getUsuario($id)
    {
        $usuario = Usuario::with('Rol')->find($id);
        if ($usuario == null) {
            return JelpController::NotFound();
        }
        return JelpController::Respuesta($usuario);
    }

    /**
     * Crear usuario
     *
     * Campos obligatorios: Nombre, Apellido, Numero de Empleado, Usuario, Contrasena.
     * Campos opcionales: Email.
     */
    public function postUsuario(Request $request, $id = 0)
    {
        $validator = Validator::make($request->all(), [
            'Usuario' => 'required|unique:usuarios,Usuario',
            'Nombre' => 'required',
            'Apellidos' => 'required',
            'NoEmpleado' => 'required|unique:usuarios,NoEmpleado',
            'iRol' => 'required|min:1'
        ]);

        if ($validator->fails()) {
            return JelpController::CustomError(['Error' => $validator->errors()->first()]);
        }

        $usuario = Usuario::find($id);

        if ($usuario != null) {

            $usuario->Usuario = $request->input('Usuario');
            $usuario->Nombre = $request->input('Nombre');
            $usuario->Apellidos = $request->input('Apellidos');
            $usuario->NoEmpleado = $request->input('NoEmpleado');
            $usuario->Email = $request->input('Email');
            $usuario->iRol = $request->input('iRol');
            if($request->exists('Password'))
            {
                $usuario->Password = $request->input('Password');
            }
            $usuario->save();

        } else {
            $usuario = Usuario::create($request->all());
            if ($request->input('iRol') == 5) {
                $repartidor = new Repartidor();
                $repartidor->iUsuario = $usuario->iUsuario;
                $repartidor->iEstatus = 1;
                $repartidor->save();
            }
        }

        /**
         * HASHEO DE PASSWORDS
         * Referencia: http://laravel.com/docs/5.0/hashing
         * Prueba:
         *  curl -X POST -F "Usuario=josuebasurto" -F "Nombre=Josue" -F "Apellidos=Basurto Nieto" -F "NoEmpleado=100" -F "Email=josue@jelp.io" -F "iRol=1" -F "Password=secretosecreto" 'http://localhost:8000/usuarios/7'
         */

        //IMPORTANTE: No activar a memos de que se este depurando!
        //$pass = ['Hashed'=>$hashedPassword, 'Unhashed'=>$password, 'HashCheck'=> Hash::check($password, $hashedPassword)];
        //return JelpController::Respuesta(['Usuario'=>$usuario, 'Pass'=> $pass]);

        return JelpController::Respuesta(['Usuario' => $usuario]);
    }

    /**
     * Metodo para poder eliminar el usuario
     */
    public function deleteUsuario($id)
    {
        try {
            //Busca al usuario
            $usuario = Usuario::find($id);
            //Error si no hay usuario
            if ($usuario == null) {
                return JelpController::NotFound();
            }
            //TODO: Si hay mas implicaciones antes de borrar un usuario
            $usuario->delete();
            //Respuesta de Succeed
            return JelpController::Respuesta();

        } catch (ModelNotFoundException $ex) {
            return JelpController::CustomError($ex);
        } catch (Exception $e) {
            return JelpController::CustomError($e);
        }
    }

    /**
     * POST - Login de usuarios
     */
    public function postLogin(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'usuario' => 'required',
            'password' => 'required'
        ]);

        if ($validator->fails()) {
            return JelpController::CustomError(['Error'=>'usuario y password son requeridos.']);
        }

        $input = $request->all();
        try{
            $usuario = Usuario::where('Usuario','=', $input['usuario'])->where('Password','=',$input['password'])->first();
            if($usuario != null){
                return JelpController::Respuesta($usuario);
            }
            else
            {
                return JelpController::CustomError("Usuario Inexistente");
            }
        }
        catch(\Exception $e)
        {
            return JelpController::CustomError($e->getMessage());
        }
        return JelpController::CustomError('Credenciales Inválidas');
    }

    /**
     * @return \Illuminate\Http\Response
     * @description Metodo que retorna el listado de usuarios con el rol de mecanico
     */
    public function getMecanicos(){
        $mecanicos = Usuario::select('iUsuario', 'Apellidos', 'Nombre', 'NoEmpleado')
            ->rol(Rol::MECHANIC)
            ->get();

        return JelpController::Respuesta($mecanicos);
    }
}
