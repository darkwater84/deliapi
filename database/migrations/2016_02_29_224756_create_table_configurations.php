<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Configuracion;

class CreateTableConfigurations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('configuraciones', function(Blueprint $table){
            $table->increments('iConfiguracion');
            $table->string('Codigo', 10)->unique();
            $table->string('Descripcion',255);
            $table->string('Valor');
        });

        Configuracion::create([
            'Codigo' => 'DTS01',
            'Descripcion' => 'Distancia maxima con sucursal para aceptar pedidos',
            'Valor' => 10
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('configuraciones');
    }
}
