
<!DOCTYPE html>
<html>
<head>
    <style>
        .center{
            vertical-align: middle;
            text-align:center;
            color: #000;
        }

        .right{
            vertical-align: middle;
            text-align:right;
            color: #000;
        }

        .left{
            vertical-align: middle;
            text-align:left;
            color: #000;
        }

        td {
            color:#6e7478;
        }

        label{
            display: block;
            font-size: 20px;
            color:#000;
        }

        .graphic{
            height: 500px;
            background-color: #fff;
        }

        .graphic h2{
            color: #fff;
        }

    </style>
</head>
<body>
<div style="width: 100%; padding: 5px 10px 5px 10px;">
    <div style="width: 100%; padding: 5px 10px 5px 10px;">
        <table style="width: 100%" cellspacing="15" cellpadding="0">
            <tbody>
            <tr>
                <td width="5%" colspan="2">
                    <img src="{{url('images/performance.png')}}">
                </td>
                <td width="72%">
                    <label>Agente de Reparto <br> Reporte de rendimiento</label>
                </td>
                <td class="center" width="23%" style="padding-top: 15px;">
                    <img src="{{url('images/3en1.png')}}">
                </td>
            </tr>
            </tbody>
        </table>
        <hr size="5" style="border-color:#000; background: #000; height: 5px;">
    </div>
    <div style="width: 100%">
        <table style="width: 100%; margin-bottom: 10px; margin-top: 20px;">
            <tbody>
            <tr>
                <td width="25%"><h2>Nombre</h2></td>
                <td width="42%" class="left"><h2>{{$nombre}}</h2></td>
                <td width="33%" class="right"><h2>{{$inicio}} - {{$fin}}</h2></td>
            </tr>
            <tr>
                <td width="25%"><h2>No. Empleado</h2></td>
                <td width="42%" class="left"><h2>{{$noEmpleado}}</h2></td>
            </tr>
            <tr>
                <td width="25%"><h2>Sucursal Asignada</h2></td>
                <td width="42%"><h2 class="left">{{$sucursal}}</h2></td>
            </tr>
            </tbody>
        </table>
    </div>
    <hr/>
    <div style="width: 100%">
        <table style="width: 100%; margin-bottom: 10px; margin-top: 20px;">
            <tbody>
                <tr>
                    <td width="50%"><h2>Tiempo en tr&aacute;nsito</h2></td>
                    <td width="50%" class="right"><h2>{{$tiempoTransito}}</h2></td>
                </tr>
                <tr>
                    <td width="50%"><h2>Tiempo en sucursal</h2></td>
                    <td width="50%" class="right"><h2>{{$tiempoSucursal}}</h2></td>
                </tr>
                <tr>
                    <td width="50%"><h2>Tiempo en l&iacute;nea</h2></td>
                    <td width="50%" class="right"><h2>{{$tiempoLinea}}</h2></td>
                </tr>
                <tr>
                    <td width="50%"><h2>Participaci&oacute;n</h2></td>
                    <td width="50%" class="right"><h2>{{$participacion}}</h2></td>
                </tr>
            </tbody>
        </table>
    </div>
    <hr color="#000">
    <div class="graphic">
        <h2>.</h2>
        <h2>.</h2>
        <h2>.</h2>
        <h2>.</h2>
        <h2>.</h2>
        <h2>.</h2>
        <h2>.</h2>
        <h2>.</h2>
        <h2>.</h2>
    </div>
    <br>
    <br>
    <div  style="width: 100%" class="right">
        <img src="{{url('images/logo-jelp.png')}}" width="150px;">
    </div>
    <br>
    <br>
    <br>
    <br>
    <div>
        <table cellspacing="5">
            <tr>
                <td>
                    <hr/>
                </td>
                <td>
                    <hr/>
                </td>
            </tr>
        </table>
    </div>
</div>
</body>
</html>

