<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Mtto_estatus;

class CreateTableMttoEstatus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mtto_estatus', function(Blueprint $table){
            $table->increments('iMttoEstatus');
            $table->string('Estatus')->unique();
        });

        Schema::table('mantenimientos', function(Blueprint $table){
            $table->unsignedInteger('iMttoEstatus')->nullable()
                ->after('Observacion');

            $table->foreign('iMttoEstatus')->references('iMttoEstatus')
                ->on('mtto_estatus');
        });

        Mtto_estatus::create(['Estatus' => 'Pendiente']);
        Mtto_estatus::create(['Estatus' => 'Aceptado']);
        Mtto_estatus::create(['Estatus' => 'Iniciado']);
        Mtto_estatus::create(['Estatus' => 'Cerrado']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mantenimientos', function(Blueprint $table){
            $table->dropForeign('mantenimientos_imttoestatus_foreign');
            $table->dropColumn('iMttoEstatus');
        });

        Schema::drop('mtto_estatus');
    }
}
