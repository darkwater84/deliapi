<?php
/**
 * Created by PhpStorm.
 * User: totushi
 * Date: 2/29/16
 * Time: 3:01 PM
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Configuracion extends Model{
    protected $table='configuraciones';
    protected $primaryKey = 'iConfiguracion';
    protected $guarded = array('iConfiguracion');
    public $timestamps = false;
    protected $fillable = array('Descripcion', 'Codigo', 'Valor');
    protected $visible = array('iConfiguracion', 'Descripcion', 'Codigo', 'Valor');
}