<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Exception;
use Illuminate\Database\Eloquent\SoftDeletes;
/**
 * Modelo de Parametros de un Reporte
 */
class Reporte_Params extends Model{

    use SoftDeletes;
	protected $table = 'reporte_params';
	protected $primaryKey = 'iReporteParams';
    protected $dates = ['deleted_at'];
	protected $fillable = array('Nombre','Descripcion','TipoParametro');
	protected $visible = array('Nombre','Descripcion','TipoParametro', 'Reporte');

	/**
	 * Relacion con la tabla de Reportes
	 */
	public function Reporte(){
		return $this->belongsTo('App\Models\Reporte','iReporte');
	}
}