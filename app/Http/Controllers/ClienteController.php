<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use App\Models\Cliente;
use Exception;

class ClienteController extends JelpController {

    public function getPhones()
    {
        $fechaInicio = INPUT::GET('Inicio');
        $fechaFin    = INPUT::GET('Fin');

        if(!$this->validateDate($fechaInicio) || !$this->validateDate($fechaFin)) {
            return JelpController::CustomError('Los parametros Inicio y Fin no son');
        }

        try {
            $newPhones = DB::table('clientes')
                         ->select('Telefono')
                         ->whereRaw("date(created_at) between '$fechaInicio' and '$fechaFin'")
                         ->count();

            $totalPhones = DB::table('ventas')
                ->select(DB::raw('DISTINCT ventas.Telefono'))
                ->whereRaw("date(created_at) between '$fechaInicio' and '$fechaFin'")
                ->count();

            $result = collect([
                ['Titulo' => 'Clientes Nuevos', 'Total' => $newPhones],
                ['Titulo' => 'Clientes Regulares', 'Total' => ($totalPhones - $newPhones)]
            ]);

            return JelpController::Respuesta($result);

        } catch (Exception $e) {
          return JelpController::CustomError($e->getMessage());
        }
    }

    public function getNewClientDeliveriesStats()
    {
        $fechaInicio = INPUT::GET('Inicio');
        $fechaFin    = INPUT::GET('Fin');

        if(!$this->validateDate($fechaInicio) || !$this->validateDate($fechaFin)) {
            return JelpController::CustomError('Los parametros Inicio y Fin no son validos');
        }

        try {
            $newSales = DB::table('clientes')
                ->whereRaw("date(clientes.created_at) between '$fechaInicio' and '$fechaFin'")
                ->join('ventas', 'ventas.Telefono', '=', 'clientes.Telefono')
                ->where('ventas.iEstatus', '=', VentaController::ENTREGADO)
                ->select(DB::raw('clientes.Telefono, count(ventas.iVenta) as Ventas'))
                ->groupBy('clientes.Telefono')
                ->get();

            $response = [
                'minusThree' => 0,
                'minusSeven' => 0,
                'plusEight'  => 0
            ];

            foreach($newSales as $sale) {
                if($sale->Ventas <= 0) {
                    continue;
                }

                if($sale->Ventas <= 3) {
                    $response['minusThree']++;
                    continue;
                }

                if($sale->Ventas <= 7) {
                    $response['minusSeven']++;
                    continue;
                }

                $response['plusEight']++;
            }

            return $response;
        } catch (Exception $e) {
            return JelpController::CustomError($e->getMessage());
        }
    }

    
}
