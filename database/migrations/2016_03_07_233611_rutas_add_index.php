<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RutasAddIndex extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(Schema::hasColumn('rutas','created_at')){
            Schema::table('rutas',function(Blueprint $table){
                $table->index(['created_at','iRepartidor']);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumn('rutas','created_at')){
            Schema::table('rutas',function(Blueprint $table){
                $table->dropIndex(['created_at','iRepartidor']);
            });
        }
    }
}
