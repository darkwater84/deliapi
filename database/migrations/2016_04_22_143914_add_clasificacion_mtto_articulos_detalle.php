<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddClasificacionMttoArticulosDetalle extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mtto_articulos_detalle',function(Blueprint $table){
            $table->string('Clasificacion',11)->nullable()->after('Importe');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mtto_articulos_detalle',function(Blueprint $table){
            $table->dropColumn('Clasificacion');
        });
    }
}
