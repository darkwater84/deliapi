<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Grupo_checklist;

class CreateTableGruposChecklist extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('grupos_checklist', function(Blueprint $table){
            $table->increments('iGrupoChecklist');
            $table->string('Titulo', 60);
            $table->integer('Orden');
            $table->tinyInteger('Activo')->default(1);
            $table->timestamps();
        });

        Grupo_checklist::firstOrCreate([
            'iGrupoChecklist' => 1,
            'Titulo' => 'Llave y Documentos',
            'Orden' => 1,
            'Activo' => 1
        ]);

        Grupo_checklist::firstOrCreate([
            'iGrupoChecklist' => 2,
            'Titulo' => 'Herramientas',
            'Orden' => 2,
            'Activo' => 1
        ]);

        Grupo_checklist::firstOrCreate([
            'iGrupoChecklist' => 3,
            'Titulo' => 'Tapiceria',
            'Orden' => 3,
            'Activo' => 1
        ]);

        Grupo_checklist::firstOrCreate([
            'iGrupoChecklist' => 4,
            'Titulo' => 'Accesorios',
            'Orden' => 4,
            'Activo' => 1
        ]);
        Grupo_checklist::firstOrCreate([
            'iGrupoChecklist' => 5,
            'Titulo' => 'Niveles de Fluidos',
            'Orden' => 5,
            'Activo' => 1
        ]);

        Grupo_checklist::firstOrCreate([
            'iGrupoChecklist' => 6,
            'Titulo' => 'Funcionamiento sin Fallas',
            'Orden' => 6,
            'Activo' => 1
        ]);

        Grupo_checklist::firstOrCreate([
            'iGrupoChecklist' => 7,
            'Titulo' => 'Carroceria',
            'Orden' => 7,
            'Activo' => 1
        ]);

        Grupo_checklist::firstOrCreate([
            'iGrupoChecklist' => 8,
            'Titulo' => 'GPS',
            'Orden' => 8,
            'Activo' => 1
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('grupos_checklist');
    }
}
