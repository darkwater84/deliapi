<?php

namespace App\Http\Controllers;

use App\Models\Repartidor;
use App\Models\Usuario;
use App\Models\Dispositivo;
use App\Models\Asignacion;
use App\Models\Log;
use App\Models\LogRepartidor;
use App\Http\Controllers\JelpController;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;
use Validator;
use Exception;
use Mail;
use DB;

class RepartidorController extends JelpController{

    const MOVIL = 'MOVIL';
    const CRM = 'CRM';
    const API = 'API';
    const LOGIN = 1;
    const LOGOUT = 2;

    const QUERY_UBICACIONES = <<<EOD
SELECT u.iUsuario, u.nombre, u.apellidos, ur.iRepartidor, r.iEstatus, er.Estatus,ur.Latitud AS latitude, ur.Longitud AS longitude, ur.created_at
FROM
(
  SELECT iRuta, iRepartidor, Latitud, Longitud, max(created_at) AS created_at
  FROM
  (
    SELECT iRuta, iRepartidor, Latitud, Longitud, created_at
    FROM rutas
    where created_at > NOW() - INTERVAL 1 DAY
    ORDER BY created_at DESC, iRepartidor
  ) AS tabla
  GROUP BY iRepartidor
) AS ur
INNER JOIN repartidores AS r ON ur.iRepartidor = r.iRepartidor
INNER JOIN usuarios AS u ON r.iUsuario = u.iUsuario
INNER JOIN estatus_repartidor AS er ON r.iEstatus = er.iEstatus
ORDER BY iEstatus ASC, iUsuario ASC;
EOD;

    const QUERY_UBICACION_REPARTIDOR = <<<EOD
      select u.iUsuario, u.nombre, u.apellidos, ur.iRepartidor, ur.Latitud, ur.Longitud, ur.Latitud as latitude, ur.Longitud as longitude, ur.created_at from
      (
        select iRuta, iRepartidor, Latitud, Longitud, max(created_at) as created_at from
        (
          select iRuta, iRepartidor, Latitud, Longitud, created_at
          from rutas
          order by created_at desc, iRepartidor
        ) as tabla
        group by iRepartidor
      ) as ur
      inner join repartidores as r
      on ur.iRepartidor = r.iRepartidor
      inner join usuarios as u
      on r.iUsuario = u.iUsuario where r.iRepartidor=
EOD;

    const ESTATUS_REPARTIDOR_ACTIVO = 1;
    const ESTATUS_REPARTIDOR_INACTIVO = 2;

    public function getAll(){
        $repartidores = DB::table('vw_repartidores')->whereRaw('deleted_at IS NULL')->get();
        return JelpController::Respuesta(['Repartidores'=>$repartidores]);
    }

    public function getLogin(Request $request)
    {
        $input = $request->all();
        $validations = Validator::make($input, [
            'usuario'   => 'required',
            'password'  => 'required',
        ]);

        if($validations->fails())
        {
            return JelpController::CustomError($validations->errors()->first());
        }

        try{

            $repartidor = Usuario::select('iUsuario', 'Usuario', 'Apellidos', 'Nombre', 'NoEmpleado')
                ->where('Usuario','=',$input['usuario'])
                ->with('Repartidor')
                ->where('Password','=',$input['password'])
                ->where('iRol','=',5)
                ->first();

            if(!$repartidor){
                Log::create(array('iUsuario'=>$input['usuario'],
                    'Mensaje'=>'Se intento loggear',
                    'Request'=>json_encode($request->all()),
                    'Origen'=>$this::MOVIL));
                return JelpController::InvalidAccess();
            }
            //mientras que la variable de Inspección sea false, se ignorara la funcionalidad para revisar si ya tiene inspeccion,
            //y siempre regresara FALSE
            $repartidor->Inspeccion = (env('INSPECCION') == true ?  $repartidor->hasInspeccion($repartidor->iRepartidor) : true);

            Log::create(array('iUsuario'=>$input['usuario'],
                'Mensaje'=>'Usuario Logeado',
                'Request'=>json_encode($request->all()),
                'Origen'=>$this::API));

            return JelpController::Respuesta(['Usuario'=>$repartidor]);

        } catch (Exception $e) {
            return JelpController::CustomError($e->getMessage());
        }
    }

    public function alarma(Request $request){
        $inputs = $request->all();
        $coordenadas = $inputs['latitud'].','.$inputs['longitud'];

        try {
            $repartidor = Repartidor::where('iUsuario','=',$inputs['iUsuario'])->first();
            $asignacion = Asignacion::where('iRepartidor','=',$repartidor->iRepartidor)
                ->orderBy('created_at', 'desc')
                ->first();

            if(!empty($asignacion))
            {
              $nombre = $repartidor->Usuario->Nombre;
              $sucursal = $asignacion->Sucursal->NumeroSucursal.'-'.$asignacion->Sucursal->Sucursal;

              Mail::send('email', array('coordenadas' => $coordenadas,'repartidor'=>$nombre, 'sucursal'=>$sucursal ), function($message)
              {
                if(getenv('APP_ENV')=='local')
                {
                  $message->to('dtorres@firstkontact.com','David Torres')->subject('Alerta');
                }
                if(getenv('APP_ENV')=='production')
                {
                  $message->to('Emmanuel.rodriguez@farmaciasroma.com', 'Emmanuel Rdoriguez')
                    ->cc('dtorres@firstkontact.com')
                    ->cc('olangarica@farmaciasroma.com')
                    ->cc('ipalacios@farmaciasroma.com')
                    ->cc('Abel.aparicio@farmaciasroma.com')
                    ->subject('Alerta');
                }
              });
              return JelpController::Respuesta('');
            }
        } catch (Exception $e) {
            return JelpController::CustomError($e->getMessage());
        }
    }

    public function logout(Request $request){

        $inputs = $request->all();
        $IMEI = $request->input('IMEI');
        $validations = Validator::make($inputs, [
            'IMEI' => 'required|exists:dispositivos,IMEI,IMEI,'.$IMEI,
            'iusuario' => 'required'
        ]);

        $validations->setAttributeNames([
            'IMEI'  => 'IMEI',
            'iusuario'     => 'iusuario',
        ]);

        if($validations->fails()){
            return JelpController::CustomError($validations->errors()->first());
        }

        try {
            //actualizacion de estatus del dispositivo
            $dispositivo = Dispositivo::where('IMEI','=',$IMEI)
                ->first();
            $dispositivo->iEstatus = $this::ESTATUS_REPARTIDOR_INACTIVO;
            $dispositivo->save();

            //actualizacion de estatus del repartidor
            $repartidor = Repartidor::where('iUsuario', '=', $inputs['iusuario'])
                ->first();
            $repartidor->iEstatus = $this::ESTATUS_REPARTIDOR_INACTIVO;
            $repartidor->save();

           Usuario::validateLogout($repartidor, $dispositivo);

            return JelpController::Respuesta(['Respuesta'=>'200','Mensaje'=>'OK']);
        } catch (Exception $e) {
            return JelpController::CustomError($e->getMessage());
        }
    }

    public function getUbicacionRepartidor($iRepartidor){
        $ubicaciones = DB::select( DB::raw( $this::QUERY_UBICACION_REPARTIDOR ) );
        return JelpController::Respuesta(['Ubicaciones'=>$ubicaciones]);
    }

    /**
     * GET - Metodo para poder obtener ubicaciones para el mapa de administracion.
     * TODO: Esto se puede mejorar con una consulta en eloquent.
     */
    public function getUbicaciones(){
        $ubicaciones = DB::select( DB::raw( $this::QUERY_UBICACIONES ) );
        return JelpController::Respuesta(['Ubicaciones'=>$ubicaciones]);
    }
}
