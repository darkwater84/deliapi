<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Exception;

class Mtto_articulo_detalle extends Model{
    protected $table='mtto_articulos_detalle';
    protected $primaryKey = 'iMttoArticuloDetalle';
    protected $guarded = array('iMttoArticuloDetalle');
    public $timestamps = true;
    protected $fillable = array('iMttoArticulo', 'iMantenimiento','Articulo','Cantidad','Importe','Precio','Clasificacion');
    protected $visible = array('iMttoArticulo', 'iMantenimiento','Articulo','Cantidad','Importe','Precio', 'Clasificacion','iMttoArticuloDetalle');


    /**
     * @param $quantity
     * @param $maintenance
     * @param $iMttoArticulo
     * @return mixed
     * @Description: Metodo para actualizar el articulo
     */
    public static function updateArticleDetail($quantity, $maintenance, $iMttoArticulo){
        //Si no lo encuentra significa que nunca se ha creado el registro
        $article_detail = Mtto_articulo_detalle::where('iMttoArticulo', '=', $iMttoArticulo)
            ->where('iMantenimiento', '=', $maintenance->iMantenimiento)->firstOrFail();

        $oldAmount = $article_detail->Importe;
        $amount = ($article_detail->Precio * $quantity);
        //Actualiza unicamente la cantidad y el importe
        $article_detail->Cantidad = $quantity;
        $article_detail->Importe = $amount;
        $article_detail->save();

        $difference = $amount - $oldAmount;

        $maintenance->Total += $difference;
        $maintenance->save();

        return $article_detail;
    }

    /**
     * @param $inputs
     * @param $maintenance
     * @return static
     * @throws Exception
     * @Description: Metodo para almacenar el articulo
     */
    public static function storeArticleDetail($inputs, $maintenance)
    {
        $article = Mtto_articulo::find($inputs['iMttoArticulo']);

        $exists_in_detail = Mtto_articulo_detalle::where('iMantenimiento', '=', $maintenance->iMantenimiento)
            ->where('iMttoArticulo', '=', $article->iMttoArticulo)
            ->first();

        //Se valida que no este ya registrado
        if($exists_in_detail){
            throw new Exception('Articulo repetido en detalle');
        }
        $amount = ($article->Precio * $inputs['Cantidad']);
        //Si no existia se crea el nuevo registro
        $article_detail = Mtto_articulo_detalle::create([
            'iMttoArticulo'     => $article->iMttoArticulo,
            'iMantenimiento'    => $maintenance->iMantenimiento,
            'Articulo'          => $article->Concepto,
            'Precio'            => $article->Precio,
            'Cantidad'          => $inputs['Cantidad'],
            'Importe'           => $amount
        ]);
        $total = $maintenance->Total + $amount;
        $maintenance->Total = $total;
        $maintenance->save();
        return $article_detail;
    }

    public static function deleteArticleDetail($maintenance, $iMttoArticulo){
        $exists_in_detail = Mtto_articulo_detalle::where('iMantenimiento', '=', $maintenance->iMantenimiento)
            ->where('iMttoArticulo', '=', $iMttoArticulo)
            ->first();

        if(!$exists_in_detail){
            throw new Exception('Articulo en detalle invalido');
        }

        $maintenance->Total -= $exists_in_detail->Importe;
        $maintenance->save();
        $exists_in_detail->delete();
    }

}