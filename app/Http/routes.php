<?php

//verificar que el usuario y password son correctos
$app->post('login', 'UsuarioController@postLogin');
$app->post('login/refresh', 'UsuarioController@refreshToken');
$app->post('movil/login', 'RepartidorController@getLogin');
$app->post('movil/logout', 'RepartidorController@logout');
$app->post('movil/invalidate','UsuarioController@invalidateToken');
/**
 * LOG
 */

/* Endpoint para insertar logs */
$app->post('log', 'LogController@postLog');
/* Endpoint para obtener logs recientes */
$app->get('log', 'LogController@getLog');

$app->get('/', function () use ($app) {
    return $app->welcome();
});

//crear un registro para saber en que sucursal se asigno
$app->post('asignacion', 'AsignacionController@registro');


/**
 * SUCURSALES
 */
//obtener todas las sucursales.
$app->get('sucursales', 'SucursalController@getAllSucursales');
//Obtener sucurales promedio
$app->get('sucursales/promedio', 'SucursalController@getAllSucursalesPromedio');
//Obtiene la informacion de Una sucursal en especifico.
$app->get('sucursales/{id}', 'SucursalController@getSucursal');
//Crear o editar sucursal
$app->post('sucursales', 'SucursalController@postSucursal');
$app->post('sucursales/{id}', 'SucursalController@postSucursal');
//Borrar Sucursal
$app->delete('sucursales/{id}', 'SucursalController@deleteSucursal');

/**
 * ROLES (Ahora llamados perfiles)
 */
//Obtiene el listado de Roles
$app->get('roles', 'RolesController@getAll');
//Obtiene un rol en especifico
$app->get('roles/{id}', 'RolesController@getRole');

/**
 * USUARIOS
 */
//Obtener Usuarios.
//Ruta de repatos (MOVILES)
$app->get('usuarios/repartos', 'RepartosController@obtenerRepartos');

$app->get('usuarios/mecanicos','UsuarioController@getMecanicos');

$app->get('usuarios', 'UsuarioController@getAll');
//Obtener Usuario en especifico.
$app->get('usuarios/{id}', 'UsuarioController@getUsuario');
//Crear un usuario
$app->post('usuarios', 'UsuarioController@postUsuario');
//Editar un Usuario
$app->post('usuarios/{id}', 'UsuarioController@postUsuario');
//Borrar un usuario
$app->delete('usuarios/{id}', 'UsuarioController@deleteUsuario');

/**
 * COMENTARIOS
 */
//Obtener catalogo de comentarios
$app->get('comentarios', 'ComentarioController@getAll');
//Obtener 1 comentario
$app->get('comentarios/{id}', 'ComentarioController@getComentario');
//Crear un comentario
$app->post('comentarios', 'ComentarioController@postComentario');
//Actualizar el comentario
$app->post('comentarios/{id}', 'ComentarioController@postComentario');
//Borrar el comentario
$app->delete('comentarios/{id}', 'ComentarioController@deleteComentario');

/**
 * DISPOSITIVOS - EQUIPOS MOVILES
 */
//Obtener catalogo de equipos
$app->get('dispositivos', 'DispositivoController@getAll');
//Obtener 1 Equipo
$app->get('dispositivos/{id}', 'DispositivoController@getDispositivo');
//Crear un Equipo
$app->post('dispositivos', 'DispositivoController@postDispositivo');
//Actualizar el Equipo
$app->post('dispositivos/{id}', 'DispositivoController@postDispositivo');
//Borrar el Equipo
$app->delete('dispositivos/{id}', 'DispositivoController@deleteDispositivo');

/**
 * ESTATUS DISPOSITIVOS
 */
//Metodo para obtener los estatus de un dispositivo.
// Si se requiere mantener este catalogo es necesario generar su propio controllador.
$app->get('estatus_dispositivos', 'DispositivoController@getDispositivosEstatus');

/**
 * ESTATUS MOTOCICLETAS
 */
//Metodo para obtener los estatus de un dispositivo.
// Si se requiere mantener este catalogo es necesario generar su propio controllador.
$app->get('estatusmotocicletas', 'MotocicletaController@getEstatusMotocicletas');

/**
 * RUTAS
 */
//guardar los datos GPS de la venta
$app->post('ruta', 'RutaController@insertar');

//crear un registro del reparto apartir del Folio y el iRepartidor
$app->post('/repartos/nuevo', 'RepartosController@enproceso');
//obtener los repartos asignados al iRepartidor
$app->get('/repartos/pendientes/{iUsuario}', 'RepartosController@pendientes');
$app->get('/ventas/pendientes/{iUsuario}', 'VentaController@ventasPendientes');
//obtener los repartos completados asignados al Repartidor
$app->get('/repartos/completados/{iRepartidor}', 'RepartosController@completados');
//marcar un reparto como completado, para Repartidor
$app->post('/repartos/completar', 'RepartosController@completar');
//Marcar el reparto como Cancelado
$app->post('/repartos/cancelar', 'RepartosController@cancelar');
$app->post('/repartos/cancelar/{iReparto}', 'RepartosController@cancelarEnvio');
$app->get('/repartos', 'RepartosController@getRepartosEnProceso');
$app->get('/repartos/asignado', 'RepartosController@getRepartosAsignados');
$app->post('/repartos/completar/{iReparto}', 'RepartosController@completarEnvio');
/*Contactos*/
$app->get('/contactos/sucursales', 'SucursalController@getContactos');
$app->get('/contactos/repartidores', 'DispositivoController@getContactosDispositivos');
$app->get('/contactos/callcenter', function () use ($app) {
    echo '{"Contacto":"6643863753.","Mensaje":"200"}';
});
$app->get('/contactos/gerente', function () use ($app) {
    echo '{"Gerente":"6644062418","Mensaje":"200"}';
});

//obtener las ventas pendientes mientras el repartidor esta fuera.
$app->get('/ventas/sucursal/{iRepartidor}', 'VentaController@pendientes');
//poner la venta en estatus de enterado, para deshabilitar las notificaciones
$app->post('/ventas/enterado', 'VentaController@enterado');
//crear nueva venta desde la api
$app->post('/ventas/nueva', 'VentaController@nueva');
//alerta
$app->post('alerta', 'RepartidorController@alarma');
/* Endpoint par apoder realizar pruebas de concepto */
$app->get('poc/logs', 'PruebasController@getPrueba');

$app->post('traspaso', 'RepartosController@traspaso');

/* Reportes */
$app->get('reportes/ejecutar/{iReporte}', 'ReportesController@getEjecutarReporte');

$app->get('reportes/{iReporte}', 'ReportesController@getReporte');

    /**
     * REPARTIDORES
     */
    //Obtener ubicacion de repartidor
    $app->get('repartidores/ubicaciones/{iRepartidor}', 'RepartidorController@getUbicacionRepartidor');

    $app->get('repartidores', 'RepartidorController@getAll');

    //Obtener todos los repartidores
    $app->get('repartidores/ubicaciones', 'RepartidorController@getUbicaciones');

    /**
     * DASHBOARD
     */
    //Dashboard ventas
    $app->get('dashboard/ventas', 'VentaController@getAllVentas');
    //Obtiene la ruta del repartidor de una venta en especfico
    $app->get('dashboard/ventas/{id}/ruta', 'VentaController@getRutaVentaReparto');
    /*Ventas generales para todas las sucursales*/
    $app->get('dashboard/chain/ventas', 'VentaController@getVentasCadena');

    $app->get('dashboard/chain/entregas', 'VentaController@getEntregasCadena');

    $app->get('dashboard/chain/promedios', 'VentaController@getPromediosCadena');

    $app->get('dashboard/chain/entregashorario', 'VentaController@getEntregasPorHorario');

    $app->get('dashboard/chain/promedioshorario', 'VentaController@getPromediosPorHorario');
    /* ventas generales por sucursal*/
    $app->get('dashboard/store/ventas', 'VentaController@getEntregasSucursal');
    /*tiempo promedio de entrega en sucursal*/
    $app->get('dashboard/store/promedios', 'VentaController@getPromedioSucursal');
    /*Entregas en sucursal*/
    $app->get('dashboard/store/entregas', 'VentaController@getSucursalEntregas');
    /*Agentes - Entregas*/
    $app->get('dashboard/agentes/entregas', 'VentaController@getEntregasAgente');

    $app->get('/dashboard/repartos', 'RepartosController@getRepartos');
    /*Rutas de repartos */
    $app->get('dashboard/repartos/{id}/ruta', 'RepartosController@getRutaReparto');
    /*obtener numero de venas por agente*/
    $app->get('dashboard/agentes/ventas', 'VentaController@getVentasAgente');
    /*obtener numero de ventas entregadas por Agente*/
    $app->get('dashboard/agentes/entregasventas', 'VentaController@getEntregasVentas');
    /*Listar dispositivos y su estatus.*/
    $app->get('dashboard/dispositivos', 'DispositivoController@getListadoDispositivos');
    //Obtener los mainstats
    $app->get('dashboard/overview/mainstats', 'VentaController@getVentaTotal');
    //Obtiene la grafica
    $app->get('dashboard/overview/graph', 'VentaController@getGraph');
    //Obtiene los repartos entregados por estatus
    $app->get('dashboard/overview/deliveries', 'VentaController@getDelivieries');
    //Obtiene los dispositovos conectados y desconectados
    $app->get('dashboard/overview/devices', 'DispositivoController@getStatusDevices');
    //Rangos de ventas
    $app->get('dashboard/ventas/rangos','VentaController@getGroupOfSales');
    //Transgerencias realizadas
    $app->get('dashboard/ventas/traspasos','VentaController@getTransfers');

    $app->get('dashboard/ventas/canceladas','VentaController@getCancelSales');

    $app->get('dashboard/ventas/motivos-cancelacion','VentaController@getCancelReasons');
    //Obtiene los tiempos de repartos en minutos
    $app->get('dashboard/overview/times', 'RepartosController@getTimesDeliveries');
    //Obtiene el numero de telefonos de los clientes
    $app->get('dashboard/stats/new_clients', 'ClienteController@getPhones');
    //Obtiene el numero de telefonos de los clientes
    $app->get('dashboard/stats/new_clients/deliveries', 'ClienteController@getNewClientDeliveriesStats');
    $app->get('dashboard/posicion/ventas','VentaController@posicionventas');
    /**
     * MOTOCICLETAS
     */
    //Obtener todas
    $app->get('motocicletas', 'MotocicletaController@getAll');
    //Obtener 1
    $app->get('motocicletas/{id}', 'MotocicletaController@getMotocicleta');
    //Agregar
    $app->post('motocicletas', 'MotocicletaController@postMotocicleta');
    //Actualizar
    $app->post('motocicletas/{id}', 'MotocicletaController@postMotocicleta');
    //Borrar
    $app->delete('motocicletas/{id}', 'MotocicletaController@deleteMotocicleta');

    //Rutas del modulo de mantenimiento (CRM)
    $app->get('busqueda/motocicletas', 'MotocicletaController@searchMotocicleta');
    $app->get('mantenimiento', 'MantenimientoController@maintenances');
    $app->get('mantenimiento/reporte', 'MantenimientoController@rangeMaintenances');
    $app->get('mantenimiento/datos', 'MantenimientoController@createMaintenanceOrder');
    $app->get('mantenimiento/{id}/reporte', 'MantenimientoController@getMaintenanceReport');
    $app->post('mantenimiento', 'MantenimientoController@storeMaintenanceOrder');
    $app->get('mantenimiento/{id}', 'MantenimientoController@maintenanceDetail');
    $app->post('mantenimiento/motocicleta/{iMotocicleta}','MantenimientoController@sendToMaintenance');
    $app->post('mantenimiento/{iMantenimiento}', 'MantenimientoController@updateMaintenanceOrder');

    //Articulos Detalle en Mantenimiento (CRM)
    $app->post('mantenimiento/{iMantenimiento}/agregar/articulo', 'MttoDetalleArticuloController@addArticleDetail');
    $app->put('mantenimiento/{iMantenimiento}/actualizar/articulo/{iMttoArticulo}', 'MttoDetalleArticuloController@updateArticleDetail');
    $app->delete('mantenimiento/{iMantenimiento}/eliminar/articulo/{iMttoArticulo}', 'MttoDetalleArticuloController@deleteArticleDetail');

    //Servicios Detalle en mantenimiento (CRM)
    $app->put('mantenimiento/{iMantenimiento}/actualizar/servicio/{iMttoServicio}', 'MttoDetalleServicioController@updateServiceDetail');

    //Mantenimiento (MOVILES)
    $app->post('mantenimiento/observaciones/{mantenimiento}', 'MantenimientoController@maintenanceObservations');
    $app->post('mantenimiento/motocicleta/aceptar/{iMantenimiento}', 'MantenimientoController@acceptMaintenance');
    //Bitacoras (MOVILES)
    $app->get('bitacora/checklist', 'BitacoraController@getChecklist');
    $app->post('bitacora/checklist','BitacoraController@saveChecklist');
    $app->get('bitacora/{id}','BitacoraController@getBitacoraChecklist');

    //Bitacoras (CRM)
    $app->get('bitacora', 'BitacoraController@getBitacora');

    //Articulos (CRM)
    $app->get('articulos', 'Articulo_tallerController@articulos');
    $app->post('articulos', 'Articulo_tallerController@guardarArticulo');
    $app->put('articulos/{id}', 'Articulo_tallerController@actualizarArticulo');
    $app->delete('articulos/{id}', 'Articulo_tallerController@borrarArticulo');
    //Servicios (CRM)
    $app->get('servicios','Servicio_tallerController@servicios');
    $app->post('servicios', 'Servicio_tallerController@guardarServicio');
    $app->put('servicios/{id}', 'Servicio_tallerController@actualizarServicio');
    $app->delete('servicios/{id}', 'Servicio_tallerController@borrarServicio');
    //Clasificaciones (CRM)
    $app->get('clasificaciones','ClasificacionesController@getClasificaciones');






