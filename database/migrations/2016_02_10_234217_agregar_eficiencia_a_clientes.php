<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Cliente;
use App\Models\ClienteEntrega;
use App\Lib\GeoCalculator;

class AgregarEficienciaAClientes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('clientes', function (Blueprint $table) {
            $table->double('Eficiencia')->nullable();
            $table->integer('TotalEntregas')->default(0);
        });

        Cliente::chunk(200, function($clientes) {
            DB::beginTransaction();

            foreach($clientes as $cliente) {
                $entregas = ClienteEntrega::
                    where('iCliente', '=', $cliente->iCliente)
                    ->orderBy('created_at', 'ASC')->get();

                $latBase = null;
                $lonBase = null;

                $inRange = 0;
                $total = 0;

                foreach($entregas as $entrega) {
                    $total += 1;

                    if($latBase == null) {
                        $latBase = $entrega->Latitude;
                        $lonBase = $entrega->Longitude;
                        $inRange += 1;
                        continue;
                    }

                    $distance = GeoCalculator::getDistanceBetweenPoints($latBase, $lonBase, $entrega->Latitude, $entrega->Longitude);

                    if($distance <= intval(env('DELIVERY_RANGE'))) {
                        $inRange += 1;
                    }
                }

                if($total > 0) {
                    $cliente->Eficiencia = ($inRange * 100) / $total;
                    $cliente->TotalEntregas = $total;
                    $cliente->save();
                }
            }

            DB::commit();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('clientes', function (Blueprint $table) {
            $table->dropColumn('Eficiencia');
            $table->dropColumn('TotalEntregas');
        });
    }
}
