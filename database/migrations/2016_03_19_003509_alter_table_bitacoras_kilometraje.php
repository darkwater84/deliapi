<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableBitacorasKilometraje extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bitacoras',  function(Blueprint $table){
            $table->integer('Kilometraje')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bitacoras',  function(Blueprint $table){
            $table->decimal('Kilometraje')->change();
        });
    }
}
