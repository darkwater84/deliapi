<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Servicio_taller;

class CreateTableServiciosTaller extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('servicios_taller', function(Blueprint $table){
            $table->increments('iServicioTaller');
            $table->string('Servicio',70)->unique();
            $table->timestamps();
        });

        Servicio_taller::firstOrCreate([
            'iServicioTaller' => 1,
            'Servicio' => 'Cambio de aceite y bujía'
        ]);

        Servicio_taller::firstOrCreate([
            'iServicioTaller' => 2,
            'Servicio' => 'Lubricado de chicotes'
        ]);

        Servicio_taller::firstOrCreate([
            'iServicioTaller' => 3,
            'Servicio' => 'Limpieza de filtros'
        ]);

        Servicio_taller::firstOrCreate([
            'iServicioTaller' => 4,
            'Servicio' => 'Ajuste de rayos'
        ]);

        Servicio_taller::firstOrCreate([
            'iServicioTaller' => 5,
            'Servicio' => 'Revisión sistema eléctrico'
        ]);
        Servicio_taller::firstOrCreate([
            'iServicioTaller' => 6,
            'Servicio' => 'Calibración de punterías'
        ]);
        Servicio_taller::firstOrCreate([
            'iServicioTaller' => 7,
            'Servicio' => 'Limpieza y lubricado de cadena'
        ]);
        Servicio_taller::firstOrCreate([
            'iServicioTaller' => 8,
            'Servicio' => 'Ajuste de balatas o cambio'
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('servicios_taller');
    }
}
