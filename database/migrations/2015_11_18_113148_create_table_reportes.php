<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Tablas de Reporteo y Parametros.
 */
class CreateTableReportes extends Migration
{
	const SHORT_TEXT = 25;
	const MID_TEXT = 50;
	const LONG_TEXT = 100;
	const TEXT = 255;

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		
		Schema::create('reportes', function (Blueprint $table) {
			$table->increments('iReporte');
			$table->string('Nombre', $this::SHORT_TEXT);
			$table->string('Descripcion', $this::TEXT);
			$table->text('Query');
			$table->boolean('Activo', $this::SHORT_TEXT);
			$table->nullableTimestamps();
			$table->softDeletes();
		});

		Schema::create('reporte_params', function (Blueprint $table) {
			$table->increments('iReporteParams');
			$table->integer('iReporte')->unsigned();
			$table->foreign('iReporte')->references('iReporte')->on('reportes')->onDelete('cascade');
			$table->string('Nombre', $this::SHORT_TEXT);
			$table->string('Descripcion', $this::TEXT);
			$table->string('TipoParametro',$this::SHORT_TEXT);
			$table->nullableTimestamps();
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('reporte_params');
		Schema::drop('reportes');
	}
}
