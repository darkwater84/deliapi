<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Database\Eloquent\SoftDeletes;
use Parse\ParseClient;
use Parse\ParseInstallation;
use Parse\ParsePush;

final class Usuario extends Model implements AuthenticatableContract, CanResetPasswordContract{

    use Authenticatable, CanResetPassword;
    use SoftDeletes;

    protected $table = 'usuarios';
    protected $primaryKey = 'iUsuario';
    protected $dates = ['deleted_at'];
    protected $guarded = array('iUsuario');
    protected $hidden=array('Password');
    protected $fillable = array('Usuario','Password','Nombre','Apellidos','NoEmpleado','Email', 'iRol');
    protected $visible = array('iUsuario','Usuario','Nombre','Apellidos','NoEmpleado','Email',
        'iRol','Rol','Repartidor','created_at', 'updated_at','token', 'Repartidor','Inspeccion','Sucursal');
    // protected $hidden=array('Password');
    const LOGIN = 1;
    const LOGOUT = 2;
    /**
     * Relacion con Roles
     */
    public function Rol(){
        return $this->belongsTo('App\Models\Rol', 'iRol', 'iRol');
    }

    /**
     * Repartidor
     */
    public function Repartidor(){
        return $this->hasOne('App\Models\Repartidor','iUsuario');
    }

    public function getAuthPassword()
    {
        return $this->password;
    }

    public function scopeRol($query, $iRol){
        return $query->where('iRol', '=', $iRol);
    }

    /**
     * @param $repartidor
     * @param $dispositivo
     */
    public static function validateLogin($repartidor, $dispositivo)
    {
        $iDispositivo = null;

        //Obtengo el ultimo log del repartidor
        $lastLog = LogRepartidor::where('iUsuario', '=', $repartidor->iUsuario)
            ->orderBy('created_at', 'DESC')
            ->first();

        //Si existe algun log
        if ($lastLog) {

            //Este log es un login?
            if ($lastLog->Tipo_Registro == self::LOGIN) {

                $today = Carbon::now()->toDateString();

                $lastLogCreated = $lastLog->created_at->format('Y-m-d');

                //El login fue de un dia enterior (no hizo nunca logout)
                if ($lastLogCreated < $today) {
                    //Invalidamos su ultimo login como penalizacion
                    $lastLog->Invalidado = 1;
                    $lastLog->save();

                    //Se le genera el logout de ese login invalidado
                    LogRepartidor::create([
                        'iUsuario' => $repartidor->iUsuario,
                        'Tipo_Registro' => self::LOGOUT,
                        'iDispositivo' => $lastLog->iDispositivo,
                        'Invalidado' => 1
                    ]);

                    LogRepartidor::create([
                        'iUsuario' => $repartidor->iUsuario,
                        'Tipo_Registro' => self::LOGIN,
                        'iDispositivo' => $dispositivo->iDispositivo
                    ]);
                }else{
                    //Se le genera el logout de ese login invalidado
                    LogRepartidor::create([
                        'iUsuario' => $repartidor->iUsuario,
                        'Tipo_Registro' => self::LOGOUT,
                        'iDispositivo' => $lastLog->iDispositivo
                    ]);

                    LogRepartidor::create([
                        'iUsuario' => $repartidor->iUsuario,
                        'Tipo_Registro' => self::LOGIN,
                        'iDispositivo' => $dispositivo->iDispositivo
                    ]);
                }
                if($dispositivo){
                    if($lastLog->iDispositivo != $dispositivo->iDispositivo) {
                        ParseClient::initialize('DiK8MAsgN6u2q6LGgjnGJB0lotZ2YVrMUUGpFhXH', '5JF7eBR3zoDi6vNoihzvQshxfhn8N1FPSuc3k3S5', 'tkrrbH4R9j7LjDgYhT5SUNSTpFAT973gaRoVXQbx');
                        ParsePush::send(array(
                            "where" => ParseInstallation::query()->equalTo("IMEI", $lastLog->Dispositivo->IMEI),
                            "data" => ['doLogout' => true]
                        ));
                    }
                }

            } else {
                LogRepartidor::create([
                    'iUsuario' => $repartidor->iUsuario,
                    'Tipo_Registro' => self::LOGIN,
                    'iDispositivo' => ($dispositivo) ? $dispositivo->iDispositivo: null
                ]);
            }


        }
    }

    public static function validateLogout($repartidor, $dispositivo)
    {
        //Buscamos su ultimo registro
        $lastLog = LogRepartidor::where('iUsuario', '=', $repartidor->iUsuario)
            ->orderBy('created_at', 'DESC')
            ->first();

        if ($lastLog) {
            $today = Carbon::now()->toDateString();
            $lastLogCreated = $lastLog->created_at->format('Y-m-d');
            //Unicamente llegara a almacenar el logout si anteriormente habia un login
            if ($lastLog->Tipo_Registro == self::LOGIN) {
                //Se genera el logout al trabajar el mismo dia
                if ($lastLogCreated == $today) {
                    LogRepartidor::create([
                        'iUsuario' => $repartidor->iUsuario,
                        'Tipo_Registro' => self::LOGOUT,
                        'iDispositivo' => ($dispositivo) ? $dispositivo->iDispositivo: null
                    ]);
                }else
                {
                    $lastDay = strtotime('-1 day', strtotime($today));
                    $lastDay = date('Y-m-d', $lastDay);

                    if($lastLogCreated == $lastDay){
                        //Crea registro del logout en dia anterior
                        LogRepartidor::create([
                            'iUsuario' => $repartidor->iUsuario,
                            'Tipo_Registro' => self::LOGOUT,
                            'iDispositivo' => ($dispositivo) ? $dispositivo->iDispositivo: null,
                            'created_at' => $lastDay . ' 23:59:00'
                        ]);

                        //Crea registro del login
                        LogRepartidor::create([
                            'iUsuario' => $repartidor->iUsuario,
                            'Tipo_Registro' => self::LOGIN,
                            'iDispositivo' => ($dispositivo) ? $dispositivo->iDispositivo: null,
                            'created_at' => $today . ' 00:00:00'
                        ]);

                        //Crea registro del logout
                        LogRepartidor::create([
                            'iUsuario' => $repartidor->iUsuario,
                            'Tipo_Registro' => self::LOGOUT,
                            'iDispositivo' => ($dispositivo) ? $dispositivo->iDispositivo: null,
                        ]);
                    }else{
                        //Invalidamos su ultimo login como penalizacion
                        $lastLog->Invalidado = 1;
                        $lastLog->save();

                        //Crea registro del logout
                        LogRepartidor::create([
                            'iUsuario' => $repartidor->iUsuario,
                            'Tipo_Registro' => self::LOGOUT,
                            'iDispositivo' => ($dispositivo) ? $dispositivo->iDispositivo: null,
                            'Invalidado' => 1
                        ]);
                    }
                }
            }
        }
    }

    /*
  * Obtener la inspeccion del dia y regresar true/false
  */
    public static function hasInspeccion($iUsuario)
    {
        $date = new \DateTime();
        $bitacora= (bool)Bitacora::where('iUsuarioRecibido','=',$iUsuario)
            ->whereDate('created_at','=',$date->format('Y-m-d'))
            ->first();
        return $bitacora;
    }
}
