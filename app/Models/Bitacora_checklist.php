<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Bitacora_checklist extends Model
{
    protected $table = 'bitacora_checklist';
    protected $primaryKey = 'iBitacoraChecklist';
    protected $guarded = array('iBitacoraChecklist');
    public $timestamps = true;
    protected $fillable = array('iChecklist', 'iBitacora', 'Valor', 'Check');
    protected $visible = array('iChecklist', 'iBitacora', 'Valor', 'Check', 'iBitacoraChecklist','bitacora','checklist');

    public static function storeCheckList($values, $iBitacora)
    {
        foreach ($values as $value) {
            self::create([
                'iBitacora'     => $iBitacora,
                'Valor'         => $value['Valor'],
                'Check'         => $value['Check'],
                'iChecklist'    => $value['iCheckList']
            ]);
        }
    }

    public function bitacora(){
        return $this->belongsTo('App\Models\Bitacora','iBitacora');
    }

    public function checklist(){
        return $this->belongsTo('App\Models\Checklist','iChecklist');
    }

}