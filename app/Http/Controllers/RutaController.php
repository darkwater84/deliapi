<?php

namespace App\Http\Controllers;

use App\Models\Ruta;
use App\Models\Repartidor;
use App\Http\Controllers\JelpController;
use Faker\Provider\DateTime;
use Illuminate\Http\Request;
use Validator;

class RutaController extends JelpController {

    /**
     * POST - Recibe la ubicacion de un repartidor.
     */
    public function insertar(Request $request)
    {
        $inputs=$request->all();

        $validator = Validator::make($inputs, [
            'iUsuario' => 'required|numeric',
            'Latitud' => 'required|numeric',
            'Longitud' => 'required|numeric'
        ]);

        if ($validator->fails()) {
            return JelpController::CustomError(['Error'=>'Ruta requiere iRepartidor, Latitud y Longitud válidos.']);
        }

        $latitud = $inputs['Latitud'];
        $longitud = $inputs['Longitud'];

        try
        {
            $repartidor = Repartidor::where('iUsuario','=',$inputs['iUsuario'])->first();
            if($repartidor == null){
                return JelpController::NotFound();
            }
            $now = new \DateTime();
            $ruta = new Ruta();
            $ruta->iRepartidor = $repartidor->iRepartidor;
            $ruta->Latitud = $latitud;
            $ruta->Longitud = $longitud;
            $ruta->created_at = $now->format('Y-m-d H:i:s');
            $ruta->save();
            return JelpController::Respuesta();
        }
        catch(\Exception $e)
        {
            return JelpController::CustomError(['Mensaje'=>$e]);
        }
    }
}
