<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableMantenimientosServicios extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mantenimiento_servicio', function(Blueprint $table){

            $table->renameColumn('iMantenimientoServicio', 'iMttoServicioDetalle');
        });

        Schema::rename('mantenimiento_servicio', 'mtto_servicios_detalle');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mtto_servicios_detalle', function(Blueprint $table){

            $table->renameColumn('iMttoServicioDetalle', 'iMantenimientoServicio');
        });

        Schema::rename('mtto_servicios_detalle', 'mantenimiento_servicio');
    }
}
