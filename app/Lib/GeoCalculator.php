<?php

namespace App\Lib;

class GeoCalculator
{
    const EARTH_RADIUS = 6371000;

    /**
     * @param $lat1
     * @param $lon1
     * @param $lat2
     * @param $lon2
     * @return float Distancia en metros
     */
    public static function getDistanceBetweenPoints($lat1, $lon1, $lat2, $lon2)
    {
        // convert from degrees to radians
        $lat1 = deg2rad($lat1);
        $lon1 = deg2rad($lon1);
        $lat2 = deg2rad($lat2);
        $lon2 = deg2rad($lon2);

        $latDelta = $lat2 - $lat1;
        $lonDelta = $lon2 - $lon1;

        $angle = 2 * asin(sqrt(pow(sin($latDelta / 2), 2) +
                cos($lat1) * cos($lat2) * pow(sin($lonDelta / 2), 2)));

        return $angle * self::EARTH_RADIUS;
    }
}