<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Mtto_estatus extends Model{

    protected $table='mtto_estatus';
    protected $primaryKey = 'iMttoEstatus';
    protected $guarded = array('iMttoEstatus');
    public $timestamps = false;
    protected $fillable = array('Estatus');
    protected $visible = array('iMttoEstatus', 'Estatus');

    const STATUS_PENDING = 1;
    const STATUS_ACEPTED = 2;
    const STATUS_STARTED = 3;
    const STATUS_ENDED = 4;
}