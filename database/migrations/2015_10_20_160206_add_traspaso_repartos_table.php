<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTraspasoRepartosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('repartos', function ($table) {
            $table->boolean('Traspaso')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('repartos', 'Traspaso')) {
            Schema::table('repartos', function ($table) {
                $table->dropColumn('Traspaso');
            });
        }
    }
}
