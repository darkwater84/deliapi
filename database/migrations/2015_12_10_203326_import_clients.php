<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Cliente;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class ImportClients extends Migration
{
    // Pasar los telefonos registrados en ventas a la tabla de clientes
    public function up()
    {
        Cliente::truncate();

        Schema::table('clientes', function (Blueprint $table) {
            $table->unique('Telefono');
        });

        $clientes = DB::table('ventas')
            ->select(DB::raw("DISTINCT(Telefono) as Telefono, created_at, updated_at"))
            ->groupBy('Telefono')
            ->orderBy('created_at')
            ->get();

        $count = count($clientes);

        for($i = 0; $i < $count; $i++) {
            $clientes[$i] = (array) $clientes[$i]; // Express serialization!
        }

        DB::beginTransaction();

        try {
            Cliente::insert($clientes);
            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }

        echo "Clientes importados: $count \n";
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('clientes', function (Blueprint $table) {
            $table->dropUnique('Telefono');
        });
    }
}
