<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNoEmpleadoBitacora extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bitacoras',function(Blueprint $table){
            $table->string('NoEmpleadoEntrega',25)->after('iUsuarioEntrega');
        });

        DB::statement("ALTER TABLE `bitacoras` CHANGE `iUsuarioEntrega` `iUsuarioEntrega` INT(10)  UNSIGNED  NULL");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bitacoras',function(Blueprint $table){
            $table->dropColumn('NoEmpleadoEntrega');
        });
    }
}
