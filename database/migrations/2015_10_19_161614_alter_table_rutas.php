<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableRutas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumn('rutas', 'updated_at')) {
            DB::statement("ALTER TABLE rutas MODIFY Latitud DECIMAL(12,9) NULL DEFAULT NULL");
            DB::statement("ALTER TABLE rutas MODIFY Longitud DECIMAL(12,9) NULL DEFAULT NULL");
            DB::statement("ALTER TABLE rutas DROP COLUMN updated_at");
            DB::statement("ALTER TABLE rutas DROP COLUMN deleted_at");
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('rutas', 'updated_at')) {
            DB::statement("ALTER TABLE rutas MODIFY Latitud DECIMAL(12,9) NULL DEFAULT NULL");
            DB::statement("ALTER TABLE rutas MODIFY Longitud DECIMAL(12,9) NULL DEFAULT NULL");
            DB::statement("ALTER TABLE rutas DROP COLUMN updated_at");
            DB::statement("ALTER TABLE rutas DROP COLUMN deleted_at");
        }

    }
}
