<?php

namespace app\Http\Controllers;

use App\Models\Dispositivo;
use App\Http\Controllers\JelpController;
use Illuminate\Http\Request;
use Validator;
use DB;
use Illuminate\Support\Facades\Input;

/**
 * Controlador de Dispositivos moviles
 */
class DispositivoController extends JelpController  {

const QUERY_ASIGNACIONES=<<<EOD
      SELECT *
      FROM   (SELECT *
        FROM   vw_asignaciones
        ORDER  BY fecha DESC) AS m
      GROUP  BY irepartidor
EOD;

const ONLINE  = 1;

    /**
     * COSNTRUCTOR
     */
    public function __construct(Dispositivo $comentario) {
        $this->comentario = $comentario;
    }

    /*
     * Metodo de entrada a la APP
     * @Param iRepartidor Integer
     * @Param IMEI Varchar(??)
     */
    public function dispositivoPropio(Request $request){
        $inputs=$request->all();
        if(!empty($inputs['iRepartidor']) && !empty($inputs['IMEI'])){
        	//TODO - NO HAY RESPUESTA!
        } else {
            echo json_encode(['respuesta'=>404, 'mensaje'=>'NEc']);
        }
        //TODO - NO HAY RESPUESTA
    }

    /**
     * Obtener telefonos por sucursal para contactos de repartidores
     */
    public  function getContactosDispositivos(){
    	//TODO - DEPRECAR VISTA PARA HACER RELACIONES POR LOS MODELOS
        $query=$this::QUERY_ASIGNACIONES;
        $dispositivos = DB::SELECT(DB::RAW($query));
        echo json_encode(['Repartidores'=>$dispositivos,'Mensaje'=>'200']);
    }

    /**
     * OPERACIONES CRUD
     */

    /**
     * GET - Trae todos los dispositivos
     */
    public function getAll(){
        $dispositivos = Dispositivo::with('Sucursal')->with('Estatus')->get();
        return JelpController::Respuesta($dispositivos);
    }

    /**
     * GET - Metodo para poder obtener una sucursal por id.
     */
    public function getDispositivo($id){
        $dispositivo=Dispositivo::with('Estatus')
        ->with('Sucursal')
        ->find($id);
        if($dispositivo == null){
            return JelpController::NotFound();
        }
        return JelpController::Respuesta($dispositivo);
    }

    /**
     * POST - Crea dispositivo
     */
    public function postDispositivo(Request $request, $id=0){

        $validator = Validator::make($request->all(), [
            'Marca' => 'required',
            'Modelo' => 'required',
            'IMEI' => 'required',
            'Numero' => 'required',
            'iEstatus' => 'required',
            'iSucursal' => 'required'
        ]);

        if ($validator->fails()) {
            return JelpController::CustomError(['Error'=>'Marca, Modelo, IMEI, Numero, Estatus y Sucursal son requeridos.']);
        }

        $dispositivo = Dispositivo::updateOrCreate(
            array( 'iDispositivo' => $id ),
            array( 'Marca' => $request->input('Marca'),
                'Modelo'=> $request->input('Modelo'),
                'IMEI'=> $request->input('IMEI'),
                'Numero'=> $request->input('Numero'),
                'iEstatus'=> $request->input('iEstatus'),
                'iSucursal'=> $request->input('iSucursal')
                ));

        return $this->getDispositivo($dispositivo->iDispositivo);
    }

    /**
     * DELETE - Borra dispositivo
     */
    public function deleteDispositivo($id){
        try {
            $Dispositivo = Dispositivo::find($id);
            if($Dispositivo == null){
                return JelpController::NotFound();
            }
            $Dispositivo->delete();
            return JelpController::Respuesta();
        } catch (ModelNotFoundException $ex){
            return JelpController::CustomError($ex);
        }catch (Exception $e) {
            return JelpController::CustomError($e);
        }
    }

    /**
     * ESTATUS DISPOSITIVO
     */

    /**
     * GET - Estatus del dispositivo
     */
    public function getDispositivosEstatus(Request $request){
        $estatus = DB::table('estatus_dispositivo')->get();
        return JelpController::Respuesta(['Estatus_Dispositivos'=>$estatus]);
    }

    /*Mostrar Dispositivo y su estatus actual, asi como su asignacion*/
    public function getListadoDispositivos()
    {
      try {
        $query = "select if(d.iEstatus=1,'Activo','Inactivo') as Status,d.Numero,d.IMEI,d.VersionApp,d.VersionAndroid,s.NumeroSucursal,s.Sucursal,
                  if(d.iEstatus=1,(
	                select max(concat(vw.Nombre,' ',vw.Apellidos))
	                from vw_asignaciones vw
	                where vw.iDispositivo=d.iDispositivo
                ),'NA') as Repartidor
                  from dispositivos d join sucursales s on d.iSucursal=s.iSucursal";
        $dispositivos = DB::select(DB::raw($query));
        return JelpController::Respuesta($dispositivos);
      } catch (Exception $e) {
        return JelpController::CustomError($e->getMessage());
      }
    }

    public function getStatusDevices()
    {
        $fechaInicio = INPUT::GET('Inicio');
        $fechaFin    = INPUT::GET('Fin');

        if(!$this->validateDate($fechaInicio) || !$this->validateDate($fechaFin)) {
            return JelpController::CustomError('Los parametros Inicio y Fin no son');
        }

        try {
            $elementos = DB::table('dispositivos')
                           ->select(DB::raw("count(IMEI) as Total, if(iEstatus=" . $this::ONLINE . ",'Online','Offline') as Status"))
                           ->whereRaw("date(created_at) between '$fechaInicio' and '$fechaFin'")
                           ->groupBy('iEstatus')
                           ->get();

            $response  = [
                'Online'  => 0,
                'Offline' => 0
            ];

            foreach($elementos as $dispositivosCount) {
                $response[$dispositivosCount->Status] = intval($dispositivosCount->Total);
            }

            return $response;
        } catch (\Exception $e) {
            return JelpController::CustomError($e->getMessage());
        }
    }
}
