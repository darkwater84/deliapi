<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Checklist extends Model{
    protected $table='checklists';
    protected $primaryKey = 'iChecklist';
    protected $guarded = array('iChecklist');
    public $timestamps = true;
    protected $fillable = array('Check', 'TipoDato', 'Orden', 'iGrupoChecklist');
    protected $visible = array('iChecklist', 'Check', 'TipoDato', 'Orden', 'iGrupoChecklist','grupoChecklist');

    public function grupoChecklist(){
        return $this->belongsTo('App\Models\Grupo_checklist','iGrupoChecklist');
    }
}