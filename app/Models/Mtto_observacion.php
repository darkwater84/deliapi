<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Mtto_observacion extends Model{
    protected $table='mtto_observaciones';
    protected $primaryKey = 'iMttoObservacion';
    protected $guarded = array('iMttoObservacion');
    public $timestamps = true;
    protected $fillable = array('Aceptado', 'iUsuario', 'iMantenimiento', 'Comentario');
    protected $visible = array('iMttoObservacion','Aceptado', 'iUsuario', 'iMantenimiento', 'Comentario');
}