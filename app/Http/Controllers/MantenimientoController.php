<?php

namespace App\Http\Controllers;

use App\Lib\ErrorCodes;
use App\Lib\ParseCalls;
use App\Lib\PdfFile;
use App\Models\Dispositivo;
use App\Models\Estatus;
use App\Models\Estatus_repartidor;
use App\Models\EstatusMotocicleta;
use App\Models\Motocicleta;
use App\Models\Mtto_estatus;
use App\Models\Mtto_observacion;
use App\Models\Mtto_servicio_detalle;
use App\Models\Repartidor;
use App\Models\Reparto;
use App\Models\Rol;
use App\Models\Usuario;
use App\Models\Venta;
use Carbon\Carbon;
use App\Models\Mtto_articulo;
use App\Models\Asignacion;
use App\Models\Mantenimiento;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Validator;
use App\Models\Mtto_servicio;
use Illuminate\Http\Request;
use Exception;
use DB;

class MantenimientoController extends JelpController{

    public function maintenances(Request $request){
        $inputs = $request->all();

        $validations = Validator::make($inputs, [
            'Inicio' => 'required|date_format:Y-m-d',
            'Fin' => 'required|date_format:Y-m-d'
        ]);

        $validations->setAttributeNames([
            'Inicio' => 'Inicio',
            'Fin' => 'Fin'
        ]);

        if($validations->fails()){
            return JelpController::CustomError($validations->errors()->first());
        }

        $mantenimientos = Mantenimiento::with('motocicleta', 'sucursal', 'mecanico', 'estatus', 'usuario')
            ->whereDate('FechaEntrada', '>=' , $inputs['Inicio'])
            ->whereDate('FechaEntrada', '<=', $inputs['Fin'])
            ->orderBy('FechaEntrada', 'desc')
            ->get();

        foreach($mantenimientos as $mant){
            $mant->Motocicleta = $mant->motocicleta;
            $mant->Sucursal = $mant->sucursal;
            $mant->Mecanico = $mant->mecanico;
            $mant->Estatus = $mant->estatus;
            $mant->Usuario = $mant->usuario;
         }

        return JelpController::Respuesta($mantenimientos);
    }

    public function createMaintenanceOrder(){
        $servicios = Mtto_servicio::all();
        $articulos = Mtto_articulo::all();
        $mecanicos  = Usuario::join('roles', 'usuarios.iRol', '=', 'roles.iRol')
            ->where('Rol', '=', 'Mantenimiento')
            ->get();
        return JelpController::Respuesta(['servicios' => $servicios, 'articulos' => $articulos, 'mecanicos' => $mecanicos]);
    }

    public function storeMaintenanceOrder(Request $request){
        $inputs = $request->all();
        $iMecanico = $request->input('iMecanico');
        $iMotocicleta = $request->input('iMotocicleta');
        $iSucursal = $request->input('iSucursal');

        $validations = Validator::make($inputs,[
            'iMotocicleta'  => 'required|exists:motocicletas,iMotocicleta,iMotocicleta,' . $iMotocicleta,
            'iMecanico'     => 'required|exists:usuarios,iUsuario,iUsuario,' . $iMecanico,
            'iSucursal'     => 'required|exists:sucursales,iSucursal,iSucursal,' . $iSucursal,
            'Kilometraje'   => 'required|numeric',
            'iUsuario'      => 'required'
        ]);

        $validations->setAttributeNames([
            'iMotocicleta'  => 'iMotocicleta',
            'iMecanico'     => 'iMecanico',
            'iSucursal'     => 'iSucursal',
            'Kilometraje'   =>  'Kilometraje',
            'iUsuario'      =>  'iUsuario'
        ]);

        if($validations->fails())
        {
            return JelpController::CustomError($validations->errors()->first());
        }

        try {
            $motocicleta = Motocicleta::find($iMotocicleta);

            //Si no esta activa, no la tiene asignada ningun usuario por lo que no deberia poder seguir con el mtto.
            if ($motocicleta->iEstatus != Motocicleta::STATUS_ACTIVE) {
                return JelpController::CustomError('Motocicleta fuera de servicio o en mantenimiento');
            }

            DB::beginTransaction();
            //Se crea el nuevo mantenimiento manual
            $mantenimiento = Mantenimiento::saveMaintenanceOrderFirstStep($inputs);
            Mantenimiento::saveMaintananceOrderSecondStep($inputs, $mantenimiento);
            //Se busca el usuario con su ultima asignacion
            $usuario = Usuario::with([
                'Repartidor.ultimaAsignacion' => function($query) {
                    $query->with('Dispositivo')->orderBy('created_at', 'desc')
                        ->take(1);
                }])
                ->where('iUsuario', '=', $mantenimiento->iUsuario)->withTrashed()->first();

            //Esto no deberia suceder, ya que cuando se asigna una moto se genera una asignacion
            if(!$usuario->Repartidor->ultimaAsignacion->first()){
                return JelpController::CustomError('Usuario sin asignacion.');
            }

            $dispositivo = $usuario->Repartidor->ultimaAsignacion->first()->Dispositivo;

            $repartidor = $usuario->Repartidor;
            $repartidor->iEstatus = Estatus_repartidor::IN_MAINTENANCE;
            $repartidor->save();

            $motocicleta->iEstatus = Motocicleta::STATUS_MAINTENANCE;
            $motocicleta->save();

            //Se envia el push notification a traves de parse
            ParseCalls::sendPush([
                'Mensaje'               => 'Motocicleta a mantenimiento',
                'iMantenimiento'        => $mantenimiento->iMantenimiento,
                'EstatusMantenimiento'  => Mtto_estatus::STATUS_PENDING,
                'Mantenimiento'         => true
            ], $dispositivo->IMEI);
            DB::commit();

            $returnMantenimiento = Mantenimiento::with('motocicleta', 'sucursal', 'mecanico', 'estatus', 'Usuario')
                ->where('iMantenimiento','=',$mantenimiento->iMantenimiento)
                ->first();

            $mantenimiento->Motocicleta = $returnMantenimiento->motocicleta;
            $mantenimiento->Sucursal = $returnMantenimiento->sucursal;
            $mantenimiento->Mecanico = $returnMantenimiento->mecanico;
            $mantenimiento->Estatus = $returnMantenimiento->estatus;
            $mantenimiento->Usuario = $returnMantenimiento->usuario;

            return JelpController::Respuesta($mantenimiento);
        }catch (Exception $e){
            DB::rollback();
            return JelpController::CustomError($e->getMessage());
        }
    }

    /**
     * @param Request $request
     * @param $iMantenimiento
     * @return \Illuminate\Http\Response
     * Metodo para actualizar el mantenimiento Datos de entrada/Salida de motocicleta
     */
    public function updateMaintenanceOrder(Request $request, $iMantenimiento)
    {
        if(!is_numeric($iMantenimiento)){
            return JelpController::CustomError('Mantenimiento Inválido');
        }

        $inputs = $request->all();

        $iMecanico = $request->input('iMecanico');
        $iMotocicleta = $request->input('iMotocicleta');
        $iSucursal = $request->input('iSucursal');

        /* Reglas de Validacion de parametros:
        *   - En caso de que no venga la hora de salida se validan todos los demas campos
        *   - En caso contrario se valida solamente la hora de salida.
        */
        $validations = Validator::make($inputs,[
            'iMecanico'     => 'required_without:HoraSalida|required_with:Kilometraje,HoraEntrada|exists:usuarios,iUsuario,iUsuario,' . $iMecanico,
            'Kilometraje'   => 'required_without:HoraSalida|required_with:iMecanico,HoraEntrada|numeric',
            'HoraEntrada'   => 'required_without:HoraSalida|required_with:iMecanico,Kilometraje|date_format:H:i:s',
            'HoraSalida'    => 'required_without_all:iMecanico,Kilometraje|date_format:H:i:s'
        ]);

        $validations->setAttributeNames([
            'Kilometraje'   => 'Kilometraje',
            'HoraSalida'    => 'HoraSalida',
            'iMecanico'     => 'iMecanico',
            'HoraEntrada'   => 'HoraEntrada'
        ]);

        if($validations->fails())
        {
            return JelpController::CustomError($validations->errors()->first());
        }

        try {
            if (isset($inputs['HoraSalida'])) {
                $maintenance = Mantenimiento::findOrFail($iMantenimiento);
                if(!$maintenance->HoraEntrada){
                    return JelpController::CustomError('Mantenimiento sin hora de entrada');
                }
                $maintenance = Mantenimiento::saveMaintananceOrderThirdStep($maintenance);

                //Realiza la busqueda de la ultima asignacion basandose en el repartidor
                $repartidor = Repartidor::with([
                    'ultimaAsignacion' => function ($query) {
                        $query->with('Dispositivo')->orderBy('created_at', 'desc')
                            ->take(1);
                    }])
                    ->where('iUsuario', '=', $maintenance->iUsuario)
                    ->first();

                ParseCalls::sendPush([
                    'Mensaje' => 'Tu motocicleta esta lista!',
                    'iMantenimiento' => $maintenance->iMantenimiento,
                    'EstatusMantenimiento' => Mtto_estatus::STATUS_STARTED
                ], $repartidor->ultimaAsignacion->first()->Dispositivo->IMEI);
            } else {
                //Almacena los datos principales del mantenimiento
                $maintenance = Mantenimiento::saveMaintenanceOrderFirstStep($inputs, $iMantenimiento);

                //Almacena los detalles de servicios/articulos
                Mantenimiento::saveMaintananceOrderSecondStep($inputs, $maintenance);
            }

            return JelpController::Respuesta($maintenance);
        }
        catch(ModelNotFoundException $e){
            return JelpController::CustomError('Mantenimiento Inválido');
        }
        catch (Exception $e){
            return JelpController::CustomError($e->getMessage());
        }
    }
    /**
     * @param $id
     * @return \Illuminate\Http\Response
     * @description Metodo para obtener el detalle del mantenimiento
     */
    public function maintenanceDetail($id){
        try {
            $mantenimiento = Mantenimiento::search($id);
            $total = $mantenimiento->Total * 0.16;
            $mantenimiento->Total = $mantenimiento->Total + $total;
            return JelpController::Respuesta($mantenimiento);
        }catch(Exception $e){
            return JelpController::CustomError($e->getMessage());
        }
    }

    public function rangeMaintenances(Request $request)
    {

        $inputs = $request->all();

        $validations = Validator::make($inputs, [
            'Inicio' => 'required|date_format:Y-m-d',
            'Fin' => 'required|date_format:Y-m-d'
        ]);

        $validations->setAttributeNames([
            'Inicio' => 'Inicio',
            'Fin' => 'Fin'
        ]);

        if ($validations->fails()) {
            return JelpController::CustomError($validations->errors()->first());
        }

        $maintenances = DB::table('mantenimientos')
            ->select(DB::raw("mantenimientos.iMantenimiento, concat(FechaEntrada, ' ', HoraEntrada) as Entrada,
            concat(FechaSalida, ' ', HoraSalida) as Salida, Marca, Modelo, Kilometraje, Placas, Estatus as Estado,
            concat(m.Nombre, ' ', m.Apellidos) as Mecanico, concat(u.NoEmpleado, ' ', u.Nombre, ' ', u.Apellidos) as Empleado,
            Observacion as Observaciones, Cantidad, Articulo as Concepto,mtto_articulos.Categoria, mtto_articulos_detalle.Precio"))
            ->leftJoin('mtto_articulos_detalle', 'mantenimientos.iMantenimiento', '=', 'mtto_articulos_detalle.iMantenimiento')
            ->leftJoin('motocicletas', 'mantenimientos.iMotocicleta', '=', 'motocicletas.iMotocicleta')
            ->leftJoin('mtto_estatus', 'mantenimientos.iMttoEstatus', '=', 'mtto_estatus.iMttoEstatus')
            ->leftJoin('usuarios as m', 'mantenimientos.iMecanico', '=', 'm.iUsuario')
            ->leftJoin('usuarios as u', 'mantenimientos.iUsuario', '=', 'u.iUsuario')
            ->leftjoin('mtto_articulos', 'mtto_articulos_detalle.iMttoArticulo', '=', 'mtto_articulos.iMttoArticulo')
            ->whereDate('FechaEntrada', '>=', $inputs['Inicio'])
            ->whereDate('FechaEntrada', '<=', $inputs['Fin'])
            ->where('mantenimientos.iMttoEstatus', Mtto_estatus::STATUS_ENDED)
            ->orderBy('mantenimientos.iMantenimiento', 'asc')
            ->orderBy('mantenimientos.created_at', 'asc')
            ->get();


        return JelpController::Respuesta($maintenances);
    }

    /**
     * @param Request $request
     * @param $iMotocicleta
     * @return \Illuminate\Http\Response
     * @method POST
     * @description Metodo para enviar motocicleta a mantenimiento
     */
    public function sendToMaintenance($iMotocicleta)
    {
        try {

            if(!is_numeric($iMotocicleta)){
                return JelpController::CustomError('Motocicleta Invalida');
            }

            DB::beginTransaction();
            //Se busca la motocicleta con la ultima asinacion y el dispositivo ligado a la asignacion
            $motorcycle = Motocicleta::where('iMotocicleta', '=', $iMotocicleta)
                ->with([
                    'ultimaAsignacion' => function($query){
                    $query->with('Dispositivo')->orderBy('created_at','desc')
                        ->take(1);
                    },
                    'ultimaAsignacion.Repartidor'
                ])
                ->first();

            //validar que el objeto motocicleta no venga vacio, y que tenga una asignacion
            if(!($motorcycle) OR count($motorcycle->ultimaAsignacion)==0){
                return JelpController::CustomError('No se encontró motocicleta o historial de asignación');
            }

            //Aunque en la relacion se pide un solo regsistro sigue siendo una collecion
            $asignacion = $motorcycle->ultimaAsignacion->first();

            /*
             * Si la moto no esta activa, no se le debe permitir mandar a mantenimiento
             * Debido a que o ya se encuentra en mantenimiento o no esta activo (siendo utilizada)
             */
            if ($motorcycle->iEstatus != Motocicleta::STATUS_ACTIVE) {
                return JelpController::CustomError('Motocicleta fuera de servicio o en mantenimiento');
            }

            $dispositivo = $asignacion->Dispositivo;

            $repartidor = $asignacion->Repartidor;
            $repartidor->iEstatus = Estatus_repartidor::IN_MAINTENANCE;
            $repartidor->save();

            //Se actualiza el estatus de la motocicleta
            $motorcycle->iEstatus = Motocicleta::STATUS_MAINTENANCE;
            $motorcycle->save();

            //Se crea un registro de mantenimiento con el usuario y la moto que deben arribar
            $mantenimiento = Mantenimiento::firstOrCreate([
                'iUsuario'      => $asignacion->Repartidor->iUsuario,
                'iMotocicleta'  => $motorcycle->iMotocicleta,
                'iSucursal'     => $asignacion->iSucursal,
                'FechaEntrada'  => Carbon::now()->toDateString()
            ]);

            $mantenimiento->iMttoEstatus = Mtto_estatus::STATUS_PENDING;
            $mantenimiento->Pendiente = Carbon::now()->toDateTimeString();
            $mantenimiento->save();

            $services = Mtto_servicio::select('iMttoServicio')
                ->get();

            $countServices = count($services);

            for($i = 0; $i < $countServices; $i++) {
                $service = $services[$i];
                Mtto_servicio_detalle::create(
                    [
                        'iMttoServicio' => $service->iMttoServicio,
                        'iMantenimiento' => $mantenimiento->iMantenimiento,
                        'ServicioAplicado' => Mtto_servicio::SERVICE_NOT_APPLIED
                    ]);
            }

            //Se envia el push notification a traves de parse
            ParseCalls::sendPush([
                'Mensaje'               => 'Motocicleta a mantenimiento',
                'iMantenimiento'        => $mantenimiento->iMantenimiento,
                'EstatusMantenimiento'  => Mtto_estatus::STATUS_PENDING,
                'Mantenimiento'         => true
            ], $dispositivo->IMEI);

            DB::commit();
            return JelpController::Creado($mantenimiento);

        }
        catch (ModelNotFoundException $e){
            DB::rollback();
            return JelpController::CustomError('Motocicleta invalida');
        }
        catch (Exception $e) {
            DB::rollback();
            return JelpController::CustomError($e->getMessage());
        }
    }


    /**
     * @param $iMantenimiento
     * @return \Illuminate\Http\Response
     * @description Metodo para aceptar ir a mantenimiento MOVIL
     */
    public function acceptMaintenance($iMantenimiento){

        if(!is_numeric($iMantenimiento)){
            return JelpController::CustomError('Mantenimiento Inválido');
        }

        $mantenimiento = Mantenimiento::find($iMantenimiento);

        if(!$mantenimiento){
            return JelpController::CustomError('Mantenimiento Inválido');
        }

        //Verifica que el mantenimiento no haya sido aceptado con anterioridad
        if($mantenimiento->iMttoEstatus != Mtto_estatus::STATUS_PENDING)
        {
            return JelpController::Respuesta(['Mensaje' => 'Mantenimiento ya habia sido aceptado']);
        }

        $repartidor = Repartidor::where('iUsuario', '=', $mantenimiento->iUsuario)
            ->first();

        $repartos = Venta::join('repartos', 'ventas.iVenta', '=', 'repartos.iVenta')
            ->where('iRepartidor', '=', $repartidor->iRepartidor)
            ->where('ventas.iEstatus', '=', Estatus::TRANSIT)
            ->whereRaw("Date(repartos.created_at) = Date('" . Carbon::now()->toDateString() . "')")
            ->count();

        //Si tiene repartos pendientes
        if($repartos)
        {
            return JelpController::Forbidden(ErrorCodes::DELIVERY_IN_PROCESS_ERROR_CODE);
        }

        $mantenimiento->iMttoEstatus = Mtto_estatus::STATUS_ACEPTED;
        $mantenimiento->Aceptado = Carbon::now()->toDateTimeString();
        $mantenimiento->save();
        return JelpController::Respuesta(['Mensaje' => 'Mantenimiento Aceptado']);
    }

    /**
     * @param Request $request
     * @param $iMantenimiento
     * @return \Illuminate\Http\Response
     * @description Metodo para movil, que almacena la respuesta de conformidad del mantenimiento que se
     * le ha realizado a la motocicleta
     */
    public function maintenanceObservations(Request $request, $iMantenimiento){

        if(!is_numeric($iMantenimiento)){
            return JelpController::CustomError('Mantenimiento Inválido');
        }

        $inputs = $request->all();
        $iUsuario = $request->input('iUsuario');
        $validations = Validator::make($inputs, [
            'Aceptado' => 'required|boolean',
            'iUsuario' => 'required|exists:usuarios,iUsuario,iUsuario,' .  $iUsuario
        ]);

        if($validations->fails()){
            return JelpController::CustomError($validations->errors()->first());
        }

        $maintenance = Mantenimiento::find($iMantenimiento);

        if(!$maintenance){
            return JelpController::CustomError('Mantenimiento Inválido');
        }

        $observation = Mtto_observacion::create([
            'Aceptado'          => $inputs['Aceptado'],
            'iUsuario'          => $inputs['iUsuario'],
            'iMantenimiento'    => $iMantenimiento
        ]);

        //Si el mantenimiento contiene alguna observacion se almacena
        if(isset($inputs['Observacion'])){
            $observation->Comentario = $inputs['Observacion'];
            $observation->save();
        }

        //Se busca el repartidor para volverlo a reactivar
        $repartidor = Repartidor::with(
            ['ultimaAsignacion' => function($query){
            $query->with('Dispositivo')->orderBy('created_at','desc')
                ->take(1);
            }])
            ->where('iUsuario', '=', $maintenance->iUsuario)
            ->first();

        if($repartidor->iEstatus == Estatus_repartidor::IN_MAINTENANCE){
            $repartidor->iEstatus = Estatus_repartidor::ACTIVE;
            $repartidor->save();
        }

        ParseCalls::sendPush(
            [
                'Mantenimiento' => false,
                'Mensaje'       => 'Mantenimiento finalizado!',
                'EstatusMantenimiento' => Mtto_estatus::STATUS_ENDED,
                'iMantenimiento' => $iMantenimiento
            ],
            $repartidor->ultimaAsignacion->first()->Dispositivo->IMEI);

        return JelpController::Creado($observation);
    }

    /**
     * #path /reporte/mantenimiento/:id
     * @param $maintenance
     * @paramDescription Id del mantenimiento
     * @throws Exception
     * @description Obtiene el reporte de un mantenimiento en PDF
     */
    public function getMaintenanceReport($maintenance){

        //Obtiene el mantenimiento con sus relaciones
        $mtto = Mantenimiento::with('detalle', 'motocicleta', 'mecanico', 'servicio.servicio', 'sucursal', 'usuario')
            ->find($maintenance);

        $servicios = $mtto->servicio->filter(function($item){
            if($item->ServicioAplicado){
                return $item;
            }
        })->values()->all();

        // Se inicializa el nuevo documento
        $file = new PdfFile();

        $fileName = 'Reporte_Mantenimiento_' . $mtto->iMantenimiento;

        //Agrega la configuracion del archivo
        $pdf = $file->PdfConfiguration($file);

        //Formatea el valor del subtotal
        $subtotal =number_format($mtto->detalle->sum('Importe'), 2, '.', '');
        $iva = $subtotal * .16;

        //Agrega la primera pagina
        $pdf->AddPage();

        //Hace el render de la vista enviando parametros para mostrarlos
        $pdf->writeHTML(view('maintenance',
            [
                'mantenimiento' => $mtto,
                'servicios' => $servicios,
                'detalles'  => $mtto->detalle,
                'motocicleta' => $mtto->motocicleta,
                'mecanico' => $mtto->mecanico,
                'subtotal'  => $subtotal,
                'sucursal'  => $mtto->sucursal,
                'iva'       => $iva,
                'repartidor' => $mtto->usuario
            ]), true, false, false, false, '');

        //Cierra el documento y lo muestra
        $pdf->Output($fileName . '.pdf' , 'I');

    }

}