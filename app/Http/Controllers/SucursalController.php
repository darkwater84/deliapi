<?php

namespace app\Http\Controllers;

use App\Models\Sucursal;
use App\Http\Controllers\JelpController;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Support\Facades\Input;
use DB;

class SucursalController extends JelpController{

const QUERY_SUCURSALES_GRAL = <<<EOD
select *,(select count(iVenta) from vw_repartos  where vw_repartos.iSucursal= s.iSucursal and Date(FechaVenta)=CURDATE()) as TotalVentas, (select (sum(time_to_sec(timediff(vw.HoraEntrega,vw.HoraSalida))))/count(vw.iReparto) as diff from vw_repartos vw where date(FechaVenta)=curdate() and vw.iSucursal=s.iSucursal) as Promedio from sucursales s;
EOD;

const QUERY_SUCURSALES_FECHA = <<<EOD
SELECT *,(select count(iVenta)
from vw_repartos
where vw_repartos.iSucursal= s.iSucursal and Date(FechaVenta) BETWEEN '%s' and '%s') as TotalVentas,
(SELECT SEC_TO_TIME((SUM(TIME_TO_SEC(TIMEDIFF(vw.HoraEntrega,vw.HoraSalida))))/COUNT(vw.iReparto)) AS diff
FROM vw_repartos vw
WHERE date(FechaVenta) BETWEEN '%s' AND '%s' AND vw.iSucursal=s.iSucursal) AS Promedio
FROM sucursales s;
EOD;
    /**
     * Metodo para regresar todas las sucursales. DEPRECAR !!!
     */
    public function getAll(Request $request){

        $sucursales=(new Sucursal())
        	->all();
        return JelpController::Deprecado(['Sucursales'=>$sucursales], null);
    }

    /**
     * Metodo para regresar todas las sucursales.
     */
    public function getAllSucursalesPromedio(){
      $inicio = Input::get('Inicio');
      $fin = Input::get('Fin');
      if(!empty($inicio) && !empty($fin)){
        $query=sprintf($this::QUERY_SUCURSALES_FECHA,$inicio, $fin,$inicio,$fin);
      }else{
        $query=sprintf($this::QUERY_SUCURSALES_GRAL);
      }
      $sucursales = DB::select( DB::raw( $query ) );
        return JelpController::Respuesta(['Sucursales'=>$sucursales]);
    }

    public function getAllSucursales()
    {
      $sucursales = Sucursal::all();
      return JelpController::Respuesta(['Sucursales'=>$sucursales]);
    }

    /**
     * Metodo para poder obtener una sucursal por id.
     */
    public function getSucursal($id){
        $sucursales=(new Sucursal())
        	->find($id);
        if($sucursales == null){
        	return JelpController::NotFound();
        }
        return JelpController::Respuesta($sucursales);
    }

    /**
     * POST - Metodo para poder editar una sucursal o crear una nueva en caso de no existir.
     */
    public function postSucursal(Request $request, $id = 0){

        $validator = Validator::make($request->all(), [
            'NumeroSucursal' => 'required',
            'Sucursal' => 'required',
            'Direccion' => 'required',
            'Latitud' => 'required|numeric',
            'Longitud' => 'required|numeric',
            'Telefono' => 'required'
        ]);

        if ($validator->fails()) {
            return JelpController::CustomError(['Error'=>'NumeroSucursal, Sucursal, Direccion, Latitud, Longitud, Telefono son requeridos.']);
        }

        $usuario = Sucursal::updateOrCreate(
            array( 'iSucursal' => $id ),
            array( 'NumeroSucursal' => $request->input('NumeroSucursal'),
                'Sucursal'=> $request->input('Sucursal'),
                'Direccion'=> $request->input('Direccion'),
                'Email'=> $request->input('Email'),
                'Latitud'=> $request->input('Latitud'),
                'Longitud'=> $request->input('Longitud'),
                'Telefono'=> $request->input('Telefono')
                ));

        return JelpController::Respuesta(['Sucursal'=>$usuario]);
    }

    /**
     * Metodo para poder eliminar el usuario
     */
    public function deleteSucursal($id){
        try {
            $sucursal = Sucursal::find($id);
            if($sucursal == null){
                return JelpController::NotFound();
            }
            $sucursal->delete();
            return JelpController::Respuesta();

        } catch (ModelNotFoundException $ex){
            return JelpController::CustomError($ex);
        }catch (Exception $e) {
            return JelpController::CustomError($e);
        }
    }

    /**
     * Metodo para poder obtener los contactos de una sucursal.
     */
    public function getContactos(){
        $all=(new Sucursal())
        	->select('NumeroSucursal','Sucursal','Telefono')
        	->get();
        return JelpController::Respuesta(['Contactos'=>$all]);
    }

}
