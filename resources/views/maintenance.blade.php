
<!DOCTYPE html>
<html>
<head>
    <style>
        .center{
            vertical-align: middle;
            text-align:center;
            color: #000;
        }

        .right{
            vertical-align: middle;
            text-align:right;
            color: #000;
        }

        .left{
            vertical-align: middle;
            text-align:left;
            color: #000;
        }

        td {
            color:#6e7478;
        }

        label{
            display: block;
            font-size: 13px;
            color:#000;
        }

        .graphic{
            height: 500px;
            background-color: #fff;
        }

        .graphic h2{
            color: #fff;
        }

        h2{
            margin-top: 0;
        }

        h1{
            font-size: 22px;
            margin: 0;
        }
        
        .bordercolumn{
            border: 1px solid #000000;;
        }

    </style>
</head>
<body>
<div style="width: 100%;">
    <div style="width:100%; text-align: center">
        <table style="width: 100%" cellspacing="5" cellpadding="0">
            <tr>
                <td class="left" style="width:24%;">
                    <h2>Fecha: <u> {{$mantenimiento->FechaEntrada}} </u></h2>
                </td>
                <td class="center" style="width: 36%">
                    <h2>Mecanico: <u> {{$mecanico->Nombre . ' ' . $mecanico->Apellidos}} </u></h2>
                </td>
                <td class="right" style="width: 36%;">
                    <h2>Repartidor: <u>{{$repartidor->NoEmpleado . ' ' . $repartidor->Nombre . ' ' . $repartidor->Apellidos}}</u></h2>
                </td>
            </tr>
        </table>
        <table>
            <tr style="background-color: #dbdbdb;">
                <td class="bordercolumn"><label>Modelo</label></td>
                <td class="bordercolumn"><label>Sucursal</label></td>
                <td class="bordercolumn"><label>Placa</label></td>
                <td class="bordercolumn"><label>KM</label></td>
            </tr>
            <tr>
                <td class="bordercolumn"><label>{{$motocicleta->Modelo}}</label></td>
                <td class="bordercolumn"><label>{{$sucursal->NumeroSucursal . ' ' . $sucursal->Sucursal}}</label></td>
                <td class="bordercolumn"><label>{{$motocicleta->Placas}}</label></td>
                <td class="bordercolumn"><label>{{$mantenimiento->Kilometraje}}</label></td>
            </tr>
            <tr>
                <td colspan="3" class="right"><label>Hora Entrada  </label></td>
                <td class="bordercolumn"><label>{{$mantenimiento->HoraEntrada}}</label></td>
            </tr>
            <tr>
                <td colspan="3" class="right"><label>Hora salida  </label></td>
                <td class="bordercolumn"><label>{{$mantenimiento->HoraSalida}}</label></td>
            </tr>
        </table>
        <br/>
    </div>
    <br/>

    <table width="100%" cellspacing="0" cellpadding="2">
        <tr style="background-color: #dbdbdb;">
            <td style="width: 35%" class="bordercolumn center"><label>Servicio</label></td>
            <td style="width: 15%" class="bordercolumn center"><label></label></td>
            <td style="width: 35%" class="bordercolumn center"><label>Servicio</label></td>
            <td style="width: 15%" class="bordercolumn center"><label></label></td>
        </tr>
        @if(count($servicios))

            @foreach($servicios as $key => $servicio)
                @if($key % 2 == 0)
                    <tr>
                        <td style="width: 35%" class="bordercolumn center"><label>{{$servicio->servicio->Servicio}}</label></td>
                        <td style="width: 15%" class="bordercolumn center"><label>{{($servicio->ServicioAplicado) ? 'Si': 'No'}}</label></td>
                        @if($key == (count($servicios)-1))
                                <td></td>
                                <td></td>
                            </tr>
                        @endif
                @else
                        <td style="width: 35%" class="bordercolumn center"><label>{{$servicio->servicio->Servicio}}</label></td>
                        <td style="width: 15%" class="bordercolumn center"><label>{{($servicio->ServicioAplicado) ? 'Si': 'No'}}</label></td>
                    </tr>
                @endif
            @endforeach
        @else
            <tr>
                <td colspan="4" class="bordercolumn center">
                    <label>No se realizo ningun servicio</label>
                </td>
            </tr>
        @endif
    </table>

    <br/>
    <br/>
    <br/>
    <table width="100%" cellspacing="0" cellpadding="2">
        <tr style="background-color: #dbdbdb;">
            <td class="bordercolumn" style="width:10%;"></td>
            <td class="bordercolumn center" style="width: 50%;"><label>Concepto</label></td>
            <td class="bordercolumn center" style="width: 20%;"><label>Precio Unitario</label></td>
            <td class="bordercolumn center" style="width: 20%;"><label>Importe</label></td>
        </tr>
        @if(count($detalles))
            @foreach($detalles as $detalle)
                <tr>
                    <td class="bordercolumn center"><label>{{$detalle->Cantidad}}</label></td>
                    <td class="bordercolumn"><label>{{ucfirst(strtolower($detalle->Articulo))}}</label></td>
                    <td class="bordercolumn right"><label>${{$detalle->Precio}}</label></td>
                    <td class="bordercolumn right"><label>${{$detalle->Importe}}</label></td>
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="4" class="bordercolumn center">
                    <label>No hay articulos ligados a este mantenimiento</label>
                </td>
            </tr>
        @endif
        <tr>
            <td colspan="3" class="right"><label>Subtotal</label></td><td class="right bordercolumn"><label>${{$subtotal}}</label></td>
        </tr>
        <tr>
            <td colspan="3" class="right"><label>IVA</label></td><td class="right bordercolumn"><label>${{number_format($iva, 2, '.', '')}}</label></td>
        </tr>
        <tr>
            <td colspan="3" class="right"><label>Total</label></td><td class="right bordercolumn"><label>${{number_format(($subtotal + $iva), 2, '.', '')}}</label></td>
        </tr>
    </table>
    <br/>
    <br/>
    @if($mantenimiento->Servicio)
        <table style="margin-top: 10px;">
            <tr>
                <td width="25%;" class="left">
                    <label style="font-size:18px;"><b>Serv. Adicional:</b></label>
                </td>
                <td width="75%" class="left">
                    <label><u>{{$mantenimiento->Servicio}}</u></label>
                </td>
            </tr>
        </table>
    @endif
    <br/>
    <br/>
    @if($mantenimiento->Observacion)
        <table style="margin-top: 10px;">
            <tr>
                <td width="25%;" class="left">
                    <label style="font-size:18px;"><b>Observaciones:</b></label>
                </td>
                <td width="75%" class="left">
                    <label><u>{{$mantenimiento->Observacion}}</u></label>
                </td>
            </tr>
        </table>
    @endif
</div>
</body>
</html>

