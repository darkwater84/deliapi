<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();
        $this->call('DeleteSeeder');
        $this->call('SucursalSeeder');
        $this->call('EstatusSeeder');
        $this->call('DispositivoEstatusSeeder');
        $this->call('DispositivoSeeder');
        $this->call('RepartidosEstatusSeeder');
        $this->call('MotocicletasEstatusSeeder');
        $this->call('MotoSeeder');
        $this->call('RolSeeder');
        $this->call('ComentarioSeeder');
        $this->call('UsuariosSeeder');
	}

}

class DeleteSeeder extends Seeder
{
    public function run()
    {
        DB::table('sucursales')->delete();
        DB::table('estatus')->delete();
        DB::table('estatus_dispositivo')->delete();
        DB::table('dispositivos')->delete();
        DB::table('estatus_repartidor')->delete();
        DB::table('motocicletas')->delete();
        DB::table('repartidores')->delete();
        DB::table('roles')->delete();
        DB::table('comentarios')->delete();

    }
}
class UsuariosSeeder extends Seeder{
    public function run(){
        \App\Models\Usuario::create(array(
            'Usuario'=>'sa',
            'Password'=>'sa',
            'Nombre'=>'S',
            'Apellidos'=>'A',
            'NoEmpleado'=>'0',
            'Email'=>'sa@sa.com',
            'iRol'=>'1'
            ));
        \App\Models\Usuario::create(array(
            'Usuario'=>'Admin',
            'Password'=>'Admin',
            'Nombre'=>'Admin',
            'Apellidos'=>'Admin',
            'NoEmpleado'=>'1',
            'Email'=>'admin@admin.com',
            'iRol'=>'2'
            ));
        \App\Models\Usuario::create(array(
            'Usuario'=>'supervisor',
            'Password'=>'supervisor',
            'Nombre'=>'Supervisor',
            'Apellidos'=>'001',
            'NoEmpleado'=>'001',
            'Email'=>'supervisor@supervisor.com',
            'iRol'=>'3'
            ));
        \App\Models\Usuario::create(array(
            'Usuario'=>'asistente',
            'Password'=>'asistente',
            'Nombre'=>'Asistente',
            'Apellidos'=>'002',
            'NoEmpleado'=>'002',
            'Email'=>'asistente@asistente.com',
            'iRol'=>'4'
            ));
        \App\Models\Usuario::create(array(
            'Usuario'=>'repartidor',
            'Password'=>'repartidor',
            'Nombre'=>'Repartidor',
            'Apellidos'=>'003',
            'NoEmpleado'=>'003',
            'Email'=>'repartidor@repartidor.com',
            'iRol'=>'5'
            ));
    }
}

class RolSeeder extends Seeder
{
    public function run()
    {
        //SUPER USUARIO!
        \App\Models\Rol::create(array('Rol'=>'SuperUsuario'));
        //Dentro de la app
        \App\Models\Rol::create(array('Rol'=>'Gerente'));
        \App\Models\Rol::create(array('Rol'=>'Supervisor'));
        \App\Models\Rol::create(array('Rol'=>'Asistente'));
        //Repartidor
        \App\Models\Rol::create(array('Rol'=>'Repartidor'));
    }
}

class ComentarioSeeder extends Seeder
{
    public function run()
    {
        \App\Models\Comentario::create(array('Comentario'=>'NA'));
        \App\Models\Comentario::create(array('Comentario'=>'Pedido incorrecto'));
        \App\Models\Comentario::create(array('Comentario'=>'Cliente inconforme'));
        \App\Models\Comentario::create(array('Comentario'=>'No contesta'));
        \App\Models\Comentario::create(array('Comentario'=>'Pago no realizado'));
        \App\Models\Comentario::create(array('Comentario'=>'Problema con tarjeta'));
    }
}

class SucursalSeeder extends Seeder
{
    public function run()
    {
        \App\Models\Sucursal::create(array(
            'Sucursal'=>'Palmas',
            'NumeroSucursal'=>'001',
            'Email'=>'josue@josue.com',
            'Direccion'=>'Ave. Lazaro Cardenas #2345',
            'Latitud'=>'',
            'Longitud'=>'',
            'Telefono'=>'6567899'
        ));
    }
}

class EstatusSeeder extends Seeder
{
    public function run()
    {
        \App\Models\Estatus::create(array(
            'Estatus'=>'Encendido'
        ));
        \App\Models\Estatus::create(array(
            'Estatus'=>'Apagado'
        ));
    }
}

class RepartidosEstatusSeeder extends Seeder
{
    public function run()
    {
        \App\Models\Estatus_repartidor::create(array(
            'Estatus'=>'Activo'
        ));

        \App\Models\Estatus_repartidor::create(array(
            'Estatus'=>'Inactivo'
        ));
    }
}

class MotocicletasEstatusSeeder extends Seeder
{
    public function run()
    {
        \App\Models\EstatusMotocicleta::create(array(
            'Estatus'=>'Activo'
        ));

        \App\Models\EstatusMotocicleta::create(array(
            'Estatus'=>'Inactivo'
        ));
    }
}

class MotoSeeder extends Seeder
{
    public function run()
    {
        \App\Models\Motocicleta::create(array(
            'Marca'=>'Harley',
            'Modelo'=>'Harley Davidson',
            'Placas'=>'BTTF01',
            'iEstatus'=>'1'
        ));
    }
}

class DispositivoEstatusSeeder extends Seeder
{
    public function run()
    {
        \App\Models\Estatus_dispositivo::create(array(
            'Estatus'=>'Activo'
        ));
        \App\Models\Estatus_dispositivo::create(array(
            'Estatus'=>'Inactivo'
        ));
    }
}

class DispositivoSeeder extends Seeder
{
    public function run()
    {
        \App\Models\Dispositivo::create(array(
            'Marca'=>'Motorola',
            'Modelo'=>'XT1032',
            'IMEI'=>'0123456789',
            'Numero'=>'6641234567',
            'iEstatus'=>'1',
            'iSucursal'=>'1'
        ));
    }
}
