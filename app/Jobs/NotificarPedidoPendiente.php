<?php

namespace App\Jobs;

use App\Models\Venta;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

/**
 * Trabajo ejecutado 15 minutos despues de que la venta ha sido creada
 *
 * Class NotificarPedidoPendiente
 * @package App\Jobs
 */
class NotificarPedidoPendiente extends Job implements SelfHandling, ShouldQueue
{
    use SerializesModels;

    private $_venta;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Venta $venta)
    {
        $this->_venta = $venta;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $venta = $this->_venta->fresh();

        // Ignorar si la venta ya no esta pendiente
        if($venta->iEstatus != 1){
            $this->delete();
            return;
        }

        $emails = DB::table('usuarios')
            ->select('Email')
            ->where('iRol', '=', 2)
            ->where('Email', '!=', '')
            ->get();

        $emails = array_map(function ($email) {
            if($email->Email) {
                return $email->Email;
            }
        }, $emails);

        try {
            Mail::send([], [],
                function ($message) use ($emails, $venta) {
                    $message->bcc($emails);
                    $message->subject('Pedido no atendido');
                    $message->setBody('Pedido con folio ' . $venta->Folio . ' sin atender por más de 15 minutos');
                });
        } catch (\Exception $e) {
            Log::warning('No se pudo enviar correo de pendido pendiente: ' . $e->getMessage());
        }
    }
}