<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Exception;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\URL;
use TCPDF;
use Illuminate\Support\Facades\View;
use DateTime;
/**
 * Modelo de Reporte
 */
class Reporte extends Model{

    use SoftDeletes;
	protected $table = 'reportes';
	protected $primaryKey = 'iReporte';
    protected $dates = ['deleted_at'];
	protected $fillable = array('Nombre','Descripcion','Query','Activo');
	protected $visible = array('iReporte','Nombre','Descripcion','Activo','created_at','updated_at',
			'Parametros');

	/**
	 * Relacion Reporte y Reporte_Params
	 */
	public function Parametros()
	{
		return $this->hasMany('App\Models\Reporte_Params', 'iReporte');
	}

    public static function PdfConfiguration($pdf)
    {
        try {
            // set document information
            $pdf->SetCreator('JELP!');
            $pdf->SetAuthor('JELP APP!');
            $pdf->SetTitle('Reportes Jelp!');
            $pdf->setPrintHeader(false);
            $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

            $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
            // set margins
            $pdf->SetMargins(15, PDF_MARGIN_TOP, 15);
            $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
            $pdf->SetFooterMargin(10);
            // set auto page breaks
            $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
            // set image scale factor
            $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
            // set font
            $pdf->SetFont('helvetica', '', 7);
            return $pdf;
        } catch (Exception $e) {
            throw  new Exception('Error en configuracion de pdf', '7026');
        }
    }

    public static function getPdf($results, $inicio, $fin, $ireporte){
        // create new PDF document
        $archivo = new TCPDF();
        $reporte = 'Reporte';
        $pdf = self::PdfConfiguration($archivo);
        setlocale(LC_ALL,"es_ES");
        $inicio = new DateTime($inicio);
        $inicio = $inicio->format('d M Y');
        $fin = new DateTime($fin);
        $fin = $fin->format('d M Y');

        if(count($results)) {
            switch ($ireporte) {
                case 1:
                    foreach ($results as $result) {
                        $pdf->AddPage();
                        $pdf->writeHTML(view('time_view',
                            [
                                'nombre' => ucwords(strtolower($result->Nombre . ' ' . $result->Apellidos)),
                                'noEmpleado' => $result->Colaborador,
                                'sucursal' => $result->Sucursal,
                                'inicio' => $inicio,
                                'fin' => $fin,
                                'pedidos' => ($result->Pedidos) ?: 0,
                                'pedidosCompletados' => ($result->PedidosCompletados) ?: 0,
                                'pedidosPendientes' => ($result->PedidosPendientes) ?: 0,
                                'escaneado' => ($result->Escaneado) ?: '00:00:00',
                                'aceptado' => ($result->Aceptado) ?: '00:00:00',
                                'transito' => ($result->Transito) ?: '00:00:00',
                                'promedioTotal' => ($result->Total) ?: '00:00:00',
                                'eficiencia' => ($result->Eficiencia) ? number_format($result->Eficiencia,2) . '% ' : 'N/A'
                            ])->render(), true, false, false, false, '');
                    }
                    $reporte .= ' tiempos';
                    break;
                case 2:
                    foreach ($results as $result) {
                        $pdf->AddPage();
                        $pdf->writeHTML(view('performances_view',
                            [
                                'nombre' => ucwords(strtolower($result->Nombre . ' ' . $result->Apellidos)),
                                'noEmpleado' => $result->NoEmpleado,
                                'sucursal' => $result->NombreSucursal,
                                'inicio' => $inicio,
                                'fin' => $fin,
                                'tiempoTransito' => ($result->TiempoTransito) ?: '00:00:00',
                                'tiempoSucursal' => ($result->TiempoTienda) ?: '00:00:00',
                                'tiempoLinea' => ($result->Linea) ?: '00:00:00',
                                'participacion' => ($result->Participacion) ? number_format($result->Participacion, 2) . '%' : 'N/A'
                            ]
                        )->render(), true, false, false, false, '');
                    }

                    $reporte .= ' rendimiento';
                    break;
            }
        }else{
            $pdf->AddPage();
            $pdf->writeHTML('<div style="text-align: center;"><h2>No hay datos disponibles</h2></div>');
        }


        //Close and output PDF document

        $pdf->Output($reporte . '_' .$inicio . '_' . $fin. '.pdf' , 'I');
    }

}