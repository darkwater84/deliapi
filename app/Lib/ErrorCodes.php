<?php

namespace App\Lib;

use Exception;

class ErrorCodes {

    const COMMON_ERROR_CODE = 0;
    const OUT_OF_RANGE_ERROR_CODE = 1;
    const FOLIO_ASSIGNED_ERROR_CODE = 2;
    const DELIVERY_IN_PROCESS_ERROR_CODE = 3;
    const NOT_BRANCH_BELONG_ERROR_CODE = 4;

    public static $errors_list = [
        0 => ['Error' => 'Ha ocurrido un error, no tiene permiso para acceder al contenido', 'Code' => 0],
        1 => ["Error" => "Para aceptar un pedido debe estar cerca de la sucursal asignada", "Code" => 1],
        2 => ["Error" => "Ya ha sido asignado", "Code" => 2],
        3 => ["Error" => "Cuenta con repartos en proceso", "Code" => 3],
        4 => ["Error" => "No pertenece a la sucursal", "Code" => 4]
    ];

    public static function getError($code){
        try{
            return self::$errors_list[$code];
        }catch(Exception $e){
            //Error generico
            return self::$errors_list[self::COMMON_ERROR_CODE];
        }
    }
}