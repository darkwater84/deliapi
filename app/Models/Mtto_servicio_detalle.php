<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Mtto_servicio_detalle extends Model{

    protected $table='mtto_servicios_detalle';
    protected $primaryKey = 'iMttoServicioDetalle';
    protected $guarded = array('iMttoServicioDetalle');
    public $timestamps = true;
    protected $fillable = array('iMantenimiento', 'iMttoServicio', 'ServicioAplicado');
    protected $visible = array('iMantenimiento', 'iMttoServicio', 'ServicioAplicado', 'iMttoServicioDetalle', 'Servicio');

    public function servicio(){
        return $this->belongsTo('App\Models\Mtto_servicio', 'iMttoServicio');
    }
}