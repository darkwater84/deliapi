<?php
/**
 * Created by PhpStorm.
 * User: totushi
 * Date: 7/20/15
 * Time: 2:49 PM
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Asignacion extends Model{

    use SoftDeletes;
    protected $table = 'asignaciones';
    protected $primaryKey = 'iAsignacion';
    protected $dates = ['deleted_at'];
    protected $guarded = array('iAsignacion', 'iMotocicleta','iDispositivo','iSucursal','iRepartidor');
    protected $fillable = array('iAsignacion','iMotocicleta','iDispositivo','iSucursal','iRepartidor',);
    protected $visible = array('iAsignacion','iMotocicleta','iDispositivo','iSucursal','iRepartidor','created_at','updated_at',
        'Dispositivo','Sucursal','Repartidor', 'Motocicleta');

    /*Relaciones*/
    public function Dispositivo()
    {
      return $this->hasOne('App\Models\Dispositivo','iDispositivo','iDispositivo');
    }

    public function Sucursal()
    {
      return $this->hasOne('App\Models\Sucursal','iSucursal','iSucursal');
    }

    public function Repartidor()
    {
      return $this->hasOne('App\Models\Repartidor','iRepartidor','iRepartidor');
    }

    public function Motocicleta(){
        return $this->belongsTo('App\Models\Motocicleta', 'iMotocicleta');
    }

}
