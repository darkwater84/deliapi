<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Mtto_servicio;
use Exception;

class Servicio_tallerController extends JelpController{

    public function servicios()
    {
        return JelpController::Respuesta(Mtto_servicio::all());
    }

    public function guardarServicio(Request $request)
    {
        $inputs = $request->all();

        $validations = Validator::make($inputs, [
            'Servicio'  => 'required|unique:mtto_servicios,Servicio',
        ]);

        $validations->setAttributeNames([
            'Servicio' => 'Servicio'
        ]);

        if ($validations->fails()) {
            return JelpController::CustomError($validations->errors()->first());
        }

        Mtto_servicio::create($inputs);

        return JelpController::Respuesta();
    }

    public function actualizarServicio(Request $request, $id)
    {
        $values = $request->all();

        $validations = Validator::make($values, [
            'Servicio'          => 'required|unique:mtto_servicios,Servicio,NULL,id,iMttoServicio,' . $id,
        ]);

        $validations->setAttributeNames([
            'Servicio' => 'Servicio'
        ]);

        if ($validations->fails()) {
            return JelpController::CustomError($validations->errors()->first());
        }

        try {

            $servicio = Mtto_servicio::findOrFail($id);
            $servicio->fill($values);
            $servicio->save();

        } catch (Exception $e) {
            return JelpController::CustomError($e->getMessage());
        }
        return JelpController::Respuesta();
    }

    public function borrarServicio($id){
        $servicio = Mtto_servicio::find($id);
        if(!$servicio)
            return JelpController::CustomError('Servicio Inválido');
        $servicio->delete();
        return JelpController::Respuesta();
    }
}