<?php namespace App\Http\Controllers;

use App\Lib\ErrorCodes;
use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Http\Response;
use Laravel\Lumen\Http\ResponseFactory;

class JelpController extends BaseController
{
	/**
	 * Método para creacion de una respuesta 200.
	 * Usado en respuestas generales exitosas
	 */
    public function Respuesta($data = '',$numeric=false){
		if($numeric == true)
			$respuesta = $this->validateNumerics($data,200);
		else
    		$respuesta = (new Response($data, 200));

    	return $respuesta;
    }

    /**
	 * Método para mensaje de CREADO.
	 * Usado despues de la creacion de un objeto.
	 */
    public function Creado($data){
    	$respuesta = (new Response($data, 201));
    	return $respuesta;
    }

    /**
	 * Método para mensaje de ACEPTADO.
	 * Usado en casos en los cuales se inicia un proceso.
	 */
    public function Aceptado($data){
        $respuesta = (new Response($data, 202));
        return $respuesta;
    }

    /**
	 * Método para mensaje de DEPRECADO.
	 */
    public function Deprecado($data, $newUrl){
    	if($newUrl != null){
    		$respuesta = (new Response(['Data'=>$data, 'NewUrl'=>$newUrl], 299));
    	} else {
    		$respuesta = (new Response($data, 299));
    	}
    	return $respuesta;
    }

    /**
	 * Método para mensaje de No encontrado
	 */
    public function CustomError($message = ''){
    	$respuesta = (new Response(['Error' => $message], 400));
    	return $respuesta;
    }

    /**
	 * Método para mensaje de No encontrado
	 */
    public function NotFound(){
        $respuesta = (new Response('', 404));
        return $respuesta;
    }

    /**
     * Metodo para acceso invalido.
     */
    public function InvalidAccess(){
        $respuesta = (new Response(['Mensaje'=>'Acceso Invalido'], 401));
        return $respuesta;
    }

    /*
     * Metodo para denegar acceso
     * Al enviar un codigo en especifico, contenido en el arreglo de errores
     * se buscara ese mismo error la lista de errores esta en Lib/ErrorCodes.php
     */
    public function Forbidden($code = ErrorCodes::COMMON_ERROR_CODE){
        $content = ErrorCodes::getError($code);
        $respuesta = (new Response($content, 403));
        return $respuesta;
    }

    /**
     * Metodo en contruccion
     */
    public function EnConstruccion(){
    	$respuesta = (new Response('En Construccion', 503));
    	return $respuesta;
    }

	public function validateDate($dateString)
	{
		\DateTime::createFromFormat('Y-m-d', $dateString);
		$date_errors = \DateTime::getLastErrors();

		return !($date_errors['warning_count'] + $date_errors['error_count'] > 0);
	}

	/**
	 * Metodo que valida los numericos para que no se vayan en String.
	 */
	private function validateNumerics($data,$code){
		//return json_encode($data);
		return  Response()->json($data, $code, [], JSON_NUMERIC_CHECK);
    }
}
