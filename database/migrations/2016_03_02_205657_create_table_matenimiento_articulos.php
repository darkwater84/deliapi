<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableMatenimientoArticulos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mantenimiento_articulos', function(Blueprint $table){
            $table->increments('iMantenimientoArticulos');
            $table->integer('iArticuloTaller')->unsigned();
            $table->integer('iMantenimiento')->unsigned();
            $table->string('Articulo');
            $table->integer('Cantidad');
            $table->decimal('Precio');
            $table->decimal('Monto');
            $table->timestamps();

            $table->foreign('iArticuloTaller')->references('iArticuloTaller')->on('articulos_taller');
            $table->foreign('iMantenimiento')->references('iMantenimiento')->on('mantenimientos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('mantenimiento_articulos');
    }
}
