<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Create4dlvlTables extends Migration
{
    const SHORT_TEXT = 25;
    const MID_TEXT = 50;
    const LONG_TEXT = 100;
    const TEXT = 255;
    
    const EMAIL = 255;
    const PHONE = 15;
    const COORDINATES = 27;

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rutas', function (Blueprint $table) {
            $table->increments('iRuta');
            $table->string('Latitud', $this::MID_TEXT);
            $table->string('Longitud', $this::MID_TEXT);
            $table->integer('iRepartidor')->unsigned();
            $table->foreign('iRepartidor')->references('iRepartidor')->on('repartidores')->onDelete('cascade');
            $table->nullableTimestamps();
            $table->softDeletes();
        });
        Schema::create('asignaciones', function (Blueprint $table) {
            $table->increments('iAsignacion');
            $table->integer('iSucursal')->unsigned();
            $table->integer('iRepartidor')->unsigned();
            $table->integer('iMotocicleta')->unsigned();
            $table->integer('iDispositivo')->unsigned();
            $table->foreign('iSucursal')->references('iSucursal')->on('sucursales')->onDelete('cascade');
            $table->foreign('iRepartidor')->references('iRepartidor')->on('repartidores')->onDelete('cascade');
            $table->foreign('iMotocicleta')->references('iMotocicleta')->on('motocicletas')->onDelete('cascade');
            $table->foreign('iDispositivo')->references('iDispositivo')->on('dispositivos')->onDelete('cascade');
            $table->nullableTimestamps();
            $table->softDeletes();
        });
        Schema::create('repartos', function (Blueprint $table) {
            $table->increments('iReparto');
            $table->string('HoraEnterado', $this::MID_TEXT);
            $table->string('HoraSalida', $this::MID_TEXT);
            $table->string('HoraEntrega', $this::MID_TEXT);
            $table->integer('iVenta')->unsigned();
            $table->integer('iRepartidor')->unsigned();
            $table->integer('iComentario')->unsigned();
            $table->integer('iSucursal')->unsigned();
            $table->foreign('iVenta')->references('iVenta')->on('ventas')->onDelete('cascade');
            $table->foreign('iRepartidor')->references('iRepartidor')->on('repartidores')->onDelete('cascade');
            $table->foreign('iComentario')->references('iComentario')->on('comentarios')->onDelete('cascade');
            $table->foreign('iSucursal')->references('iSucursal')->on('sucursales')->onDelete('cascade');
            $table->nullableTimestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('repartos');
        Schema::drop('asignaciones');
        Schema::drop('rutas');
    }
}
