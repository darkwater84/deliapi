<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FixForeignKeysRepartos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(Schema::hasColumn('repartos','iVenta'))
        {
            Schema::table('repartos',function(Blueprint $table){
                $table->dropForeign('repartos_iventa_foreign');
            });

        }

        if(Schema::hasColumn('repartos','iRepartidor'))
        {
            Schema::table('repartos',function(Blueprint $table){
                $table->dropForeign('repartos_irepartidor_foreign');
            });
        }

        if(Schema::hasColumn('repartos','iComentario'))
        {
            Schema::table('repartos',function(Blueprint $table){
                $table->dropForeign('repartos_icomentario_foreign');
            });
        }

        if(Schema::hasColumn('repartos','iSucursal'))
        {
            Schema::table('repartos',function(Blueprint $table){
                $table->dropForeign('repartos_isucursal_foreign');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('repartos',function(Blueprint $table){
            $table->foreign('iVenta')->references('iVenta')->on('ventas');
            $table->foreign('iRepartidor')->references('iRepartidor')->on('repartidores');
            $table->foreign('iComentario')->references('iComentario')->on('comentarios');
            $table->foreign('iSucursal')->references('iSucursal')->on('sucursales');
        });
    }
}
