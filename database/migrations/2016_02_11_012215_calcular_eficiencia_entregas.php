<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\ClienteEntrega;
use App\Models\Cliente;
use Illuminate\Support\Facades\DB;
use App\Lib\GeoCalculator;

class CalcularEficienciaEntregas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('clientes_entregas', function (Blueprint $table) {
            $table->boolean('Eficiente')->default(false);
        });

        Cliente::chunk(200, function($clientes) {
            DB::beginTransaction();

            foreach($clientes as $cliente) {
                $entregas = ClienteEntrega::where('iCliente', '=', $cliente->iCliente)
                    ->orderBy('created_at', 'ASC')
                    ->get();

                $latBase = null;
                $lonBase = null;

                foreach($entregas as $entrega) {
                    if($latBase == null) {
                        $latBase = $entrega->Latitude;
                        $lonBase = $entrega->Longitude;

                        $entrega->Eficiente = true;
                        $entrega->save();
                        continue;
                    }

                    $distance = GeoCalculator::getDistanceBetweenPoints($latBase, $lonBase, $entrega->Latitude, $entrega->Longitude);

                    if($distance <= intval(env('DELIVERY_RANGE'))) {
                        $entrega->Eficiente = true;
                        $entrega->save();
                    }
                }
            }

            DB::commit();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('clientes_entregas', function (Blueprint $table) {
            $table->dropColumn('Eficiente');
        });
    }
}
