<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Bitacora_checklist;

class Bitacora extends Model{
    protected $table='bitacoras';
    protected $primaryKey = 'iBitacora';
    protected $guarded = array('iBitacora');
    public $timestamps = true;
    protected $fillable = array('iUsuarioEntrega', 'NoEmpleadoEntrega' ,'iUsuarioRecibido', 'Evidencia' ,'iMotocicleta', 'Rotulos',
        'Kilometraje','FechaEntrega', 'FechaRecibido','HoraEntrega','HoraRecibido','Observacion');
    protected $visible = array('iBitacora','iUsuarioEntrega', 'NoEmpleadoEntrega','iUsuarioRecibido', 'Evidencia' ,'iMotocicleta', 'Rotulos',
        'Kilometraje','FechaEntrega', 'FechaRecibido','HoraEntrega','HoraRecibido','Observacion','bitacoraChecklist',
        'Motocicleta', 'usuarioEntrega', 'usuarioRecibido');

    public function bitacoraChecklist(){
        return $this->hasMany('App\Models\Bitacora_checklist','iBitacora');
    }

    public function Motocicleta(){
        return $this->belongsTo('App\Models\Motocicleta','iMotocicleta');
    }

    public function usuarioEntrega(){
        return $this->belongsTo('App\Models\Usuario', 'iUsuarioEntrega', 'iUsuario')->withTrashed();
    }

    public function usuarioRecibido(){
        return $this->belongsTo('App\Models\Usuario', 'iUsuarioRecibido', 'iUsuario')->withTrashed();
    }

    public static function storeUsers($values){
        return Bitacora::create([
            'iUsuarioEntrega' => $values['iUsuarioEntrega'],
            'iUsuarioRecibido' => $values['iUsuarioRecibido'],
            'iMotocicleta' => $values['iMotocicleta'],
            'Rotulos' => $values['Rotulos'],
            'Kilometraje' => $values['Kilometraje'],
            'FechaEntrega' => $values['FechaEntrega'],
            'FechaRecibido' => $values['FechaRecibido'],
            'HoraEntrega' => $values['HoraEntrega'],
            'HoraRecibido' => $values['HoraRecibido'],
            'Observaciones' => $values['Observaciones']
        ]);
    }
}