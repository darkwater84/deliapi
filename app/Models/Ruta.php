<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
//tabla de ruta
class Ruta extends Model{

    protected $table = 'rutas';
    public $timestamps = false;
    protected $primaryKey = 'iRuta';
    protected $guarded = array('iRuta','iRepartidor');
    protected $fillable = array('Latitud','Longitud','iRepartidor','created_at');
    protected $visible = array('iRuta','Latitud','Longitud','iRepartidor','created_at',
    	'Repartidor');

    /**
     * Relacion con Roles
     */
    public function Repartidor()
    {
        return $this->belongsTo('App\Models\Repartidor', 'iRepartidor', 'iRepartidor');
    }
}