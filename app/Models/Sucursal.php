<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Sucursal extends Model {
    use SoftDeletes;
    protected $table = 'sucursales';
    protected $primaryKey = 'iSucursal';

    protected $fillable = array('NumeroSucursal','Sucursal','Direccion','Email','Latitud','Longitud','Telefono');
    protected $visible = array('iSucursal','NumeroSucursal','Sucursal','Direccion','Email','Latitud','Longitud','Telefono', 'created_at','updated_at');

    // TODO: (JB) Es necesario madurar el tipo de dato de Coordenada por uno
    // que nos permita realizar consultas geoespaciales. Puede ser en la misma
    // tabla o en una tabla adicional. Requiere que el cambio sea a INNODB.

    /**
     * Relacion con Dispositivo
     */
    public function Dispositivo()
    {
        return $this->hasMany('App\Models\Dispositivo');
    }

}
