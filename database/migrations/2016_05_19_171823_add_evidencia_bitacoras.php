<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEvidenciaBitacoras extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bitacoras',function(Blueprint $table){
            $table->string('Evidencia')->nullable()->after('iUsuarioRecibido');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bitacoras',function(Blueprint $table){
            $table->dropColumn('Evidencia');
        });
    }
}
