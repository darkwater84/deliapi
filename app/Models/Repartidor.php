<?php 

namespace App\Models;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Repartidor extends Model{

    use SoftDeletes;
    protected $table = 'repartidores';
    protected $primaryKey = 'iRepartidor';
    protected $dates = ['deleted_at'];
    protected $guarded = array('iUsuario', 'iMotocicleta','iEstatus');
    protected $fillable = array('iUsuario', 'iMotocicleta','iEstatus');
    protected $visible = array('iRepartidor', 'created_at','updated_at',
        'Usuario','Ruta','Estatus','Motocicleta');

    public function getRepartos(){
        return $this->hasMany('App\Models\Reparto','iRepartidor','iRepartidor');
    }

    public function getEstatus(){
        return $this->belongsTo('App\Models\Estatus_repartidor','iEstatus');
    }

    public function ultimaAsignacion()
    {
        return $this->hasMany('App\Models\Asignacion', 'iRepartidor');
    }

    public function cantidadRepartosPendientes($iRepartidor){
        try {
            $repartidor = Repartidor::find($iRepartidor);
            $repartos = DB::table('vw_repartos')->where('Estatus', "!=", "Terminada")->where('Estatus', '!=', 'Cancelado')->count();
            //dd($repartos);
            if ($repartos > 0) {
                if ($repartidor->getEstatus->Estatus == "Libre") {
                    $estatus = Estatus_repartidor::where('Estatus', 'Ocupado')->first();
                    $repartidor->iEstatus = $estatus->iEstatus;
                }
            } else {
                if ($repartidor->getEstatus->Estatus == "Ocupado") {
                    $estatus = Estatus_repartidor::where('Estatus', 'Libre')->first();
                    $repartidor->iEstatus = $estatus->iEstatus;
                }
            }
            if ($repartidor->save())
                return true;
            else
                return false;
        }catch(\Exception $e)
        {
            throw new \Exception(1004,$e->getMessage());
        }
    }

    /**
     * Relacion de Usuario
     */
    public function Usuario(){
        return $this->belongsTo('App\Models\Usuario','iUsuario','iUsuario');
    }

    /**
     * Relacion de Motocicleta
     */
    public function Motocicleta(){
        return $this->belongsTo('App\Models\Motocicleta','iMotocicleta','iMotocicleta');
    }

    /**
     * Relacion con Estatus
     */
    public function Estatus(){
        return $this->belongsTo('App\Models\Estatus_repartidor','iEstatus','iEstatus');
    } 

    /**
     * Relacion con Rutas
     */
    public function Ruta(){
        return $this->hasMany('App\Models\Ruta','iRuta','iRuta');
    }

    public static function asignacion($iRepartidor){
        $ultimaAsignacion = Asignacion::where('iRepartidor','=',$iRepartidor)
            ->orderBy('created_at','DESC')
            ->with('Sucursal')
            ->first();
        if(!$ultimaAsignacion)
            return NULL;
        else
            return $ultimaAsignacion->Sucursal;
    }
}