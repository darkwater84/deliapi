<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCatalogoClasificacionesArticulos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clasificaciones',function(Blueprint $table){
           $table->increments('iClasificacion');
            $table->string('Clasificacion');
            $table->timestamps();
        });


        Schema::table('mtto_articulos_detalle',function(Blueprint $table){
            $table->dropColumn('Clasificacion');
            $table->unsignedInteger('iClasificacion')->nullable();
            $table->foreign('iClasificacion')->references('iClasificacion')->on('clasificaciones');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mtto_articulos_detalle',function(Blueprint $table){
            $table->dropForeign('mtto_articulos_detalle_iclasificacion_foreign');
            $table->dropColumn('iClasificacion');
        });
        Schema::drop('clasificacion');
    }
}
