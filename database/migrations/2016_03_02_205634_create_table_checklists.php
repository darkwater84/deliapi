<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Checklist;

class CreateTableChecklists extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('checklists', function (Blueprint $table) {
            $table->increments('iChecklist');
            $table->string('Check');
            $table->string('TipoDato');
            $table->integer('Orden');
            $table->integer('iGrupoChecklist')->unsigned();
            $table->timestamps();

            $table->foreign('iGrupoChecklist')->references('iGrupoChecklist')->on('grupos_checklist');
        });

        Checklist::create([
            'Check' => 'Llave',
            'TipoDato' => 'boolean',
            'Orden' => 1,
            'iGrupoChecklist' => 1
        ]);

        Checklist::create([
            'Check' => 'Tarjeta de gasolina y/o sticker',
            'TipoDato' => 'boolean',
            'Orden' => 2,
            'iGrupoChecklist' => 1
        ]);

        Checklist::create([
            'Check' => 'Tarjeta de circulación',
            'TipoDato' => 'boolean',
            'Orden' => 3,
            'iGrupoChecklist' => 1
        ]);

        Checklist::create([
            'Check' => 'Póliza de seguro vigente',
            'TipoDato' => 'boolean',
            'Orden' => 4,
            'iGrupoChecklist' => 1
        ]);

        Checklist::create([
            'Check' => 'Póliza de mantenimiento',
            'TipoDato' => 'boolean',
            'Orden' => 5,
            'iGrupoChecklist' => 1
        ]);

        Checklist::create([
            'Check' => 'Placa posterior',
            'TipoDato' => 'boolean',
            'Orden' => 6,
            'iGrupoChecklist' => 1
        ]);

        Checklist::create([
            'Check' => 'Lic. AR vigente, fecha',
            'TipoDato' => 'boolean',
            'Orden' => 7,
            'iGrupoChecklist' => 1
        ]);

        //Segunda seccion
        Checklist::create([
            'Check' => 'Estuche de herramientas',
            'TipoDato' => 'boolean',
            'Orden' => 1,
            'iGrupoChecklist' => 2
        ]);

        Checklist::create([
            'Check' => 'Llave fija, 10 X 14 mm',
            'TipoDato' => 'boolean',
            'Orden' => 2,
            'iGrupoChecklist' => 2
        ]);

        Checklist::create([
            'Check' => 'Llave fija, 10 X 17 mm',
            'TipoDato' => 'boolean',
            'Orden' => 3,
            'iGrupoChecklist' => 2
        ]);

        Checklist::create([
            'Check' => 'Llave de bujías',
            'TipoDato' => 'boolean',
            'Orden' => 4,
            'iGrupoChecklist' => 2
        ]);

        Checklist::create([
            'Check' => 'Llave tipo perica #8',
            'TipoDato' => 'boolean',
            'Orden' => 5,
            'iGrupoChecklist' => 2
        ]);

        Checklist::create([
            'Check' => 'Desarmador plano',
            'TipoDato' => 'boolean',
            'Orden' => 6,
            'iGrupoChecklist' => 2
        ]);

        Checklist::create([
            'Check' => 'Desarmador cruz',
            'TipoDato' => 'boolean',
            'Orden' => 7,
            'iGrupoChecklist' => 2
        ]);

        Checklist::create([
            'Check' => 'Bomba de aire',
            'TipoDato' => 'boolean',
            'Orden' => 8,
            'iGrupoChecklist' => 2
        ]);

        Checklist::create([
            'Check' => 'Kit de parches',
            'TipoDato' => 'boolean',
            'Orden' => 9,
            'iGrupoChecklist' => 2
        ]);

        Checklist::create([
            'Check' => 'Espatulas (2)',
            'TipoDato' => 'boolean',
            'Orden' => 10,
            'iGrupoChecklist' => 2
        ]);

        //3
        Checklist::create([
            'Check' => 'Asiento sin romper',
            'TipoDato' => 'boolean',
            'Orden' => 1,
            'iGrupoChecklist' => 3
        ]);

        Checklist::create([
            'Check' => 'Asiento sin manchas',
            'TipoDato' => 'boolean',
            'Orden' => 2,
            'iGrupoChecklist' => 3
        ]);

        //4
        Checklist::create([
            'Check' => 'Tipón de gasolina',
            'TipoDato' => 'boolean',
            'Orden' => 1,
            'iGrupoChecklist' => 4
        ]);

        Checklist::create([
            'Check' => 'Casco buen estado',
            'TipoDato' => 'boolean',
            'Orden' => 2,
            'iGrupoChecklist' => 4
        ]);

        Checklist::create([
            'Check' => 'Parrilla en buen estado',
            'TipoDato' => 'boolean',
            'Orden' => 3,
            'iGrupoChecklist' => 4
        ]);

        Checklist::create([
            'Check' => 'Caja de reparto en buen estado',
            'TipoDato' => 'boolean',
            'Orden' => 4,
            'iGrupoChecklist' => 4
        ]);

        Checklist::create([
            'Check' => 'Bisagra en buen estado',
            'TipoDato' => 'boolean',
            'Orden' => 5,
            'iGrupoChecklist' => 4
        ]);

        Checklist::create([
            'Check' => 'Chapa en buen estado',
            'TipoDato' => 'boolean',
            'Orden' => 6,
            'iGrupoChecklist' => 4
        ]);

        Checklist::create([
            'Check' => 'Cámara extra',
            'TipoDato' => 'boolean',
            'Orden' => 7,
            'iGrupoChecklist' => 4
        ]);

        //5
        Checklist::create([
            'Check' => 'Aceite de motor',
            'TipoDato' => 'percentage',
            'Orden' => 1,
            'iGrupoChecklist' => 5
        ]);

        Checklist::create([
            'Check' => 'Gasolina',
            'TipoDato' => 'percentage',
            'Orden' => 1,
            'iGrupoChecklist' => 5
        ]);

        //6
        Checklist::create([
            'Check' => 'Freno delantero',
            'TipoDato' => 'boolean',
            'Orden' => 1,
            'iGrupoChecklist' => 6
        ]);

        Checklist::create([
            'Check' => 'Freno trasero',
            'TipoDato' => 'boolean',
            'Orden' => 2,
            'iGrupoChecklist' => 6
        ]);

        Checklist::create([
            'Check' => 'Cadena lubricada',
            'TipoDato' => 'boolean',
            'Orden' => 3,
            'iGrupoChecklist' => 6
        ]);

        Checklist::create([
            'Check' => 'Cadena ajustada correctamente',
            'TipoDato' => 'boolean',
            'Orden' => 4,
            'iGrupoChecklist' => 6
        ]);

        Checklist::create([
            'Check' => 'Aire correcto en neumático delantero',
            'TipoDato' => 'boolean',
            'Orden' => 5,
            'iGrupoChecklist' => 6
        ]);

        Checklist::create([
            'Check' => 'Aire correcto en neumático trasero',
            'TipoDato' => 'boolean',
            'Orden' => 6,
            'iGrupoChecklist' => 6
        ]);

        Checklist::create([
            'Check' => 'Rayos completos delanteros',
            'TipoDato' => 'boolean',
            'Orden' => 7,
            'iGrupoChecklist' => 6
        ]);

        Checklist::create([
            'Check' => 'Rayos firmes delanteros',
            'TipoDato' => 'boolean',
            'Orden' => 8,
            'iGrupoChecklist' => 6
        ]);

        Checklist::create([
            'Check' => 'Rayos completos traseros',
            'TipoDato' => 'boolean',
            'Orden' => 9,
            'iGrupoChecklist' => 6
        ]);

        Checklist::create([
            'Check' => 'Rayos firmes traseros',
            'TipoDato' => 'boolean',
            'Orden' => 10,
            'iGrupoChecklist' => 6
        ]);

        Checklist::create([
            'Check' => 'Bocina o timbre o claxon',
            'TipoDato' => 'boolean',
            'Orden' => 11,
            'iGrupoChecklist' => 6
        ]);

        Checklist::create([
            'Check' => 'Ajuste del faro',
            'TipoDato' => 'boolean',
            'Orden' => 12,
            'iGrupoChecklist' => 6
        ]);

        Checklist::create([
            'Check' => 'Luz del freno',
            'TipoDato' => 'boolean',
            'Orden' => 13,
            'iGrupoChecklist' => 6
        ]);

        Checklist::create([
            'Check' => 'Luz trasera',
            'TipoDato' => 'boolean',
            'Orden' => 14,
            'iGrupoChecklist' => 6
        ]);

        Checklist::create([
            'Check' => 'Luz del tablero',
            'TipoDato' => 'boolean',
            'Orden' => 15,
            'iGrupoChecklist' => 6
        ]);

        Checklist::create([
            'Check' => 'Señalizador o direccional izquierdo',
            'TipoDato' => 'boolean',
            'Orden' => 16,
            'iGrupoChecklist' => 6
        ]);

        Checklist::create([
            'Check' => 'Señalizador o direccional derecho',
            'TipoDato' => 'boolean',
            'Orden' => 17,
            'iGrupoChecklist' => 6
        ]);

        Checklist::create([
            'Check' => 'Espejo retrovisor izquierdo',
            'TipoDato' => 'boolean',
            'Orden' => 18,
            'iGrupoChecklist' => 6
        ]);

        Checklist::create([
            'Check' => 'Espejo retrovisor derecho',
            'TipoDato' => 'boolean',
            'Orden' => 19,
            'iGrupoChecklist' => 6
        ]);

        //7
        Checklist::create([
            'Check' => 'Sin raspar',
            'TipoDato' => 'boolean',
            'Orden' => 1,
            'iGrupoChecklist' => 7
        ]);

        Checklist::create([
            'Check' => 'Sin rayar',
            'TipoDato' => 'boolean',
            'Orden' => 2,
            'iGrupoChecklist' => 7
        ]);

        Checklist::create([
            'Check' => 'Sin chocar',
            'TipoDato' => 'boolean',
            'Orden' => 3,
            'iGrupoChecklist' => 7
        ]);

        Checklist::create([
            'Check' => 'Limpia',
            'TipoDato' => 'boolean',
            'Orden' => 4,
            'iGrupoChecklist' => 7
        ]);

        //8
        Checklist::create([
            'Check' => 'Incluido',
            'TipoDato' => 'boolean',
            'Orden' => 1,
            'iGrupoChecklist' => 8
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('checklists');
    }
}
