<?php
namespace App\Lib;

use Exception;
use TCPDF;

class PdfFile extends TCPDF
{
    //Page header
    public function Header()
    {
        if($this->page == 1) {
            // Logo
            $image_file = storage_path() . '/../public/images/roma@3x.png';
            $this->Image($image_file, 10, 10, 15, '', 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);
            // Set font
            $this->SetFont('helvetica', 'B', 20);
            // Title
            $this->Cell(0, 15, 'Hoja de Servicio', 0, false, 'C', 0, '', 0, false, 'M', 'M');
        }
    }

    // Page footer
    public function Footer()
    {
        // Position at 15 mm from bottom
        $this->SetY(-15);
        // Set font
        $this->SetFont('helvetica', 'I', 8);
        // Page number
        $this->Cell(0, 10, 'Page ' . $this->getAliasNumPage() . '/' . $this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');

        // Logo
        $base = storage_path() . '/../public/images';
        $logo ='<img src="'.$base.'/logo-jelp.png">';
        $this->writeHTMLCell(100, 30, 100, 275, $logo, 0, 1, false, true, 'R', true);
    }

    public function PdfConfiguration($pdf)
    {
        try {
            // set document information
            $pdf->SetCreator('JELP!');
            $pdf->SetAuthor('JELP APP!');
            $pdf->SetTitle('Reportes Jelp!');
            $pdf->setPrintHeader(true);
            $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

            $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
            // set margins
            $pdf->SetMargins(15, 10, 15);
            $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
            $pdf->SetFooterMargin(PDF_MARGIN_BOTTOM);
            // set auto page breaks
            $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
            // set image scale factor
            $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
            // set font
            $pdf->SetFont('helvetica', '', 7);
            return $pdf;
        } catch (Exception $e) {
            throw  new Exception('Error en configuracion de pdf', '7026');
        }
    }
}