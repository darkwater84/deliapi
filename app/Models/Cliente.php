<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
    protected $table      = 'clientes';
    protected $primaryKey = 'iCliente';
    protected $guarded    = [];
    protected $fillable   = ['Telefono','TotalEntregas','Eficiencia'];
    protected $visible    = ['Telefono','created_at','updated_at'];

    public function Entregas(){
        return $this->hasMany('App\Models\ClienteEntrega', 'iCliente', 'iCliente');
    }

    public function agregarEficiencia($eficiencia)
    {
        if(! $this->iCliente) {
            throw new \RuntimeException('Instancia de Cliente no persistida');
        }

        if(! $this->TotalEntregas) {
            $this->TotalEntregas = 1;
            $this->Eficiencia = 100;
        } else {
            $sum = ($this->Eficiencia * $this->TotalEntregas) / 100;
            $sum = $eficiencia ? $sum + 1 : $sum - 1;

            $this->TotalEntregas += 1;
            $this->Eficiencia = ($sum * 100) / $this->TotalEntregas;
        }

        $this->save();
    }
}
