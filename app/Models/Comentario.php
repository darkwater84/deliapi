<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 7/29/2015
 * Time: 9:38 AM
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Comentario extends Model {

    use SoftDeletes;

    protected $table='comentarios';
    protected $primaryKey = 'iComentario';
    protected $dates = ['deleted_at'];
    protected $guarded = array('iComentario');
    protected $fillable = array('Comentario');
    protected $visible = array('iComentario','Comentario','Total','created_at','updated_at');
} 