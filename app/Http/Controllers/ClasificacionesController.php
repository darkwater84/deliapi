<?php

namespace App\Http\Controllers;

use App\Http\Controllers\JelpController;
use App\Models\Clasificacion;


class ClasificacionesController extends JelpController
{

    public function getClasificaciones(){
        return JelpController::Respuesta(Clasificacion::all(),true);
    }
}