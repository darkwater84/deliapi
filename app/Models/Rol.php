<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Rol extends Model{

    use SoftDeletes;
    protected $table = 'roles';
    protected $primaryKey = 'iRol';
    protected $dates = ['deleted_at'];
    protected $guarded = array('iRol');
    protected $fillable = array('Rol');
    protected $visible = array('iRol','Rol', 'created_at','updated_at');

    const SUPER_USER = 1;
    const MANAGER = 2;
    const SUPERVISOR = 3;
    const ASSISTANT = 4;
    const DELIVERY = 5;
    const MAINTENANCE = 6;
    const MECHANIC = 7;

    /**
     * Relacion con Usuarios
     */
    public function Usuario()
    {
        return $this->hasMany('App\Models\Usuario');
    }

}