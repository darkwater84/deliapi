<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCategoriaMttoArticulos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mtto_articulos',function(Blueprint $table){
            $table->string('Categoria',16)->nullable()->after('Precio');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mtto_articulos',function(Blueprint $table){
            $table->dropColumn('Categoria');
        });
    }
}
