<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableMantenimientosMecanicosYKilometraje extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mantenimientos', function(Blueprint $table){
            $table->dropForeign('mantenimientos_imecanico_foreign');
            $table->integer('Kilometraje')->change();
            $table->foreign('iMecanico')->references('iUsuario')->on('usuarios');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mantenimientos', function(Blueprint $table){
            $table->dropForeign('mantenimientos_imecanico_foreign');
            $table->decimal('Kilometraje')->change();
            $table->foreign('iMecanico')->references('iMecanico')->on('mecanicos');
        });
    }
}
