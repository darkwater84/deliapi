<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\ClienteEntrega;
use Illuminate\Support\Facades\DB;

class CreateTableClientesEntregas extends Migration
{
    public function up()
    {
        if (!Schema::hasTable('clientes_entregas')) {
            Schema::create('clientes_entregas', function (Blueprint $table) {
                $table->increments('iClienteEntrega');

                $table->string('Latitude', 50);
                $table->string('Longitude', 50);

                $table->unsignedInteger('iCliente');

                $table->timestamps();

                $table->foreign('iCliente')->references('iCliente')
                    ->on('clientes')->onDelete('cascade');
            });
        }

        ClienteEntrega::truncate();

        DB::beginTransaction();

        try {
            DB::table('clientes')
                ->select(DB::raw('ventas.created_at, ventas.updated_at, ventas.Longitud, ventas.Latitud, clientes.iCliente'))
                ->join('ventas', 'ventas.Telefono', '=', 'clientes.Telefono')
                ->where('ventas.iEstatus', '=', 4)
                ->chunk(100, function($clientes) {
                    $count = count($clientes);

                    for($i = 0; $i < $count; $i++) {
                        $value = (array) $clientes[$i]; // Express serialization!

                        $value['Latitude']  = $value['Latitud'];
                        $value['Longitude'] = $value['Longitud'];

                        unset($value['Latitud']);
                        unset($value['Longitud']);

                        $clientes[$i] = $value;
                    }

                    ClienteEntrega::insert($clientes);
                });

            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('clientes_entregas');
    }
}
