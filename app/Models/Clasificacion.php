<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Clasificacion extends Model
{
   protected $table = "clasificaciones";
    protected $primaryKey = "iClasificacion";
    protected $fillable = array('Clasificacion');
    protected $visible = array('iClasificacion','Clasificacion');
}