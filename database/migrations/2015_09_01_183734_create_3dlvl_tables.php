<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Create3dlvlTables extends Migration
{
    const SHORT_TEXT = 25;
    const MID_TEXT = 50;
    const LONG_TEXT = 100;
    const TEXT = 255;
    
    const EMAIL = 255;
    const PHONE = 15;
    const COORDINATES = 27;

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('repartidores', function (Blueprint $table) {
            $table->increments('iRepartidor');
            $table->integer('iUsuario')->unsigned();
            $table->integer('iMotocicleta')->unsigned();
            $table->integer('iEstatus')->unsigned();
            $table->foreign('iUsuario')->references('iUsuario')->on('usuarios')->onDelete('cascade');
            $table->foreign('iMotocicleta')->references('iMotocicleta')->on('motocicletas')->onDelete('cascade');
            $table->foreign('iEstatus')->references('iEstatus')->on('estatus_repartidor')->onDelete('cascade');
            $table->nullableTimestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('repartidores');
    }
}
