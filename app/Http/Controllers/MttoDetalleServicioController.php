<?php

namespace app\Http\Controllers;

use App\Models\Mantenimiento;
use App\Models\Mtto_servicio_detalle;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Exception;
use App\Http\Controllers\JelpController;

class MttoDetalleServicioController extends JelpController{

    /**
     * @param Request $request
     * @param $iMantenimiento
     * @param $iMttoServicio
     * @return \Illuminate\Http\Response
     * @description Metodo para activar o desaactivar un servicio realizado en mantenimiento
     */
    public function updateServiceDetail(Request $request, $iMantenimiento, $iMttoServicio){
        $inputs = $request->all();

        if(!is_numeric($iMantenimiento)){
            return JelpController::CustomError('Mantenimiento Inválido');
        }

        if(!is_numeric($iMttoServicio)){
            return JelpController::CustomError('Servicio Inválido');
        }

        $validations = Validator::make($inputs, [
            'ServicioAplicado' => 'required'
        ]);

        $validations->setAttributeNames([
            'ServicioAplicado'  => 'ServicioAplicado'
        ]);

        if($validations->fails()){
            return JelpController::CustomError($validations->errors()->first());
        }

        try{
            $maintenance = Mantenimiento::findOrFail($iMantenimiento);

            Mtto_servicio_detalle::where('iMantenimiento', '=', $maintenance->iMantenimiento)
                ->where('iMttoServicio', '=', $iMttoServicio)
                ->update(['ServicioAplicado' => $inputs['ServicioAplicado']]);

            return JelpController::Respuesta();

        }catch (ModelNotFoundException $e){
            return JelpController::CustomError('Mantenimiento Inválido');
        }catch(Exception $e){
            return JelpController::CustomError($e->getMessage());
        }
    }
}