<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
class LogRepartidor extends Model {

    protected $table = 'log_repartidor';
    protected $primaryKey = 'iLogRepartidor';
    protected $guarded = [];
    protected $fillable = ['iUsuario', 'Tipo_Registro', 'created_at', 'updated_at', 'iDispositivo', 'Invalidado'];
    protected $visible = ['iLogRepartidor', 'iUsuario', 'Tipo_Registro', 'created_at', 'updated_at', 'iDispositivo', 'Invalidado', 'Dispositivo'];

    public function Dispositivo(){
        return $this->belongsTo('App\Models\Dispositivo', 'iDispositivo');
    }
}
