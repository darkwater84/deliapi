<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableBitacoraChecklist extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bitacora_checklist', function(Blueprint $table){
            $table->increments('iBitacoraChecklist');
            $table->integer('iChecklist')->unsigned();
            $table->integer('iBitacora')->unsigned();
            $table->string('Valor')->nullable();
            $table->tinyInteger('Check');
            $table->timestamps();

            $table->foreign('iBitacora')->references('iBitacora')->on('bitacoras');
            $table->foreign('iChecklist')->references('iChecklist')->on('checklists');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('bitacora_checklist');
    }
}
