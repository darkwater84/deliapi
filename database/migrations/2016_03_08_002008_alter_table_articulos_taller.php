<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableArticulosTaller extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::rename('articulos_taller', 'mtto_articulos');

        Schema::table('mantenimiento_articulos',function(Blueprint $table)
        {
            $table->dropForeign('mantenimiento_articulos_iarticulotaller_foreign');
            $table->renameColumn('iArticuloTaller', 'iMttoArticulo');
        });

        Schema::table('mtto_articulos', function(Blueprint $table){
            $table->renameColumn('iArticuloTaller', 'iMttoArticulo');
            $table->softDeletes();
        });

        Schema::table('mantenimiento_articulos',function(Blueprint $table)
        {
            $table->foreign('iMttoArticulo')->references('iMttoArticulo')->on('mtto_articulos');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mantenimiento_articulos',function(Blueprint $table)
        {
            $table->dropForeign('mantenimiento_articulos_imttoarticulo_foreign');
            $table->renameColumn('iMttoArticulo','iArticuloTaller');
        });

        Schema::table('mtto_articulos', function(Blueprint $table){
            $table->renameColumn('iMttoArticulo', 'iArticuloTaller');
            $table->dropSoftDeletes();
        });

        Schema::rename('mtto_articulos', 'articulos_taller');

        Schema::table('mantenimiento_articulos',function(Blueprint $table)
        {
            $table->foreign('iArticuloTaller')->references('iArticuloTaller')->on('articulos_taller');
        });
    }
}
