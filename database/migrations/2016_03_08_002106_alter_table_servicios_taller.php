<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableServiciosTaller extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::rename('servicios_taller', 'mtto_servicios');

        Schema::table('mantenimiento_servicio',function(Blueprint $table)
        {
            $table->dropForeign('mantenimiento_servicio_iserviciotaller_foreign');
            $table->renameColumn('iServicioTaller', 'iMttoServicio');
        });

        Schema::table('mtto_servicios', function(Blueprint $table){
            $table->renameColumn('iServicioTaller', 'iMttoServicio');
            $table->softDeletes();
        });

        Schema::table('mantenimiento_servicio',function(Blueprint $table)
        {
            $table->foreign('iMttoServicio')->references('iMttoServicio')->on('mtto_servicios');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mantenimiento_servicio',function(Blueprint $table)
        {
            $table->dropForeign('mantenimiento_servicio_imttoservicio_foreign');
            $table->renameColumn('iMttoServicio','iServicioTaller');
        });

        Schema::table('mtto_servicios', function(Blueprint $table){
            $table->renameColumn('iMttoServicio', 'iServicioTaller');
            $table->dropSoftDeletes();
        });

        Schema::rename('mtto_servicios', 'servicios_taller');

        Schema::table('mantenimiento_servicio',function(Blueprint $table)
        {
            $table->foreign('iServicioTaller')->references('iServicioTaller')->on('servicios_taller');
        });
    }
}
