<?php

namespace App\Http\Controllers;

use App\Http\Controllers\JelpController;
use App\Models\Reporte;
use App\Models\Reporte_Params;
use Faker\Provider\tr_TR\DateTime;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Exception;
use Validator;
use TCPDF;

/**
 * Controlador de Reportes
 */
class ReportesController extends JelpController {

	const PARAM_TIPO_DATE = 'DATE';
	const PARAM_TIPO_TEXT = 'TEXT';
	const PARAM_TIPO_NUMBER = 'NUMBER';


	/**
	 * GET Obtiene el contenido de un Reporte.
	 * Eventualmente le vamos a mandar mas informacion como 
	 * * Los parametros que necesita un reporte e informacion adicional
	 */
	public function getReporte(Request $request, $iReporte = 0)
	{
		$reporte = Reporte::with('Parametros')
			->where('iReporte','=',$iReporte)
			->get();
		return $this::Respuesta($reporte);
	}

	public function getEjecutarReporte(Request $request, $iReporte = 0)
	{
		$inputs = $request->all();
		//Revisemos que exista el reporte.
		$reporte = Reporte::find($iReporte);
		if(empty($reporte)){
			throw new Exception("Error Processing Request", 1);
		}
		//Revisemos que vengan todos los parametros
		$parametros = Reporte_Params::where('iReporte','=',$iReporte)->get();
		foreach ($parametros as $parametro) {
			if(!array_key_exists($parametro->Nombre, $inputs)){
				throw new Exception("Parametro " . $parametro->Nombre . " no definido", 1);
			}
		}

		//Reemplazamos los parametros
		$query = $reporte->Query;
		foreach ($parametros as $parametro) {
			$query = str_replace($parametro->Nombre, 
						htmlspecialchars($inputs[$parametro->Nombre], ENT_QUOTES, 'utf-8'), 
						$query);
		}

		$dataset = DB::select(DB::raw($query));

        if(isset($inputs['Pdf'])){
            Reporte::getPdf($dataset, $inputs['Inicio'], $inputs['Fin'], $iReporte);
            return true;
        }

		return JelpController::Respuesta($dataset,true);

	}
}