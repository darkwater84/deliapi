<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Articulo_taller extends Model{
    protected $table='articulos_taller';
    protected $primaryKey = 'iArticuloTaller';
    protected $guarded = array('iArticuloTaller');
    public $timestamps = true;
    protected $fillable = array('Concepto', 'Precio');
    protected $visible = array('Concepto', 'Precio', 'iArticuloTaller');
}