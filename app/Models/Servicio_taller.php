<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Servicio_taller extends Model{
    protected $table='servicios_taller';
    protected $primaryKey = 'iServicioTaller';
    protected $guarded = array('iServicioTaller');
    public $timestamps = true;
    protected $fillable = array('Servicio');
    protected $visible = array('Servicio', 'iServicioTaller');
}