<?php

namespace App\Http\Controllers;

use Exception;
use App\Models\Mtto_articulo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class Articulo_tallerController extends JelpController
{
    public function articulos()
    {
        return JelpController::Respuesta(Mtto_articulo::all());
    }

    public function guardarArticulo(Request $request)
    {
        $inputs = $request->all();

        $validations = Validator::make($inputs, [
            'Concepto'  => 'required|unique:mtto_articulos,Concepto',
            'Precio'    => 'required|numeric',
            'Categoria' => 'required'
        ]);

        $validations->setAttributeNames([
            'Concepto' => 'Concepto',
            'Precio' => 'Precio',
            'Categoria' => 'Categoria'
        ]);

        if ($validations->fails()) {
            return JelpController::CustomError($validations->errors()->first());
        }

        Mtto_articulo::create($inputs);

        return JelpController::Respuesta();
    }

    public function actualizarArticulo(Request $request, $id)
    {
        $values = $request->all();
        $validations = Validator::make($values, [
            'Concepto'          => 'required|unique:mtto_articulos,Concepto,NULL,id,iMttoArticulo,' . $id,
            'Precio'            => 'required|numeric'
        ]);

        $validations->setAttributeNames([
            'Concepto' => 'Concepto',
            'Precio' => 'Precio'
        ]);

        if ($validations->fails()) {
            return JelpController::CustomError($validations->errors()->first());
        }

        try {
            $articulo = Mtto_articulo::findOrFail($id);
            $articulo->fill($values);
            $articulo->save();

        } catch (Exception $e) {
            return JelpController::CustomError($e->getMessage());
        }
        return JelpController::Respuesta();
    }

    public function borrarArticulo($id){
        $articulo = Mtto_articulo::find($id);
        if(!$articulo)
            return JelpController::CustomError('Articulo Inválido');
        $articulo->delete();
        return JelpController::Respuesta();
    }
}