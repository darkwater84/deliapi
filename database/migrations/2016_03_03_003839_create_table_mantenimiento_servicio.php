<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableMantenimientoServicio extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mantenimiento_servicio', function(Blueprint $table){
            $table->increments('iMantenimientoServicio');
            $table->integer('iMantenimiento')->unsigned();
            $table->integer('iServicioTaller')->unsigned();
            $table->tinyInteger('ServicioAplicado');
            $table->timestamps();
            $table->foreign('iServicioTaller')->references('iServicioTaller')->on('servicios_taller');
            $table->foreign('iMantenimiento')->references('iMantenimiento')->on('mantenimientos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('mantenimiento_servicio');
    }
}
