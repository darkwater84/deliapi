<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EstatusMotocicleta extends Model{

    use SoftDeletes;
	protected $table = 'estatus_motocicleta';
	protected $primaryKey = 'iEstatus';
    protected $dates = ['deleted_at'];
	protected $fillable = array('Estatus');
	protected $visible = array('iEstatus', 'Estatus','created_at','updated_at');

	/**
     * Relacion con Motocicletas
     */
    public function Motocicletas(){
        return $this->hasMany('App\Models\Motocicletas');
    }
}