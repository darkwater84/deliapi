<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableLogRepartidoresCampoDispositivos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('log_repartidor', function(Blueprint $table){
            $table->unsignedInteger('iDispositivo')->nullable();
            $table->tinyInteger('Invalidado')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('log_repartidor', function(Blueprint $table){
            $table->dropForeign('log_repartidor_idispositivo_foreign');
            $table->dropColumn('iDisipositivo');
            $table->dropColumn('Invalidado');
        });
    }
}