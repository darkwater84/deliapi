<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\ClienteEntrega;
use Illuminate\Support\Facades\DB;

class AsignarRepartidorAEntregas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('clientes_entregas', function (Blueprint $table) {
            $table->unsignedInteger('iRepartidor');
            $table->unsignedInteger('iVenta');
        });

        ClienteEntrega::truncate();

        DB::beginTransaction();

        try {
            DB::table('clientes')
                ->select(DB::raw('ventas.created_at, ventas.updated_at, ventas.Longitud, ventas.Latitud, clientes.iCliente, ventas.iVenta, repartos.iRepartidor'))
                ->join('ventas', 'ventas.Telefono', '=', 'clientes.Telefono')
                ->join('repartos', 'ventas.iVenta', '=', 'repartos.iVenta')
                ->where('ventas.iEstatus', '=', 4)
                ->chunk(200, function($clientes) {
                    $count = count($clientes);

                    for($i = 0; $i < $count; $i++) {
                        $value = (array) $clientes[$i]; // Express serialization!

                        $value['Latitude']  = $value['Latitud'];
                        $value['Longitude'] = $value['Longitud'];

                        unset($value['Latitud']);
                        unset($value['Longitud']);

                        $clientes[$i] = $value;
                    }

                    ClienteEntrega::insert($clientes);
                });

            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('clientes_entregas', function (Blueprint $table) {
            $table->dropColumn('iRepartidor');
            $table->dropColumn('iVenta');
        });
    }
}
