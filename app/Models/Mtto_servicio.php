<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Mtto_servicio extends Model{

    use SoftDeletes;

    protected $table='mtto_servicios';
    protected $primaryKey = 'iMttoServicio';
    protected $guarded = array('iMttoServicio');
    public $timestamps = true;
    protected $fillable = array('Servicio');
    protected $visible = array('Servicio', 'iMttoServicio', 'ServicioAplicado');

    const SERVICE_NOT_APPLIED = 0;

}