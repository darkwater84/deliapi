<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('logs',function(Blueprint $table){
          $table->increments('iLog');
          $table->integer('iUsuario')->unsigned()->nullable();
          $table->string('Mensaje', 255);
          $table->string('Request', 255)->nullable();
          $table->string('Response', 255)->nullable();
          $table->string('Detalle', 255)->nullable();
          $table->string('Origen', 10);
          $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('logs');
    }
}
