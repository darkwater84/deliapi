<?php namespace App\Models;

use App\Lib\GeoCalculator;
use Illuminate\Database\Eloquent\Model;

class ClienteEntrega extends Model
{
    protected $table      = 'clientes_entregas';
    protected $primaryKey = 'iClienteEntrega';
    protected $fillable   = ['Latitude', 'Longitude', 'iCliente'];
    protected $visible    = ['Latitude', 'Longitude', 'iCliente','created_at','updated_at'];

    public function calcularEficiencia()
    {
        if(! $this->iClienteEntrega) {
            throw new \RuntimeException('Instancia no persistida de ClienteEntrega');
        }

        $primerEntrega = ClienteEntrega::where('iCliente', '=', $this->iCliente)
            ->orderBy('created_at', 'ASC')
            ->first();

        $this->Eficiente = false;

        if(! $primerEntrega) {
            $this->Eficiente = true;
        } else {
            $distance = GeoCalculator::getDistanceBetweenPoints($primerEntrega->Latitude, $primerEntrega->Longitude, $this->Latitude, $this->Longitude);

            if($distance <= env('DELIVERY_RANGE')) {
                $this->Eficiente = true;
            }
        }

        $this->save();

        return $this->Eficiente;
    }
}
