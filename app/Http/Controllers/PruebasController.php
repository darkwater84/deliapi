<?php

namespace App\Http\Controllers;

use App\Http\Controllers\JelpController;
use Illuminate\Http\Request;

/**
 * Clase de cometarios
 */
class PruebasController extends JelpController {

    public function getPrueba(){
        return JelpController::Respuesta('Bienvenido');
    }
} 