<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableMantenimientoObservaciones extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mtto_observaciones', function(Blueprint $table){
            $table->increments('iMttoObservacion');
            $table->text('Comentario');
            $table->unsignedInteger('iUsuario');
            $table->timestamps();

            $table->foreign('iUsuario')->references('iUsuario')->on('usuarios');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('mtto_observaciones');
    }
}
