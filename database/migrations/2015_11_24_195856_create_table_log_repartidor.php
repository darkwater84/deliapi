<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableLogRepartidor extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('log_repartidor', function (Blueprint $table) {
            $table->increments('iLogRepartidor');
            $table->integer('iUsuario')->unsigned();
            $table->foreign('iUsuario')->references('iUsuario')->on('usuarios');
            $table->string('Tipo_Registro');
            $table->timestamps();
            $table->index(['iUsuario', 'created_at']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('log_repartidor');
    }
}
