<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateViews extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $vw_repartidores = <<<EOD
CREATE  OR REPLACE VIEW `vw_repartidores` AS
    select 
        u.iUsuario,
        u.Usuario,
        u.Password,
        u.Nombre,
        u.Apellidos,
        u.NoEmpleado,
        u.Email,
        r.iRepartidor,
        r.iEstatus,
        er.Estatus,
        r.iMotocicleta,
        m.Modelo,
        m.Placas,
        m.iEstatus as iEstatusMoto,
        em.Estatus as EstatusMoto
    from
        usuarios u
            join
        repartidores r ON u.iUsuario = r.iUsuario
            join
        motocicletas m ON r.iMotocicleta = m.iMotocicleta
            join
        estatus_repartidor er ON r.iEstatus = er.iEstatus
            join
        estatus_motocicleta em ON m.iEstatus = em.iEstatus;
EOD;
        $vw_usuarios = <<<EOD
CREATE  OR REPLACE VIEW `vw_usuarios` AS
    select 
        u . *, r.Rol
    from
        usuarios u
            join
        roles r ON u.iRol = r.iRol;
EOD;
        $vw_sucursaldispositivo = <<<EOD
CREATE  OR REPLACE VIEW `vw_sucursaldispositivo` AS
    select 
        s.NumeroSucursal, d.IMEI, d.Numero
    from
        sucursales s
            join
        dispositivos d ON s.iSucursal = d.iSucursal;
EOD;
        $vw_asignaciones = <<<EOD
CREATE  OR REPLACE VIEW `vw_asignaciones` AS
    select 
        a.iAsignacion,
        a.created_at as Fecha,
        a.iSucursal,
        s.NumeroSucursal,
        s.Sucursal,
        a.iRepartidor,
        vr.Nombre,
        vr.Apellidos,
        vr.NoEmpleado,
        a.iMotocicleta,
        m.Modelo,
        a.iDispositivo,
        d.Marca,
        d.Modelo as ModeloDispositivo,
        d.IMEI,
        d.Numero
    from
        asignaciones a
            join
        sucursales s ON a.iSucursal = s.iSucursal
            join
        vw_repartidores vr ON a.iRepartidor = vr.iRepartidor
            join
        motocicletas m ON a.iMotocicleta = m.iMotocicleta
            join
        dispositivos d ON a.iDispositivo = d.iDispositivo
    where
        date(a.created_at) = CURDATE()
    order by a.created_at DESC
 
;
EOD;
        $vw_repartos = <<<EOD
CREATE  OR REPLACE VIEW `vw_repartos` AS
    select 
        r.iReparto,
        DATE_FORMAT(r.HoraSalida,'%Y/%m/%d %l:%i%p') as HoraSalida,
        DATE_FORMAT(r.HoraEntrega,'%Y/%m/%d %l:%i%p') as HoraEntrega,
        r.iVenta,
        v.Folio,
        v.Telefono,
        v.Importe,
        v.Latitud,
        v.Longitud,
        v.iEstatus,
        e.Estatus as VentaEstatus,
        r.iRepartidor,
        rr.iUsuario,
        u.Nombre,
        u.Apellidos,
        u.NoEmpleado,
        r.iComentario,
        c.Comentario,
        r.iSucursal,
        s.NumeroSucursal,
        s.Sucursal,
        s.Telefono as TelefonoSucursal
    from
        repartos r
            join
        ventas v ON r.iVenta = v.iVenta
            join
        estatus e ON v.iEstatus = e.iEstatus
            join
        repartidores rr ON r.iRepartidor = rr.iRepartidor
            join
        usuarios u ON rr.iUsuario = u.iUsuario
            join
        comentarios c ON r.iComentario = c.iComentario
            join
        sucursales s ON r.iSucursal = s.iSucursal
    group by iVenta
;
EOD;

        DB::statement($vw_repartidores);
        DB::statement($vw_usuarios);
        DB::statement($vw_sucursaldispositivo);
        DB::statement($vw_asignaciones);
        DB::statement($vw_repartos);
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $vw_repartidores = 'DROP VIEW IF EXISTS vw_repartidores';
        $vw_usuarios = 'DROP VIEW IF EXISTS vw_usuarios';
        $vw_sucursaldispositivo = 'DROP VIEW IF EXISTS vw_sucursaldispositivo';
        $vw_asignaciones = 'DROP VIEW IF EXISTS vw_asignaciones';
        $vw_repartos = 'DROP VIEW IF EXISTS vw_repartos';

        DB::statement($vw_repartidores);
        DB::statement($vw_usuarios);
        DB::statement($vw_sucursaldispositivo);
        DB::statement($vw_asignaciones);
        DB::statement($vw_repartos);
    }
}
