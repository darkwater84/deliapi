<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterForeignRestrict extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ventas', function(Blueprint $table)
        {
            $table->dropForeign('ventas_isucursal_foreign');
            $table->dropForeign('ventas_iestatus_foreign');

            $table->foreign('iSucursal')->references('iSucursal')->on('sucursales')
                ->onDelete('restrict')->onUpdate('restrict');

            $table->foreign('iEstatus')->references('iEstatus')->on('estatus')
                ->onDelete('restrict')->onUpdate('restrict');
        });

        Schema::table('asignaciones', function(Blueprint $table)
        {
            $table->dropForeign('asignaciones_isucursal_foreign');
            $table->dropForeign('asignaciones_imotocicleta_foreign');
            $table->dropForeign('asignaciones_irepartidor_foreign');
            $table->dropForeign('asignaciones_idispositivo_foreign');

            $table->foreign('iSucursal')->references('iSucursal')->on('sucursales')
                ->onDelete('restrict')->onUpdate('restrict');

            $table->foreign('iRepartidor')->references('iRepartidor')->on('repartidores')
                ->onDelete('restrict')->onUpdate('restrict');

            $table->foreign('iMotocicleta')->references('iMotocicleta')->on('motocicletas')
                ->onDelete('restrict')->onUpdate('restrict');

            $table->foreign('iDispositivo')->references('iDispositivo')->on('dispositivos')
                ->onDelete('restrict')->onUpdate('restrict');
        });

        Schema::table('rutas', function(Blueprint $table)
        {
            $table->dropForeign('rutas_irepartidor_foreign');

            $table->foreign('iRepartidor')->references('iRepartidor')->on('repartidores')
                ->onDelete('restrict')->onUpdate('restrict');
        });

        Schema::table('usuarios',function(Blueprint $table)
        {
            $table->dropForeign('usuarios_irol_foreign');

            $table->foreign('iRol')->references('iRol')->on('roles')
                ->onDelete('restrict')->onUpdate('restrict');
        });

        Schema::table('repartidores', function(Blueprint $table)
        {
            $table->dropForeign('repartidores_iestatus_foreign');
            $table->dropForeign('repartidores_imotocicleta_foreign');
            $table->dropForeign('repartidores_iusuario_foreign');

            $table->foreign('iEstatus')->references('iEstatus')->on('estatus_repartidor')
                ->onDelete('restrict')->onUpdate('restrict');

            $table->foreign('iMotocicleta')->references('iMotocicleta')->on('motocicletas')
                ->onDelete('restrict')->onUpdate('restrict');

            $table->foreign('iUsuario')->references('iUsuario')->on('usuarios')
                ->onDelete('restrict')->onUpdate('restrict');


        });

        Schema::table('dispositivos', function(Blueprint $table)
        {
            $table->dropForeign('dispositivos_iestatus_foreign');
            $table->dropForeign('dispositivos_isucursal_foreign');

            $table->foreign('iEstatus')->references('iEstatus')->on('estatus_dispositivo')
                ->onDelete('restrict')->onUpdate('restrict');

            $table->foreign('iSucursal')->references('iSucursal')->on('sucursales')
                ->onDelete('restrict')->onUpdate('restrict');
        });

        Schema::table('motocicletas',function(Blueprint $table)
        {
            $table->dropForeign('motocicletas_iestatus_foreign');

            $table->foreign('iEstatus')->references('iEstatus')->on('estatus_motocicleta')
                ->onDelete('restrict')->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
