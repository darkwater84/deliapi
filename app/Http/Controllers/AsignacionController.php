<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 7/27/2015
 * Time: 11:10 AM
 */

namespace App\Http\Controllers;
use App\Models\Bitacora;
use App\Models\Bitacora_checklist;
use App\Models\Estatus_repartidor;
use App\Models\Log;
use App\Models\Motocicleta;
use App\Models\Usuario;
use Carbon\Carbon;
use Faker\Provider\Base;
use Illuminate\Support\Facades\DB;
use App\Models\Asignacion;
use App\Models\Dispositivo;
use App\Models\Repartidor;
use Illuminate\Http\Request;
use Exception;
use Illuminate\Support\Facades\Validator;

class AsignacionController extends JelpController {

    const ESTATUS_REPARTIDOR_ACTIVO = 1;
    const ESTATUS_REPARTIDOR_INACTIVO = 2;
    const MOVIL = 'MOVIL';
    const CRM = 'CRM';
    const API = 'API';
    const LOGIN = 1;
    const LOGOUT = 2;

    public function registro(Request $request)
    {
        $inputs = $request->all();

        $validator = Validator::make($request->all(),[
            'IMEI' => 'required',
            'iUsuario' => 'required',
            'iSucursal' => 'required'
        ]);

        if($validator->fails())
            return JelpController::CustomError($validator->errors()->first());

        $asignacion = new Asignacion();

        try{
            //$dispostivos = new Dispositivo();
            $dispositivo = Dispositivo::where('IMEI','=',$inputs['IMEI'])
                ->first();
            //con el iUsuario, se busca el iRepartidor y la iMotocicleta, para hacer la asignacion
            $repartidor = Repartidor::where('iUsuario','=',$inputs['iUsuario'])
                ->first();

            if(env('MANTENIMIENTO')==true)
            {
                $date = new \DateTime();
                $bitacora = Bitacora::where('iUsuarioRecibido','=',$inputs['iUsuario'])
                    ->whereDate('created_at','=',$date->format('Y-m-d'))
                    ->first();
                if(!$bitacora)
                    return JelpController::CustomError('No se encontró Inspección');
            }

            if($dispositivo)
            {
                //Generacion de la asignacion
                $asignacion->iSucursal=$inputs['iSucursal'];
                $asignacion->iRepartidor = $repartidor->iRepartidor;
                
                if(env('MANTENIMIENTO')==true)
                    $asignacion->iMotocicleta = $bitacora->iMotocicleta;

                $asignacion->iDispositivo = $dispositivo->iDispositivo;
                $asignacion->save();

                //Actualizacion del dispositivo
                $dispositivo->iEstatus=1;
                if ( isset($inputs['VersionApp']) && isset($inputs['VersionAndroid'])) {
                    $dispositivo->VersionApp = $inputs['VersionApp'];
                    $dispositivo->VersionAndroid = $inputs['VersionAndroid'];
                }
                $dispositivo->save();

                if($repartidor->iEstatus == Estatus_repartidor::IN_MAINTENANCE){
                    $mantenimiento = true;
                }else{
                    $repartidor
                        ->update([
                            'iEstatus' => $this::ESTATUS_REPARTIDOR_ACTIVO,

                        ]);
                    $mantenimiento = false;
                }

                Usuario::validateLogin($repartidor, $dispositivo);

                Log::create(array('iUsuario'=>$inputs['iUsuario'],
                    'Mensaje'=>'Usuario Asignado: ' . $inputs['iUsuario']. ' a dispositivo: ' . $inputs['IMEI'],
                    'Request'=>json_encode($request->all()),
                    'Origen'=>$this::API));
                
                return JelpController::Respuesta(['Mantenimiento' => $mantenimiento]);
            }
            else
            {
                return JelpController::CustomError('Dispositivo No Reconocido');
            }
        }
        catch(Exception $e)
        {
            return JelpController::CustomError($e->getMessage());
        }
    }

}
