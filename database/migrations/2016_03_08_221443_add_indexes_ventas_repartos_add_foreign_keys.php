<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIndexesVentasRepartosAddForeignKeys extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('repartos', function(Blueprint $table)
        {
            $table->foreign('iVenta')->references('iVenta')->on('ventas');
            $table->foreign('iComentario')->references('iComentario')->on('comentarios');
            $table->foreign('iRepartidor')->references('iRepartidor')->on('repartidores');
            $table->foreign('iSucursal')->references('iSucursal')->on('sucursales');
        });

        Schema::table('ventas',function(Blueprint $table){
            $table->index(['created_at', 'iEstatus'], 'index_created_at_iestatus');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('repartos', function(Blueprint $table)
        {
            $table->dropForeign('repartos_iventa_foreign');
            $table->dropForeign('repartos_icomentario_foreign');
            $table->dropForeign('repartos_irepartidor_foreign');
            $table->dropForeign('repartos_isucursal_foreign');
        });

        Schema::table('ventas',function(Blueprint $table){
            $table->dropIndex('index_created_at_iestatus');
        });
    }
}
