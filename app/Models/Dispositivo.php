<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Dispositivo extends Model{

    use SoftDeletes;

    protected $table = 'dispositivos';
    protected $primaryKey = 'iDispositivo';
    protected $dates = ['deleted_at'];
    protected $fillable = array('Marca', 'Modelo', 'IMEI','Numero',
    	'iEstatus',
        'iSucursal', 'VersionApp', 'VersionAndroid');
    protected $visible = array('iDispositivo', 'Marca', 'Modelo', 'IMEI','Numero','created_at','updated_at',
    	'Estatus',
    	'Sucursal', 'VersionApp', 'VersionAndroid');

	/**
	 * RELACIONES
	 */

    /**
     * Relacion con EstatusDispositivo
     */
    public function Estatus(){
        return $this->belongsTo('App\Models\Estatus_dispositivo','iEstatus','iEstatus');
    }

    /**
     * Relacion con Sucursales
     */
    public function Sucursal(){
        return $this->belongsTo('App\Models\Sucursal','iSucursal','iSucursal');
    }

}
