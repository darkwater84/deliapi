<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableMantenimiento extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mantenimientos', function(Blueprint $table){
            $table->increments('iMantenimiento');
            $table->integer('iMecanico')->unsigned();
            //usuario
            $table->integer('iUsuario')->unsigned();
            $table->integer('iSucursal')->unsigned();
            //motocicleta
            $table->integer('iMotocicleta')->unsigned();
            $table->decimal('Kilometraje', 10,2)->nullable();
            //fechas
            $table->date('FechaEntrada');
            $table->date('FechaSalida')->nullable();
            $table->time('HoraEntrada');
            $table->time('HoraSalida')->nullable();
            //Monto
            $table->decimal('Total')->nullable();
            //Descripciones del servicio
            $table->text('Servicio')->nullable();
            $table->text('Observacion')->nullable();

            $table->timestamps();
            $table->foreign('iSucursal')->references('iSucursal')->on('sucursales');
            $table->foreign('iMecanico')->references('iMecanico')->on('mecanicos');
            $table->foreign('iUsuario')->references('iUsuario')->on('usuarios');
            $table->foreign('iMotocicleta')->references('iMotocicleta')->on('motocicletas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('mantenimientos');
    }
}
