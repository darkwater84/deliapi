<?php
/**
 * Created by PhpStorm.
 * User: totushi
 * Date: 7/20/15
 * Time: 3:00 PM
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Exception;
use DB;
use Illuminate\Database\Eloquent\SoftDeletes;

class Venta extends Model{

    use SoftDeletes;
    protected $table = 'ventas';
    protected $primaryKey = 'iVenta';
    protected $dates = ['deleted_at'];
    protected $guarded = array('iVenta','iEstatus','iSucursal');
    protected $fillable = array('Folio','Telefono','Importe','Firma','Latitud','Longitud',);
    protected $visible = array('iVenta','Folio','Telefono','Importe','Firma','Latitud',
        'Longitud','iComentario','totales','created_at','updated_at', 'NumeroSucursal',
        'Total', 'Sucursal', 'TotalVentas', 'Segundos', 'Rango', 'Reparto', 'iSucursal');


    public function getEstatus()
    {
        return $this->belongsTo('App\Models\Estatus','iEstatus');
    }

    public function Reparto(){
        return $this->hasOne('App\Models\Reparto', 'iVenta');
    }

    public function completarVenta($iVenta,$firma)
    {
        $venta=Venta::find($iVenta);
        $venta->iEstatus=1;
        $venta->Firma=$firma['firma'];
        if($venta->save())
            return true;
        else
            throw new Exception('No se ha podido actualizar la venta',1002);
    }

    public function cancelarVenta($iVenta)
    {
        try {
            $venta = Venta::find($iVenta);

            $estatus=Estatus::where('Estatus','=','Cancelada')->first();

            $venta->iEstatus = $estatus->iEstatus;
            if ($venta->save())
                return true;
            else
                throw new Exception('No se ha podido cancelar la venta', 1008);
        }catch (Exception $e)
        {
            throw new Exception('No se ha podido cancelar la venta', 1007);
        }
    }

    public function ventaEnProceso($iReparto)
    {
        $estatus=Reparto::find($iReparto)->getVenta->getEstatus;
        if($estatus->Estatus='En Proceso')
            return true;
        else
            return false;
    }

    public static function sumaTotalImporte($fechaInicio, $fechaFin)
    {
      return DB::table('ventas')
               ->select('Importe')
               ->whereRaw("date(created_at) between '$fechaInicio' and '$fechaFin'")
               ->sum('Importe');
    }

}
