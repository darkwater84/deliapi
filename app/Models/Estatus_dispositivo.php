<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Estatus_dispositivo extends Model{

    use SoftDeletes;
    protected $table = 'estatus_dispositivo';
    protected $primaryKey = 'iEstatus';
    protected $dates = ['deleted_at'];
    protected $fillable = array('Estatus');
    protected $visible = array('iEstatus','Estatus','created_at','updated_at');

    /**
     * Relacion con Dispositivo
     */
    public function Dispositivo()
    {
        return $this->hasMany('App\Models\Dispositivo');
    }

}