<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInitTables extends Migration
{
    const SHORT_TEXT = 25;
    const MID_TEXT = 50;
    const LONG_TEXT = 100;
    const TEXT = 255;
    
    const EMAIL = 255;
    const PHONE = 15;
    const COORDINATES = 27;

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('roles', function (Blueprint $table) {
            $table->increments('iRol');
            $table->string('Rol', $this::SHORT_TEXT);
            $table->nullableTimestamps();
            $table->softDeletes();
        });

        Schema::create('estatus_repartidor', function (Blueprint $table) {
            $table->increments('iEstatus');
            $table->string('Estatus', $this::SHORT_TEXT);
            $table->nullableTimestamps();
            $table->softDeletes();
        });

        Schema::create('estatus_motocicleta', function (Blueprint $table) {
            $table->increments('iEstatus');
            $table->string('Estatus', $this::SHORT_TEXT);
            $table->nullableTimestamps();
            $table->softDeletes();
        });

        Schema::create('estatus_dispositivo', function (Blueprint $table) {
            $table->increments('iEstatus');
            $table->string('Estatus', $this::SHORT_TEXT);
            $table->nullableTimestamps();
            $table->softDeletes();
        });

        Schema::create('sucursales', function (Blueprint $table) {
            $table->increments('iSucursal');
            $table->string('NumeroSucursal', $this::SHORT_TEXT);
            $table->string('Sucursal', $this::LONG_TEXT);
            $table->string('Direccion', $this::TEXT);
            $table->string('Email', $this::EMAIL);
            $table->string('Coordenadas', $this::COORDINATES); //Pasar a POINT
            $table->string('Telefono', $this::PHONE);
            $table->nullableTimestamps();
            $table->softDeletes();
        });

        Schema::create('estatus', function (Blueprint $table) {
            $table->increments('iEstatus');
            $table->string('Estatus', $this::SHORT_TEXT);
            $table->nullableTimestamps();
            $table->softDeletes();
        });

        Schema::create('comentarios', function (Blueprint $table) {
            $table->increments('iComentario');
            $table->string('Comentario', $this::TEXT);
            $table->nullableTimestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('roles');
        Schema::drop('estatus_repartidor');
        Schema::drop('estatus_motocicleta');
        Schema::drop('estatus_dispositivo');
        Schema::drop('sucursales');
        Schema::drop('estatus');
        Schema::drop('comentarios');
    }
}
