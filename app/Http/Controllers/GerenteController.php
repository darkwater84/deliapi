<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 7/24/2015
 * Time: 3:52 PM
 */

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use App\Models\Gerentes;
use App\Models\Asignacion;
use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller as BaseController;

class GerenteController extends BaseController {

    public function getAll()
    {
        $gerentes = new Gerentes();
        echo json_encode(['Gerentes'=>$gerentes->all(),'Mensaje'=>'200']);
    }

    public function getContacto(Request $request)
    {
        $input = $request->all();
        $repartidor = DB::table('vw_asignaciones')->where('iRepartidor','=',$input['iRepartidor'])->get();

        //$contacto = Gerentes::where('iSucursal','=',$inputs)->get();
        $contacto = DB::table('gerentes')->where('iSucursal','=',$repartidor[0]->iSucursal)->get();
        //dd($contacto);
        echo json_encode(['Gerente'=>$contacto[0]->Telefono,'Mensaje'=>'200']);
    }

    public function test(Request $request)
    {
        $input = $request->all();
        echo json_encode($input);
    }
} 