<?php

namespace app\Http\Controllers;

use App\Models\Rol;
use Laravel\Lumen\Routing\Controller as BaseController;
use Faker\Provider\Base;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Controllers\JelpController;

class RolesController extends JelpController{

    /**
     * Metodo para regresar todos los Usuarios.
     */
    public function getAll(){
        $usuarios=Rol::all();
        return JelpController::Respuesta(['Roles'=>$usuarios], null);
    }

    public function getRole($id){
    	$rol = Rol::find($id);
    	return JelpController::Respuesta($rol, null);
    }
}