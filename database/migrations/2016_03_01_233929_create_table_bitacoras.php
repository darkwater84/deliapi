<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableBitacoras extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //Bitacoras diarias de revision de motos
        Schema::create('bitacoras', function(Blueprint $table){
            $table->increments('iBitacora');
            //Usuarios
            $table->integer('iUsuarioEntrega')->unsigned();
            $table->integer('iUsuarioRecibido')->unsigned()->nullable();
            //Motocicleta
            $table->integer('iMotocicleta')->unsigned();
            $table->tinyInteger('Rotulos');
            $table->decimal('Kilometraje', 10,2)->nullable();
            //Fechas
            $table->date('FechaEntrega');
            $table->date('FechaRecibido')->nullable();
            $table->time('HoraEntrega');
            $table->time('HoraRecibido')->nullable();
            //Observaciones
            $table->text('Observacion')->nullable();
            $table->timestamps();

            //Llaves foraneas
            $table->foreign('iUsuarioEntrega')->references('iUsuario')->on('usuarios');
            $table->foreign('iUsuarioRecibido')->references('iUsuario')->on('usuarios');
            $table->foreign('iMotocicleta')->references('iMotocicleta')->on('motocicletas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('bitacoras');
    }
}
