<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTimestampsStatusMantenimientos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mantenimientos',function(Blueprint $table){
            $table->timestamp('Pendiente')->nullable();
            $table->timestamp('Aceptado')->nullable();
            $table->timestamp('Iniciado')->nullable();
            $table->timestamp('Cerrado')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mantenimientos',function(Blueprint $table){
            $table->dropColumn('Pendiente');
            $table->dropColumn('Aceptado');
            $table->dropColumn('Iniciado');
            $table->dropColumn('Cerrado');
        });
    }
}
