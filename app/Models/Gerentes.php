<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 7/24/2015
 * Time: 3:50 PM
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
class Gerentes extends Model {

    protected $table = 'gerentes';
    protected $primaryKey = 'iGerente';
    protected $guarded = array('iGerente', 'iUsuario','iSucursal');
    protected $fillable = array('iGerente','Telefono','iUsuario','iSucursal');
    protected $visible = array('iGerente','Telefono','iUsuario','iSucursal','created_at','updated_at');
} 