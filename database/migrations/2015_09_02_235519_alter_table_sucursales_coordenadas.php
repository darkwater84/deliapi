<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableSucursalesCoordenadas extends Migration
{
    const SHORT_TEXT = 25;
    const MID_TEXT = 50;
    const LONG_TEXT = 100;
    const TEXT = 255;
    
    const EMAIL = 255;
    const PHONE = 15;
    const COORDINATES = 27;

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sucursales', function ($table) {
            $table->decimal('Latitud',17,14);
            $table->decimal('Longitud',17,14);
        });
        Schema::table('sucursales', function ($table) {
            $table->dropColumn('Coordenadas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sucursales', function ($table) {
            $table->text('Coordenadas', COORDINATES);
        });
        Schema::table('sucursales', function ($table) {
            $table->dropColumn('Latitud');
            $table->dropColumn('Longitud');
        });
    }
}
