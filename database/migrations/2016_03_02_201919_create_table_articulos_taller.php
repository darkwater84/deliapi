<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Articulo_taller;

class CreateTableArticulosTaller extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('articulos_taller', function (Blueprint $table) {
            $table->increments('iArticuloTaller');
            $table->string('Concepto', 120)->unique();
            $table->decimal('Precio');
            $table->timestamps();
        });

        Articulo_taller::create([
            'Concepto' => 'ACEITE LUBRICANTE DE CADENA ECOMOTO 240 ML', 'Precio' => 80.00
        ]);
        Articulo_taller::create([
            'Concepto' => 'ACEITE LUBRICANTE DE CADENA MAC KAY', 'Precio' => 120.00
        ]);
        Articulo_taller::create([
            'Concepto' => 'ACEITE QUAKER RACING OIL 20W-50', 'Precio' => 85.00
        ]);
        Articulo_taller::create([
            'Concepto' => 'BALATA TAMBOR 0.25 P/HONDA CG125 TITAN KS', 'Precio' => 100.00
        ]);
        Articulo_taller::create([
            'Concepto' => 'BALATAS DELANTERAS /TRASERAS CARGO GL150 JPN', 'Precio' => 300.00
        ]);
        Articulo_taller::create([
            'Concepto' => 'BALATAS DELANTERAS ITALIKA FORZA 150 WSTD', 'Precio' => 80.00
        ]);
        Articulo_taller::create([
            'Concepto' => 'BALATAS DELANTYERAS/TRASERAS YAMAHA YBR 125', 'Precio' => 90.00
        ]);
        Articulo_taller::create([
            'Concepto' => 'BALATAS TRASERAS DELANTERAS CARGO GL150 (WSTD)', 'Precio' => 100.00
        ]);
        Articulo_taller::create([
            'Concepto' => 'BALATAS TRASERAS ITALIKA RT200 DINAMO', 'Precio' => 90.00
        ]);
        Articulo_taller::create([
            'Concepto' => 'BALATAS TRASERAS YAMAHA YBR125 (WSTD)', 'Precio' => 100.00
        ]);
        Articulo_taller::create([
            'Concepto' => 'BALERO 6004-ZZ NACHI JAPON', 'Precio' => 120.00
        ]);
        Articulo_taller::create([
            'Concepto' => 'BALERO 6005-ZZ NACHI JAPON', 'Precio' => 150.00
        ]);
        Articulo_taller::create([
            'Concepto' => 'BALERO 6203-ZZ NACHI JAPON', 'Precio' => 150.00
        ]);
        Articulo_taller::create([
            'Concepto' => 'BALERO 6204-ZZ NACHI JAPON', 'Precio' => 180.00
        ]);
        Articulo_taller::create([
            'Concepto' => 'BALERO RIFFEL 6301-2RS', 'Precio' => 120.00
        ]);
        Articulo_taller::create([
            'Concepto' => 'BIELA COMPLETA YAMAHA YB125/YBR125/YBR125 C EXP (WSTD)', 'Precio' => 450.00
        ]);
        Articulo_taller::create([
            'Concepto' => 'BOBINA ENCENDIDO EXTERNA HONDA GL150 CARGO (WSTD)', 'Precio' => 150.00
        ]);
        Articulo_taller::create([
            'Concepto' => 'BUJIA NGK C7HSA', 'Precio' => 50.00
        ]);
        Articulo_taller::create([
            'Concepto' => 'BUJIA NGK C8E', 'Precio' => 50.00
        ]);
        Articulo_taller::create([
            'Concepto' => 'BUJIA NGK CPR8', 'Precio' => 120.00
        ]);
        Articulo_taller::create([
            'Concepto' => 'BUJIA NGK CR8', 'Precio' => 100.00
        ]);
        Articulo_taller::create([
            'Concepto' => 'CABLE ACELERADOR HONDA GL150 CARGO NAC', 'Precio' => 90.00
        ]);
        Articulo_taller::create([
            'Concepto' => 'CABLE EMBRAGUE HONDA GL 150 CARGO NAC', 'Precio' => 90.00
        ]);
        Articulo_taller::create([
            'Concepto' => 'CABLE EMBRAGUE HONDA GL 150 CARGO WSTD', 'Precio' => 80.00
        ]);
        Articulo_taller::create([
            'Concepto' => 'CABLE EMBRAGUE PROMOTO P/HONDA CGL125 TOOL', 'Precio' => 70.00
        ]);
        Articulo_taller::create([
            'Concepto' => 'CADENA 428H X 116 CHOHO', 'Precio' => 150.00
        ]);
        Articulo_taller::create([
            'Concepto' => 'CADENA 428H X 126 CHOHO', 'Precio' => 160.00
        ]);
        Articulo_taller::create([
            'Concepto' => 'CADENA PROMOTO 428 -126 CONVENCIONAL', 'Precio' => 150.00
        ]);
        Articulo_taller::create([
            'Concepto' => 'CADENA RIFFEL 428H -126 REFORZADA', 'Precio' => 160.00
        ]);
        Articulo_taller::create([
            'Concepto' => 'CADENA RIFFEL 428H -136L REFORZADA', 'Precio' => 160.00
        ]);
        Articulo_taller::create([
            'Concepto' => 'CADENA TIEMPO HONDA CARGO GL150 WSTD', 'Precio' => 120.00
        ]);
        Articulo_taller::create([
            'Concepto' => 'CAMARA MOTO CHAO YANG', 'Precio' => 100.00
        ]);
        Articulo_taller::create([
            'Concepto' => 'CAMARA PROMOTO 2.75/3.00 -18', 'Precio' => 80.00]);
        Articulo_taller::create([
            'Concepto' => 'CAMPANA EMBRAGUE HONDA GL 150', 'Precio' => 600.00]);
        Articulo_taller::create([
            'Concepto' => 'CARBURADOR HONDA GL150 CARGO', 'Precio' => 1, 400.00]);
        Articulo_taller::create([
            'Concepto' => 'CASCO RODA DELIVERY ROJO  XG ABATIBLE', 'Precio' => 1, 100.00]);
        Articulo_taller::create([
            'Concepto' => 'CASCO RODA DELIVERY ROJO G ABATIBLE', 'Precio' => 1, 100.00]);
        Articulo_taller::create([
            'Concepto' => 'CASCO RODA R1020 CERRADO SVS ROJO EG', 'Precio' => 950.00]);
        Articulo_taller::create([
            'Concepto' => 'CASCO RODA R1020 CERRADO SVS ROJO G', 'Precio' => 950.00]);
        Articulo_taller::create([
            'Concepto' => 'CILINDRO CON PISTON HONDA TITAN 150/GL150 CARGO', 'Precio' => 800.00]);
        Articulo_taller::create([
            'Concepto' => 'CILINDRO CON PISTON HONDA TITAN 150/GL150 CARGO WSTD', 'Precio' => 900.00]);
        Articulo_taller::create([
            'Concepto' => 'CLAXON HONDA GL150 CARGO/125 ITALIKA FORZA (WSTD)', 'Precio' => 120.00]);
        Articulo_taller::create([
            'Concepto' => 'CLAXON RIFFEL 12V', 'Precio' => 80.00]);
        Articulo_taller::create([
            'Concepto' => 'COMMUTADOR DERECHO GL150 CARGO WSTD', 'Precio' => 150.00]);
        Articulo_taller::create([
            'Concepto' => 'COMMUTADOR IZQUIERDO GL150 CARGO WSTD', 'Precio' => 180.00]);
        Articulo_taller::create([
            'Concepto' => 'CONTROL ELECTRICO DER PROMOTO P/HONDA CGL125 TOOL', 'Precio' => 100.00]);
        Articulo_taller::create([
            'Concepto' => 'CONTROL ELECTRICO IZQ PROMOTO P/HONDA CGL125 TOOL', 'Precio' => 100.00]);
        Articulo_taller::create([
            'Concepto' => 'CORBATA RIM 17', 'Precio' => 20.00]);
        Articulo_taller::create([
            'Concepto' => 'CORBATA RIM 18', 'Precio' => 25.00]);
        Articulo_taller::create([
            'Concepto' => 'DIRECCIONAL DERECHA/IZQUIERDA HONDA GL150 CARGO  HUMO', 'Precio' => 160.00]);
        Articulo_taller::create([
            'Concepto' => 'DIRECCIONAL VARIOS LADOS PROMOTO P/HONDA CGL125 TOOL', 'Precio' => 70.00]);
        Articulo_taller::create([
            'Concepto' => 'DISCO DE EMBRAGUE PASTA HONDA GL150', 'Precio' => 220.00]);
        Articulo_taller::create([
            'Concepto' => 'DISCO DE EMBRAGUE PASTA YAMAHA YBR 125', 'Precio' => 160.00]);
        Articulo_taller::create([
            'Concepto' => 'DISCO EMBRAGUE PASTA 5 HONDA GL 150', 'Precio' => 150.00]);
        Articulo_taller::create([
            'Concepto' => 'DISCO EMBRAGUE PASTA P/YAMAHA YBR125 K (JGO 5 PZAS)', 'Precio' => 180.00]);
        Articulo_taller::create([
            'Concepto' => 'EJE RUEDA DELANTERA ORIGINAL GL 150', 'Precio' => 90.00]);
        Articulo_taller::create([
            'Concepto' => 'EJE RUEDA TRASERA CON TUERCA P/YAMAHA YBR125 ED', 'Precio' => 150.00]);
        Articulo_taller::create([
            'Concepto' => 'EJE RUEDA TRASERA GL150', 'Precio' => 110.00]);
        Articulo_taller::create([
            'Concepto' => 'EJE RUEDA TRASERA HONDA CARGO/TITAN 125 MA CT125 Y/ YBR125 C', 'Precio' => 80.00]);
        Articulo_taller::create([
            'Concepto' => 'ENGRANE DE VELOCIMETRO  CONTADOR GL 150', 'Precio' => 100.00]);
        Articulo_taller::create([
            'Concepto' => 'ENGRANE DELAN RIFFEL 15D P/HONDA CG125', 'Precio' => 50.00]);
        Articulo_taller::create([
            'Concepto' => 'ENGRANE TRASERO RIFFEL 43D P/HONDA CG125 CDI', 'Precio' => 120.00]);
        Articulo_taller::create([
            'Concepto' => 'ESPATULA DESMONTAR LLANTAS 10 PULG TWN', 'Precio' => 150.00]);
        Articulo_taller::create([
            'Concepto' => 'ESPATULA DESMONTAR LLANTAS 14 PULG C/MANGO PLASTICO TWN', 'Precio' => 160.00]);
        Articulo_taller::create([
            'Concepto' => 'ESPEJO DERECHO GL150 ORIGINAL', 'Precio' => 130.00]);
        Articulo_taller::create([
            'Concepto' => 'DERECHO/IZQUIERDO  YAMAHA YBR 125', 'Precio' => 160.00]);
        Articulo_taller::create([
            'Concepto' => 'ESPEJO DERECHO/IZQUIERDO HONDA GL150 CARGO', 'Precio' => 80.00]);
        Articulo_taller::create([
            'Concepto' => 'ESPEJO IZQUIERDO  GL 150 ORIGINAL', 'Precio' => 130.00]);
        Articulo_taller::create([
            'Concepto' => 'ESTATOR GENERADOR CARGO GL 150', 'Precio' => 800.00]);
        Articulo_taller::create([

            'Concepto' => 'FARO DELANTERO HONDA GL150 CARGO LED', 'Precio' => 490.00]);
        Articulo_taller::create([
            'Concepto' => 'FARO DELANTERO SUZUKI EN125 ,HONDA GL150 WSTD', 'Precio' => 400.00]);
        Articulo_taller::create([
            'Concepto' => 'FARO DELANTERO YAMAHA YBR 125 C EXP (WSTD)', 'Precio' => 350.00]);
        Articulo_taller::create([
            'Concepto' => 'FILTRO AIRE HONDA GL150 CARGO (ELEMENTO)', 'Precio' => 90.00]);
        Articulo_taller::create([
            'Concepto' => 'FILTRO DE AIRE YAMAHA YBR 125 C EXP WSTD', 'Precio' => 120.00]);
        Articulo_taller::create([
            'Concepto' => 'FLOTADOR TANQUE GASOLINA PROMOTO P/HONDA GL150 CARGO', 'Precio' => 90.00]);
        Articulo_taller::create([
            'Concepto' => 'FOCO FARO DELANTERO HALOGENO 12V  35 W', 'Precio' => 60.00]);
        Articulo_taller::create([
            'Concepto' => 'GUANTES PROBIKER C/ PROTECTOR ANTIDERRAPANTE NEGRO L', 'Precio' => 300.00]);
        Articulo_taller::create([
            'Concepto' => 'GUANTES PROBIKER C/ PROTECTOR ANTIDERRAPANTE NEGRO XL', 'Precio' => 300.00]);
        Articulo_taller::create([
            'Concepto' => 'GUANTES PROBIKER C/PROTECTOR  NEGRO XXL', 'Precio' => 300.00]);
        Articulo_taller::create([
            'Concepto' => 'INTERRUPTOR ENCENDIDO CON LLAVE HONDA CGL TOOL', 'Precio' => 150.00]);
        Articulo_taller::create([
            'Concepto' => 'INTERRUPTOR ENCENDIDO CON LLAVE HONDA TITAN 150', 'Precio' => 180.00]);
        Articulo_taller::create([
            'Concepto' => 'JGO DE GOMAS DE TRACCION GL 150', 'Precio' => 80.00]);
        Articulo_taller::create([
            'Concepto' => 'JUEGO BALERO DIRECCION P/YAMAHA YBR125 ED', 'Precio' => 250.00]);
        Articulo_taller::create([
            'Concepto' => 'JUEGO ENGRANES RIFFEL 42DX15D CON CADENA 428HX118L PREMIUM P/HONDA CG150', 'Precio' => 350.00]);
        Articulo_taller::create([
            'Concepto' => 'JUEGO ENGRANES RIFFEL 43DX16D CON CADENA 428Hx118L P/HONDA C', 'Precio' => 350.00]);
        Articulo_taller::create([
            'Concepto' => 'JUEGO ENGRANES RIFFEL 43X14 P/YAMAHA YBR125 ED', 'Precio' => 180.00]);
        Articulo_taller::create([
            'Concepto' => 'JUEGO ENGRANES RIFFEL 54Dx17D CON CADENA 428Hx130L P/HONDA NXR125 BROS ES', 'Precio' => 380.00]);
        Articulo_taller::create([
            'Concepto' => 'JUEGO HULES POSAPIE RIALLI P/YAMAHA YBR125C EXPRESS', 'Precio' => 80.00]);
        Articulo_taller::create([
            'Concepto' => 'JUEGO MANOPLAS P/YAMAHA YBR125C EXPRESS', 'Precio' => 120.00]);
        Articulo_taller::create([
            'Concepto' => 'JUEGO POSAPIES TRASERO PROMOTO P/HONDA CGL125 TOOL', 'Precio' => 100.00]);
        Articulo_taller::create([
            'Concepto' => 'JUEGO VALVULAS ADMISION Y ESCAPE P/YAMAHA YBR125C EXPRESS', 'Precio' => 80.00]);
        Articulo_taller::create([
            'Concepto' => 'KIT  CILINDRO COMPLETO PROMOTO P/YAMAHA YBR125 ED', 'Precio' => 850.00]);
        Articulo_taller::create([
            'Concepto' => 'KIT DE EMPAQUES COMPLETO HONDA TOOL 125', 'Precio' => 100.00]);
        Articulo_taller::create([
            'Concepto' => 'KIT DE RETENES MOTOR YAMAHA YBR 125', 'Precio' => 80.00]);
        Articulo_taller::create([
            'Concepto' => 'KIT EMPAQUES MOTOR A GL150 JGO 3', 'Precio' => 80.00]);
        Articulo_taller::create([
            'Concepto' => 'KIT EMPAQUES MOTOR COMPLETO  HONDA GL150', 'Precio' => 150.00]);
        Articulo_taller::create([
            'Concepto' => 'KIT EMPAQUES MOTOR COMPLETO HONDA CG125 CARGO', 'Precio' => 150.00]);
        Articulo_taller::create([
            'Concepto' => 'KIT ENGRANE 38X15 CADENA 428X108 P HONDA TOOL 125', 'Precio' => 300.00]);
        Articulo_taller::create([
            'Concepto' => 'KIT ENGRANE 43X16 CADENA 428X120 HONDA TITAN 150 GL150', 'Precio' => 450.00]);
        Articulo_taller::create([
            'Concepto' => 'LLANTA 90/90-18 6PR GHIRA', 'Precio' => 400.00]);
        Articulo_taller::create([
            'Concepto' => 'LLANTA 90/90-18 TT MANDRAKE PIRELLI', 'Precio' => 800.00]);
        Articulo_taller::create([
            'Concepto' => 'LLANTA RODA DP TT  3.00 -18', 'Precio' => 400.00]);
        Articulo_taller::create([
            'Concepto' => 'LLANTA TRAB KENDA 2.75 - 18 K208 TL', 'Precio' => 550.00]);
        Articulo_taller::create([
            'Concepto' => 'LLANTA TRAB KENDA 2.75 -18 K254 TT', 'Precio' => 550.00]);
        Articulo_taller::create([
            'Concepto' => 'LLANTA TRAB KENDA 90/90 - 18 K328 51P 4PL TL', 'Precio' => 700.00]);
        Articulo_taller::create([
            'Concepto' => 'LLANTA TRAB KENDA 90/90 -18 K328 TT', 'Precio' => 670.00]);
        Articulo_taller::create([
            'Concepto' => 'MANDO CONMUTADOR  IZQUIERDO HONDA GL150 CARGO', 'Precio' => 150.00]);
        Articulo_taller::create([
            'Concepto' => 'MANDO CONMUTADOR DERECHO HONDA GL150 CARGO', 'Precio' => 150.00]);
        Articulo_taller::create([
            'Concepto' => 'MANIJA  DE EMBRAGUE HONDA CGL 125 CON SOPORTE', 'Precio' => 120.00]);
        Articulo_taller::create([
            'Concepto' => 'MANIJA DE EMBRAGUE ORIGINAL GL150', 'Precio' => 120.00]);
        Articulo_taller::create([
            'Concepto' => 'MANIJA DE EMBRAGUE YAMAHA YBR 125', 'Precio' => 70.00]);
        Articulo_taller::create([
            'Concepto' => 'MANIJA DE FRENO HONDA CGL TOOL 125 CON ', 'Precio' => 120.00]);
        Articulo_taller::create([
            'Concepto' => 'MANIJA DE FRENO ORIGINAL GL 150', 'Precio' => 120.00]);
        Articulo_taller::create([
            'Concepto' => 'MANIJA DE FRENO YAMAHA YBR 125', 'Precio' => 70.00]);
        Articulo_taller::create([
            'Concepto' => 'MANIJA EMBRAGUE HONDA GL150 CON SOPORTE', 'Precio' => 120.00]);
        Articulo_taller::create([
            'Concepto' => 'MANIJA FRENO HONDA GL150 CON SOPORTE', 'Precio' => 120.00]);
        Articulo_taller::create([
            'Concepto' => 'MANUBRIO GL 150 ORIGINAL', 'Precio' => 450.00]);
        Articulo_taller::create([
            'Concepto' => 'MANUBRIO HONDA CARGO125 MA/MN YAMAHA YBR125 C EXP', 'Precio' => 250.00]);
        Articulo_taller::create([
            'Concepto' => 'MICA CASCO RODA DELIVER HUMO', 'Precio' => 180.00]);
        Articulo_taller::create([
            'Concepto' => 'MICA CASCO RODA EXPREES R CROMO', 'Precio' => 160.00]);
        Articulo_taller::create([
            'Concepto' => 'MOTOBATERIA 12N53B', 'Precio' => 450.00]);
        Articulo_taller::create([
            'Concepto' => 'MOTOBATERIA CTX7L-BS LTH', 'Precio' => 580.00]);
        Articulo_taller::create([
            'Concepto' => 'MOTOR ARRANQUE HONDA GL150 CARGO (WSTD)', 'Precio' => 800.00]);
        Articulo_taller::create([
            'Concepto' => 'PALANCA PERNO MOVIL FRENO TRASERO PROMOTO P/HONDA CGL125 TOOL', 'Precio' => 45.00]);
        Articulo_taller::create([
            'Concepto' => 'PARADOR LATERAL HONDA GL150 CARGO (WSTD)', 'Precio' => 100.00]);
        Articulo_taller::create([
            'Concepto' => 'PARCHE VERMAR V-1  42 MM', 'Precio' => 8.00]);
        Articulo_taller::create([
            'Concepto' => 'PARRILLA TRAS P/HONDA GL150 CARGO 359.09 359.09', 'Precio' => 650.00]);
        Articulo_taller::create([
            'Concepto' => 'PEDAL CAMBIOS YAMAHA YBR 125', 'Precio' => 90.00]);
        Articulo_taller::create([
            'Concepto' => 'PEDAL DE ARRANQUE HONDA CARGO 125', 'Precio' => 150.00]);
        Articulo_taller::create([
            'Concepto' => 'PEDAL DE CAMBIOS GL 150', 'Precio' => 90.00]);
        Articulo_taller::create([
            'Concepto' => 'PEDAL DE FRENO TRASERO HONDA CARGO GL 150', 'Precio' => 220.00]);
        Articulo_taller::create([
            'Concepto' => 'PEDAL DE FRENO TRASERO HONDA CGL TOOL 125', 'Precio' => 100.00]);
        Articulo_taller::create([
            'Concepto' => 'PEDAL FRENO P/YAMAHA YBR125 ED', 'Precio' => 300.00]);
        Articulo_taller::create([
            'Concepto' => 'PERNO MOVIL TRASERO PROMOTO P/HONDA CGL125 TOOL', 'Precio' => 45.00]);
        Articulo_taller::create([
            'Concepto' => 'PISTON COMPLETO HONDA GL150 .25', 'Precio' => 250.00]);
        Articulo_taller::create([
            'Concepto' => 'PORTA ENGRANE TRASERO HONDA GL150 CARGO (WSTD)', 'Precio' => 300.00]);
        Articulo_taller::create([
            'Concepto' => 'PRENSA CENTRO GUIA EMBRAGUE HONDA GL150 CARGO', 'Precio' => 200.00]);
        Articulo_taller::create([
            'Concepto' => 'PRENSA CENTRO GUIA EMBRAGUE P/ YAMAHA 125 WSTD', 'Precio' => 140.00]);
        Articulo_taller::create([
            'Concepto' => 'RAYO EXTERNO RUEDA TRASERA', 'Precio' => 80.00]);
        Articulo_taller::create([
            'Concepto' => 'RAYO INTERNO RUEDA TRASERA', 'Precio' => 80.00]);
        Articulo_taller::create([
            'Concepto' => 'RESORTE PARADOR LATERAL HONDA GL150', 'Precio' => 20.00]);
        Articulo_taller::create([
            'Concepto' => 'RETEN BARRA SUSPENSION HONDA GL 150/TOOL 31X43X10.5', 'Precio' => 25.00]);
        Articulo_taller::create([
            'Concepto' => 'RIN 1.60 X 18 9G HONDA/CARGO125/150 TITAN 125 YBR125 C (DEL)', 'Precio' => 450.00]);
        Articulo_taller::create([
            'Concepto' => 'SALPICADERA DELANTERA HONDA GL150 CARGO (NEGRO)', 'Precio' => 350.00]);
        Articulo_taller::create([
            'Concepto' => 'SALPICADERA DELANTERA ORIGINAL YAMAHA YBR NEGRO', 'Precio' => 600.00]);
        Articulo_taller::create([
            'Concepto' => 'SELLO DE VALVULA', 'Precio' => 80.00]);
        Articulo_taller::create([
            'Concepto' => 'SENSOR DE FRENO DELANTERO GL 150', 'Precio' => 100.00]);
        Articulo_taller::create([
            'Concepto' => 'SENSOR DE FRENO TRASERO GL150', 'Precio' => 100.00]);
        Articulo_taller::create([
            'Concepto' => 'SWITH DE ENCENDIDO PARA CARGO GL 150 ORIGINAL', 'Precio' => 450.00]);
        Articulo_taller::create([
            'Concepto' => 'TABLERO DE INSTRUMENTOS YBR ORIGINAL', 'Precio' => 2, 100.00]);
        Articulo_taller::create([
            'Concepto' => 'TABLERO INSTRUMENTOS P/YAMAHA YBR125C EXPRESS', 'Precio' => 800.00]);
        Articulo_taller::create([
            'Concepto' => 'TANQUE GASOLINA HONDA CGL TOOL 125 BCO', 'Precio' => 900.00]);
        Articulo_taller::create([
            'Concepto' => 'TAPA INFERIOR DE TABLERO GL 150', 'Precio' => 150.00]);
        Articulo_taller::create([
            'Concepto' => 'TAPA LATERAL DEREC HA IZQUIERDA HONDA GL150', 'Precio' => 200.00]);
        Articulo_taller::create([
            'Concepto' => 'TAPA SUPERIOR DE TABLERO GL 150', 'Precio' => 250.00]);
        Articulo_taller::create([
            'Concepto' => 'TAPON GASOLINA PROMOTO P/HONDA CGL125 TOOL', 'Precio' => 150.00]);
        Articulo_taller::create([
            'Concepto' => 'TAPON TANQUE GASOLINA HONDA CARGO125 GL150 CARGO BROS 125', 'Precio' => 150.00]);
        Articulo_taller::create([
            'Concepto' => 'TENSOR CADENA DISTRIBUCION P/YAMAHA YBR125C EXPRESS PCS', 'Precio' => 100.00]);
        Articulo_taller::create([
            'Concepto' => 'TENSOR DE CADENA DER/IZQ HONDA GL 150 WSTD', 'Precio' => 80.00]);
        Articulo_taller::create([
            'Concepto' => 'TENSOR DE CADENA DERECHO IZQUIERDO YAMAHAYBR 125', 'Precio' => 40.00]);
        Articulo_taller::create([
            'Concepto' => 'TUBO DE CEMENTO 20 ML', 'Precio' => 40.00]);
        Articulo_taller::create([
            'Concepto' => 'TUBO DE FUERZA COMPLETO DER/IZQ HONDA GL150 WSTD', 'Precio' => 1, 600.00]);
        Articulo_taller::create([
            'Concepto' => 'TUBO FUERZA COMPLETO DER/IZQ HONDA GL150 CARGO (WSTD) (JGO)', 'Precio' => 1, 800.00]);
        Articulo_taller::create([
            'Concepto' => 'TUBO FUERZA SUSPENSION DELANTERA YAMAHA YBR125 C EXP (JGO2)', 'Precio' => 800.00]);
        Articulo_taller::create([
            'Concepto' => 'TUERCA TELESCOPIO CARGO GL 150', 'Precio' => 40.00]);
        Articulo_taller::create([
            'Concepto' => 'VALVULA ESCAPE HONDA TITAN 150/GL150 CARGO JAPON', 'Precio' => 120.00]);
        Articulo_taller::create([
            'Concepto' => 'VARILLA FRENO P/YAMAHA YBR125 ED', 'Precio' => 80.00]);
        Articulo_taller::create([
            'Concepto' => 'VELOCIMETRO HONDA GL150 CARGO', 'Precio' => 600.00]);
        Articulo_taller::create([
            'Concepto' => 'YUGO SUPERIOR ORIGINAL YBR 125', 'Precio' => 190.00]);

    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('articulos_taller');
    }
}
