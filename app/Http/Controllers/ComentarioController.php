<?php

namespace App\Http\Controllers;

use App\Models\Comentario;
use App\Http\Controllers\JelpController;
use Illuminate\Http\Request;
use Validator;

/**
 * Clase de cometarios
 */
class ComentarioController extends JelpController {

    public function __construct(Comentario $comentario) {
        $this->comentario = $comentario;
    }

	/**
	 * Obtiene el listado de Comentarios.
	 */
    public function getAll()
    {
        $comentario = new Comentario();
        $comentarios = $comentario->all();
        return JelpController::Respuesta(['Comentarios'=>$comentarios,'Respuesta'=>'200']);
    }

    /**
     * Obtiene un comentario por ID.
     */
    public function getComentario($id){
    	$comentario = Comentario::find($id);
    	if($comentario==null)
    		return JelpController::NotFound();
    	return JelpController::Respuesta(['Comentario'=>$comentario]);
    }

    /**
     * Metodo para crear y actualizar un comentario
     */
    public function postComentario(Request $request, $id = 0){

		$validator = Validator::make($request->all(), [
            'Comentario' => 'required',
        ]);

        if ($validator->fails()) {
            return JelpController::CustomError(['Error'=>'Comentario es requerido.']);
        }

        $comentario = Comentario::updateOrCreate(
        	array('iComentario' => $id), 
        	array('Comentario' => $request->input('Comentario')));

        return JelpController::Respuesta(['Comentario'=>$comentario]);
    }

    /**
     * Metodo para eliminar un comentario
     */
    public function deleteComentario($id){
		try {
			$comentario = Comentario::find($id);
			if($comentario == null){
				return JelpController::NotFound();
			}
			$comentario->delete();
			return JelpController::Respuesta();
		} catch (ModelNotFoundException $ex){
			return JelpController::CustomError($ex);
		}catch (Exception $e) {
			return JelpController::CustomError($e);
		}
    }
} 