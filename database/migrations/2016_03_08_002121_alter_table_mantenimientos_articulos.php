<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableMantenimientosArticulos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mantenimiento_articulos', function(Blueprint $table){
            $table->renameColumn('Monto', 'Importe');
            $table->renameColumn('iMantenimientoArticulos', 'iMttoArticuloDetalle');
        });

        Schema::rename('mantenimiento_articulos', 'mtto_articulos_detalle');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mtto_articulos_detalle', function(Blueprint $table){
            $table->renameColumn('Importe', 'Monto');
            $table->renameColumn('iMttoArticuloDetalle','iMantenimientoArticulos');
        });

        Schema::rename('mtto_articulos_detalle', 'mantenimiento_articulos');
    }
}
