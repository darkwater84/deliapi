<?php

namespace App\Http\Controllers;

use App\Models\Asignacion;
use App\Models\Motocicleta;
use App\Models\EstatusMotocicleta;
use App\Http\Controllers\JelpController;
use App\Models\Sucursal;
use Illuminate\Http\Request;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Validator;

/**
 * Clase de cometarios
 */
class MotocicletaController extends JelpController {

    public function __construct(Motocicleta $motocicleta) {
        $this->motocicleta = $motocicleta;
    }

	/**
	 * Obtiene el listado de Motocicletas.
	 */
    public function getAll(){
        $motocicletas = Motocicleta::with('Estatus')->with('Bitacora')->get();
        return JelpController::Respuesta(['Motocicletas'=>$motocicletas]);
    }

    /**
     * GET - Motocicleta
     */
    public function getMotocicleta($id){
        $motocicleta = Motocicleta::find($id);
        if($motocicleta == null){
            return JelpController::NotFound();
        }
        return JelpController::Respuesta(['Motocicleta'=>$motocicleta]);
    }

    /**
     * POST - Agrega o Edita una motocicleta
     */
    public function postMotocicleta(Request $request, $id = 0){

        $validator = Validator::make($request->all(), [
            'Marca'=>'required',
            'Modelo' => 'required',
            'Placas' => 'required',
            'iEstatus' => 'required']);

         if($validator->fails()){
             return JelpController::CustomError(['Error'=>'Modelo, Placas, iEstatus son requeridos.']);
         }

        $motocicleta = Motocicleta::updateOrCreate(array('iMotocicleta' => $id), array(
            'Marca'=> $request->input('Marca'),
            'Modelo' => $request->input('Modelo'),
            'Placas' => $request->input('Placas'),
            'iEstatus' => $request->input('iEstatus')));

        $motocicleta->estatus = EstatusMotocicleta::where('iEstatus','=',$motocicleta->iEstatus)
                                ->first();

        return JelpController::Respuesta(['Motocicleta'=>$motocicleta]);
    }

    /**
     * DELETE - Elimina una motocicleta
     */
    public function deleteMotocicleta($id){
        try {
            $motocicleta = Motocicleta::find($id);
            if($motocicleta == null){
                return JelpController::NotFound();
            }
            $motocicleta->delete();
            return JelpController::Respuesta();
        } catch (ModelNotFoundException $ex){
            return JelpController::CustomError($ex);
        }catch (Exception $e) {
            return JelpController::CustomError($e);
        }
    }

    public function getEstatusMotocicletas(){
        $estatusMotocicletas = EstatusMotocicleta::all();
        return JelpController::Respuesta(['Estatus_Motocicleta'=>$estatusMotocicletas]);
    }

    public function searchMotocicleta(Request $request){
        $placas = ($request->input('Placas')) ?: '';

        $motocicletas = Motocicleta::select('iMotocicleta', 'Modelo', 'Placas', 'Marca')
            ->where('Placas', 'like', $placas . '%')
            ->get();

        foreach($motocicletas as $motocicleta)
        {
            $asignacion = Asignacion::where('iMotocicleta', '=', $motocicleta->iMotocicleta)
                ->orderBy('created_at', 'desc')
                ->first();
            if($asignacion) {
                $motocicleta->Sucursal = Sucursal::select('iSucursal', 'Sucursal', 'NumeroSucursal')
                    ->where('iSucursal', '=', $asignacion->iSucursal)
                    ->first();
            }else{
                $motocicleta->Sucursal = Sucursal::select('iSucursal', 'NumeroSucursal', 'iSucursal')
                    ->first();
            }
        }
        return JelpController::Respuesta($motocicletas);
    }
}
