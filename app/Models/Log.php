<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
class Log extends Model {

    protected $table = 'logs';
    protected $primaryKey = 'iLog';
    protected $fillable = array('iLog','iUsuario','Mensaje','Request','Response','Detalle','Origen');
    protected $visible = array('iLog','iUsuario','Mensaje','Request','Response','Detalle','Origen','created_at');
}
