<?php

namespace App\Lib;

use Parse\ParseClient;
use Parse\ParseInstallation;
use Parse\ParsePush;

class ParseCalls{
    public static function sendPush($data, $devices){

        ParseClient::initialize(env("PARSE_PUBLIC"), env("PARSE_PRIVATE"), env("PARSE_APP"));

        if(is_array($devices))
        {
            $query = ParseInstallation::query()->containedIn("IMEI", $devices);
        }else{

            $query = ParseInstallation::query()->equalTo("IMEI", $devices);
        }

        ParsePush::send(array(
            "where" => $query,
            "data" => $data
        ));
    }
}