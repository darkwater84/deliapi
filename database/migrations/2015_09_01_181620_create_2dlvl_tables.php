<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Create2dlvlTables extends Migration
{
    const SHORT_TEXT = 25;
    const MID_TEXT = 50;
    const LONG_TEXT = 100;
    const TEXT = 255;
    
    const EMAIL = 255;
    const PHONE = 15;
    const COORDINATES = 27;

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usuarios', function (Blueprint $table) {
            $table->increments('iUsuario');
            $table->string('Usuario', $this::MID_TEXT);
            $table->string('Password', $this::LONG_TEXT);
            $table->string('Nombre', $this::MID_TEXT);
            $table->string('Apellidos', $this::MID_TEXT);
            $table->string('NoEmpleado', $this::SHORT_TEXT);
            $table->string('Email', $this::EMAIL);
            $table->integer('iRol')->unsigned();
            $table->foreign('iRol')->references('iRol')->on('roles')->onDelete('cascade');
            $table->nullableTimestamps();
            $table->softDeletes();
        });

        Schema::create('motocicletas', function (Blueprint $table) {
            $table->increments('iMotocicleta');
            $table->string('Marca', $this::MID_TEXT);
            $table->string('Modelo', $this::MID_TEXT);
            $table->string('Placas', $this::SHORT_TEXT);
            $table->integer('iEstatus')->unsigned();
            $table->foreign('iEstatus')->references('iEstatus')->on('estatus_motocicleta')->onDelete('cascade');
            $table->nullableTimestamps();
            $table->softDeletes();
        });

        Schema::create('dispositivos', function (Blueprint $table) {
            $table->increments('iDispositivo');
            $table->string('Marca', $this::SHORT_TEXT);
            $table->string('Modelo', $this::SHORT_TEXT);
            $table->string('IMEI', $this::SHORT_TEXT);
            $table->string('Numero', $this::PHONE);
            $table->integer('iEstatus')->unsigned();
            $table->integer('iSucursal')->unsigned();
            $table->foreign('iEstatus')->references('iEstatus')->on('estatus_dispositivo')->onDelete('cascade');
            $table->foreign('iSucursal')->references('iSucursal')->on('sucursales')->onDelete('cascade');
            $table->nullableTimestamps();
            $table->softDeletes();
        });

        Schema::create('ventas', function (Blueprint $table) {
            $table->increments('iVenta');
            $table->string('Folio', $this::MID_TEXT);
            $table->string('Telefono', $this::MID_TEXT);
            $table->string('Importe', $this::MID_TEXT);
            $table->string('Firma', $this::MID_TEXT);
            $table->string('Estatus', $this::MID_TEXT);
            $table->string('Latitud', $this::MID_TEXT);
            $table->string('Longitud', $this::MID_TEXT);
            $table->integer('iEstatus')->unsigned();
            $table->integer('iSucursal')->unsigned();
            $table->foreign('iEstatus')->references('iEstatus')->on('estatus')->onDelete('cascade');
            $table->foreign('iSucursal')->references('iSucursal')->on('sucursales')->onDelete('cascade');
            $table->nullableTimestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ventas');
        Schema::drop('dispositivos');
        Schema::drop('motocicletas');
        Schema::drop('usuarios');
    }
}
