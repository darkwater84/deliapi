<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIndexPlacasMotocicletas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('motocicletas',function(Blueprint $table){
            $table->index('Placas', 'index_placas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('motocicletas',function(Blueprint $table){
            $table->dropIndex('index_placas');
        });
    }
}
