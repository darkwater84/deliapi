<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Exception;
use DB;
use Illuminate\Database\Eloquent\SoftDeletes;

class Reparto extends Model{

    use SoftDeletes;
    protected $table = 'repartos';
    protected $primaryKey = 'iReparto';
    protected $dates = ['deleted_at'];
    protected $guarded = array('iReparto', 'iVenta','iRepartidor','iSucursal');
    protected $fillable = array('HoraSalida', 'HoraEntrega', 'iReparto', 'iVenta','iRepartidor','iSucursal','Traspaso' );
    protected $visible = array('HoraSalida', 'HoraEntrega', 'iReparto', 'iVenta','iRepartidor','iSucursal',
        'created_at','updated_at','Traspaso', 'Total', 'Sucursal', 'NumeroSucursal', 'HoraEnterado');


    const CODIGO_DISTANCIA = 'DTS01';

    //Retorna la venta ligada al reparto
    public function getVenta()
    {
        return $this->hasOne('App\Models\Venta','iVenta');
    }

    //Retorna el repartido ligado el reparto/envio
    public function getRepartidor()
    {
        return $this->hasOne('App\Models\Repartidor','iRepartidor');
    }

    public function completarReparto($id)
    {
        try {
            $reparto=$this->find($id);
            $reparto->iComentario=1;
            if($reparto->save()) {
                return $reparto;
            }
            else
                throw new Exception('No se ha logrado actualizar el registro de reparto o no existe',1001);
        }catch (Exception $e)
        {
            throw new Exception('No se ha logrado actualizar el registro de reparto o no existe',1001);
        }
    }

    public function cancelarReparto($id, $comentario)
    {
        try {
            $reparto=$this->find($id);
            $reparto->iComentario=$comentario['iComentario'];
            if($reparto->save()) {
                return $reparto;
            }
            else
                throw new Exception('No se ha logrado actualizar el registro de reparto o no existe',1006);
        }catch (Exception $e)
        {
            throw new Exception('No se ha logrado actualizar el registro de reparto o no existe',1005);
        }
    }

    /**
     * Obtiene la ruta del Reparto.
     */
    public function RutaRepartidor(){
        try {

            /*$query = 'SELECT Latitud AS latitude, Longitud AS longitude, created_at ' .
                ' FROM rutas ' .
                ' WHERE ' .
                ' created_at BETWEEN \'' . $this->HoraSalida . '\' AND \''.$this->HoraEntrega.'\' AND' .
                ' iRepartidor = ' . $this->iRepartidor .
                ' ORDER BY created_at asc;';*/
            $query = "SELECT Latitud AS latitude, Longitud AS longitude, created_at from rutas where created_at between '$this->HoraSalida' and '$this->HoraEntrega' and iRepartidor=$this->iRepartidor order by created_at ASC";

            $ruta = DB::select(DB::raw($query));

            return $ruta;
        } catch (Exception $e) {
            return array('Error:'=>'Error de Ruta de Repartidor'.$e->getMessage());
        }
    }


}
