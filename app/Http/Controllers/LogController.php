<?php
namespace App\Http\Controllers;
use App\Http\Controllers\JelpController;
use Illuminate\Http\Request;
use App\Models\Log;
use Validator;
use Exception;
/**
 *
 */
class LogController extends JelpController
{
  public function __construct(Log $log) {
      $this->log = $log;
  }

  public function getLog()
  {
    $logs = Log::whereRaw('DATE(created_at) >= CURDATE() - INTERVAL 1 DAY')
            ->orderBy('created_at','desc')
            ->get();
    return JelpController::Respuesta($logs);
  }

  public function postLog(Request $request)
  {
    $validator = Validator::make($request->all(), [
        'mensaje' => 'required',
        'origen' => 'required',
    ]);

    if ($validator->fails()) {
        return JelpController::CustomError('mensaje y origen Requeridos');
    }
    try {
      $log = new Log();
      $log->iUsuario = $request->input('iUsuario');
      $log->Mensaje = $request->input('mensaje');
      $log->Request = $request->input('request');
      $log->Response = $request->input('response');
      $log->Detalle = $request->input('detalle');
      $log->Origen = $request->input('origen');
      $log->save();

      return JelpController::Respuesta();

    } catch (Exception $e) {
      return JelpController::CustomError('Fallo Log' . $e->getMessage());
    }

  }
}
