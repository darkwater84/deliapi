<?php
/**
 * Created by PhpStorm.
 * User: totushi
 * Date: 7/20/15
 * Time: 2:46 PM
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Estatus_repartidor extends Model{

    use SoftDeletes;
    protected $table = 'estatus_repartidor';
    protected $primaryKey = 'iEstatus';
    protected $dates = ['deleted_at'];
    protected $guarded = array('iEstatus');
    protected $fillable = array('Estatus');
    protected $visible = array('iEstatus','Estatus','created_at','updated_at');

    const IN_MAINTENANCE = 3;
    const ACTIVE = 1;

}