<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableMttoObservaciones extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mtto_observaciones', function(Blueprint $table){
            $table->unsignedInteger('iMantenimiento')->after('iUsuario');
            $table->foreign('iMantenimiento')->references('iMantenimiento')->on('mantenimientos');
            $table->tinyInteger('Aceptado')->after('Comentario');
            $table->text('Comentario')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mtto_observaciones', function(Blueprint $table){
            $table->dropForeign('mtto_observaciones_imantenimiento_foreign');
            $table->dropColumn('iMantenimiento');
            $table->dropColumn('Aceptado');
            $table->text('Comentario')->change();
        });
    }
}
