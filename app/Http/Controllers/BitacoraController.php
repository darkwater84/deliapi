<?php
namespace App\Http\Controllers;

use App\Models\Asignacion;
use App\Models\Bitacora;
use App\Models\Bitacora_checklist;
use App\Models\Repartidor;
use App\Models\Usuario;
use Faker\Provider\DateTime;
use Illuminate\Http\Request;
use App\Models\Grupo_checklist;
use Illuminate\Support\Facades\Validator;
use League\Flysystem\Exception;
use App\Models\Log;
use Illuminate\Support\Facades\DB;

class BitacoraController extends JelpController{

    const MOVIL = 'MOVIL';
    const CRM = 'CRM';
    const API = 'API';
    const REPARTIDOR = 5;

    /*
     * Obtener las series de preguntas para la inspeccion
     */
    public function getChecklist(){
        $encabezados = Grupo_checklist::where('Activo' , '=' , 1)
            ->orderBy('Orden', 'asc')
            ->get();

        foreach($encabezados as $encabezado)
        {
            $encabezado->Checklist = $encabezado->checklist;
        }

        return JelpController::Respuesta(["respuesta"=>$encabezados]);
    }
    /*
     * Guardar una bitacora de Inspeccion con respuestas
     */
    public function saveChecklist(Request $request){

        $validator = Validator::make($request->all(),[
            'EmpleadoEntrega' => 'required',
            'iUsuarioRecibido' => 'required|different:EmpleadoEntrega',
            'Evidencia' => 'required|url',
            'iMotocicleta' => 'required',
            'Rotulos' => 'required',
            'Kilometraje' => 'required',
            'Checklist' => 'required'
        ]);

        if($validator->fails())
        {
            return JelpController::CustomError($validator->errors()->first());
        }
        $inputs = $request->all();

        try{
            $date = new \DateTime();

            $empleadoEntrega = Usuario::where('NoEmpleado','=',$inputs['EmpleadoEntrega'])->where('iRol','=',$this::REPARTIDOR)->first();

            $repartidor = Repartidor::where('iUsuario','=',$inputs['iUsuarioRecibido'])
                ->first();

            if($empleadoEntrega != NULL AND $empleadoEntrega->iUsuario == $repartidor->iUsuario)
                return JelpController::CustomError('Usuario Recibido y Entrega es el mismo');

            $bitacora = Bitacora::create([
                'NoEmpleadoEntrega' => $inputs['EmpleadoEntrega'],
                'iUsuarioRecibido' => $inputs['iUsuarioRecibido'],
                'Evidencia' => $inputs['Evidencia'],
                'iMotocicleta' => $inputs['iMotocicleta'],
                'Rotulos' => boolval($inputs['Rotulos']),
                'Kilometraje' => $inputs['Kilometraje'],
                'FechaEntrega' => $date->format('Y-m-d'),
                'FechaRecibido' => $date->format('Y-m-d'),
                'HoraEntrega' => $date->format('H:i:s'),
                'HoraRecibido' => $date->format('H:i:s'),
                'Observacion' => $inputs['Observacion']
            ]);

            if($empleadoEntrega != NULL){
                $bitacora->iUsuarioEntrega = $empleadoEntrega->iUsuario;
                $bitacora->save();
            }

            //una vez que se tiene salvada la bitacora, hay que recorrer las respuestas
            $checklist = $inputs['Checklist'];
            foreach($checklist as $respuesta)
            {
                $bitacoraCheck = new Bitacora_checklist();
                $bitacoraCheck->iChecklist = $respuesta['iChecklist'];
                $bitacoraCheck->iBitacora = $bitacora->iBitacora;
                if(is_bool($respuesta['Valor']))
                {
                    $bitacoraCheck->Check = $respuesta['Valor'];
                }
                else{
                    $bitacoraCheck->Valor = $respuesta['Valor'];
                }
                $bitacoraCheck->save();
            }


            $ultimaAsignacion=Asignacion::where('iRepartidor','=',$repartidor->iRepartidor)
                ->orderBy('created_at','DESC')
                ->first();
            if($ultimaAsignacion)
            {
                $ultimaAsignacion->iMotocicleta=$inputs['iMotocicleta'];
                $ultimaAsignacion->save();
            }

            $repartidor->iMotocicleta = $inputs['iMotocicleta'];
            $repartidor->save();

            return JelpController::Respuesta($bitacora);
        }
        catch(Exception $e)
        {
            Log::create(array('Mensaje' => $e->getMessage(),
                'Request' => json_encode($request->all()),
                'Response' => $e->getMessage(),
                'Detalle' => $e->getTrace(),
                'Origen' => $this::API));
            return JelpController::CustomError($e->getMessage());
        }

    }
    /*
     * Obtener una bitacora y sus respuestas por ID de bitacora
     */
    public function getBitacoraChecklist($id){
        try{
            $bitacora = Bitacora::where('iBitacora','=',$id)
                ->with('bitacoraChecklist.checklist.grupoChecklist')
                ->with('Motocicleta')
                ->with('usuarioEntrega.Repartidor')
                ->with('usuarioRecibido.Repartidor')
                ->first();

            if($bitacora->usuarioEntrega != NULL){
                $bitacora
                    ->usuarioEntrega
                    ->Sucursal=Repartidor::asignacion($bitacora
                    ->usuarioEntrega
                    ->Repartidor
                    ->iRepartidor);
            }

            $bitacora
                ->usuarioRecibido
                ->Sucursal=Repartidor::asignacion($bitacora
                                                    ->usuarioRecibido
                                                    ->Repartidor
                                                    ->iRepartidor);
            return JelpController::Respuesta($bitacora,true);
        }
        catch(Exception $e){
            Log::create(array('Mensaje' => $e->getMessage(),
                'Request' => 'bitacora/'.$id,
                'Response' => $e->getMessage(),
                'Detalle' => $e->getTrace(),
                'Origen' => $this::API));
            return JelpController::CustomError($e->getMessage());
        }
    }

    public function getBitacora(Request $request){
        $inputs = $request->all();

        $validations = Validator::make($inputs, [
            'Inicio' => 'required|date',
            'Fin'    => 'required|date'
        ]);

        if($validations->fails()){
            return JelpController::CustomError($validations->errors()->first());
        }

        $bitacoras = DB::table('bitacoras as b')
            ->select('iBitacora', 'Kilometraje', 'FechaEntrega', 'FechaRecibido','HoraEntrega','HoraRecibido',
            'iUsuarioRecibido', 'Evidencia' ,'iUsuarioEntrega','NoEmpleadoEntrega' ,'b.iMotocicleta', 'Marca', 'Modelo', 'Placas',
            DB::raw("concat(us.NoEmpleado, ' - ', us.Nombre, ' ', us.Apellidos) as UsuarioEntrega"),
            DB::raw("concat(u.NoEmpleado, ' - ', u.Nombre, ' ', u.Apellidos) as UsuarioRecibido"))
            ->join('motocicletas as m', 'b.iMotocicleta', '=', 'm.iMotocicleta')
            ->join('usuarios as u', 'iUsuarioRecibido', '=', 'u.iUsuario')
            ->join('usuarios as us', 'iUsuarioEntrega','=', 'us.iUsuario', 'left outer')
            ->whereDate('b.created_at', '>=' , $inputs['Inicio'])
            ->whereDate('b.created_at', '<=', $inputs['Fin'])
            ->get();

        return JelpController::Respuesta($bitacoras);
    }
}