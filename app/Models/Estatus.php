<?php
/**
 * Created by PhpStorm.
 * User: totushi
 * Date: 7/20/15
 * Time: 2:46 PM
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Estatus extends Model{

    use SoftDeletes;

    protected $table = 'estatus';
    protected $primaryKey = 'iEstatus';
    protected $dates = ['deleted_at'];
    protected $visible = array('iEstatus','Estatus','created_at');
    protected $fillable = array('Estatus');

    const PENDING   = 1;
    const ACCEPTED  = 2;
    const TRANSIT   = 3;
    const COMPLETED = 4;
}