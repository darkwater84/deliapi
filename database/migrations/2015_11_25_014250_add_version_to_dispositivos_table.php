<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddVersionToDispositivosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('dispositivos', function (Blueprint $table) {
            $table->string('VersionApp', 10)->nullable();
            $table->string('VersionAndroid', 5)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      if (Schema::hasColumn('dispositivos', 'Version')) {
          Schema::table('dispositivos', function (Blueprint $table) {
              $table->dropColumn('VersionApp');
              $table->dropColumn('VersionAndroid');
          });
      }
    }
}
