<?php

namespace app\Http\Controllers;

use App\Models\Mantenimiento;
use App\Models\Mtto_articulo_detalle;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\JelpController;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Exception;

class MttoDetalleArticuloController extends JelpController{

    /**
     * @param Request $request
     * @param $iMantenimiento
     * @return \Illuminate\Http\Response
     * @description Metodo para agregar un articulo al detalle del matenimiento
     */
    public function addArticleDetail(Request $request, $iMantenimiento){
        if(!is_numeric($iMantenimiento)){
            return JelpController::CustomError('Mantenimiento Inválido');
        }

        $article = $request->input('iMttoArticulo');
        $inputs = $request->all();

        $validations = Validator::make($inputs, [
            'Cantidad' => 'required|numeric|min:1',
            'iMttoArticulo' => 'required|numeric|exists:mtto_articulos,iMttoArticulo,iMttoArticulo,'. $article
        ]);

        $validations->setAttributeNames([
            'Cantidad'      => 'Cantidad',
            'iMttoArticulo' => 'iMttoArticulo'
        ]);

        if($validations->fails())
        {
            return JelpController::CustomError($validations->errors()->first());
        }

        try{
            $maintenance = Mantenimiento::findOrFail($iMantenimiento);
            $article_detail = Mtto_articulo_detalle::storeArticleDetail($inputs, $maintenance);
            return JelpController::Creado($article_detail);
        }
        catch (ModelNotFoundException $e){
            return JelpController::CustomError('Mantenimiento Inválido');
        }
        catch(Exception $e){
            return JelpController::CustomError($e->getMessage());
        }
    }

    /**
     * @param $iMantenimiento
     * @param $iMttoArticulo
     * @return \Illuminate\Http\Response
     * @description Metodo para eliminar articulo de detalle del mantenimiento
     */
    public function deleteArticleDetail($iMantenimiento, $iMttoArticulo){
        try{

            if(!is_numeric($iMantenimiento)){
                return JelpController::CustomError('Mantenimiento Inválido');
            }

            if(!is_numeric($iMttoArticulo)){
                return JelpController::CustomError('Articulo Inválido');
            }

            $maintenance = Mantenimiento::findOrFail($iMantenimiento);
            Mtto_articulo_detalle::deleteArticleDetail($maintenance, $iMttoArticulo);
            return JelpController::Respuesta();
        }
        catch (ModelNotFoundException $e){
            return JelpController::CustomError('Mantenimiento Inválido');
        }
        catch(Exception $e){
            return JelpController::CustomError($e->getMessage());
        }
    }

    /**
     * @param Request $request
     * @param $iMantenimiento
     * @param $iMttoArticulo
     * @return \Illuminate\Http\Response
     * @description Metodo para actualizar un articuloe existente en detalle de mantenimiento
     */
    public function updateArticleDetail(Request $request, $iMantenimiento, $iMttoArticulo)
    {
        if(!is_numeric($iMantenimiento)){
            return JelpController::CustomError('Mantenimiento Inválido');
        }

        if(!is_numeric($iMttoArticulo)){
            return JelpController::CustomError('Articulo Inválido');
        }

        $inputs = $request->all();

        $validations = Validator::make($inputs, [
            'Cantidad' => 'required|numeric|min:1'
        ]);

        $validations->setAttributeNames([
            'Cantidad'      => 'Cantidad'
        ]);

        if($validations->fails())
        {
            return JelpController::CustomError($validations->errors()->first());
        }

        try {
            $maintenance = Mantenimiento::findOrFail($iMantenimiento);
            $article_detail = Mtto_articulo_detalle::updateArticleDetail($inputs['Cantidad'], $maintenance, $iMttoArticulo);
            return JelpController::Creado($article_detail);
        }
        catch (ModelNotFoundException $e) {
            return JelpController::CustomError('Mantenimiento Inválido');
        }
        catch (Exception $e) {
            return JelpController::CustomError($e->getMessage());
        }
    }
}