<?php

namespace App\Jobs;

use App\Models\Venta;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

/**
 * Tarea ejecutada 45 minutos despues de que la venta se haya puesto en transito
 *
 * Class NotificarTransito
 * @package App\Jobs
 */
class NotificarTransito extends Job implements SelfHandling, ShouldQueue
{
    use SerializesModels;

    private $_venta;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Venta $venta)
    {
        $this->_venta = $venta;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $venta = $this->_venta->fresh();

        // Ignorar si la venta ya no esta en transito
        if($venta->iEstatus != 3){
            $this->delete();
            return;
        }

        $emails = DB::table('usuarios')
            ->select('Email')
            ->where('iRol', '=', 2)
            ->where('Email', '!=', '')
            ->get();

        $emails = array_map(function ($email) {
            if($email->Email) {
                return $email->Email;
            }
        }, $emails);

        try {
            Mail::send([], [],
                function ($message) use ($emails, $venta) {
                    $message->bcc($emails);
                    $message->subject('Pedido en transito');
                    $message->setBody('Pedido con folio ' . $venta->Folio . ' en transito por más de 45 minutos');
                });
        } catch (\Exception $e) {
            Log::warning('No se pudo enviar correo de pendido pendiente: ' . $e->getMessage());
        }
    }
}